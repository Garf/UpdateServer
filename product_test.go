package main

import (
	"testing"
)

func TestProduct(t *testing.T) {
	router := initTest(t)

	// List Products (empty)
	AssertAPIOK(t, router, "GET", productPrefix, "", "[]")
	// Get Product that does not exists
	AssertAPIErrorNotFound(t, router, "GET", s("%s/%s", productPrefix, "42"), "")
	// Create
	id1 := AssertAPIOKAndGetID(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`,
		`{"id": %d, "name": "vlc", "description": "A cØØl multimedia player"}`)
	// Cannot recreate a product with the same name
	AssertAPIErrorUniqueConstraint(t, router, "POST", productPrefix, `{"name": "vlc", "description": ""}`)
	// Cannot create a product with an empty name
	AssertAPIErrorRequired(t, router, "POST", productPrefix, `{"name": ""}`)
	// Get Product
	AssertAPIOK(t, router, "GET", s("%s/%d", productPrefix, id1), "",
		s(`{"id": %d, "name": "vlc", "description": "A cØØl multimedia player"}`, id1))
	// Modify
	AssertAPIOK(t, router, "PUT", s("%s/%d", productPrefix, id1),
		`{"name": "newvlc", "description": "A new cool multimedia player"}`,
		s(`{"id": %d, "name": "newvlc", "description": "A new cool multimedia player"}`, id1))
	// Default Channel process
	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, id1)))
	// Set channel1 as default channel
	AssertAPIOK(t, router, "PUT", s("%s/%d", productPrefix, id1),
		s(`{"name": "newvlc", "description": "A new cool multimedia player", "default_channel": %d}`, channelID1),
		s(`{"id": %d, "name": "newvlc", "description": "A new cool multimedia player", "default_channel": %d}`, id1, channelID1))
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%d", productPrefix, id1), "",
		s(`{"id": %d, "name": "newvlc", "description": "A new cool multimedia player", "default_channel": %d}`, id1, channelID1))
	// Delete Channel
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", channelPrefix, channelID1), "")
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%d", productPrefix, id1), "",
		s(`{"id": %d, "name": "newvlc", "description": "A new cool multimedia player"}`, id1))
	// Delete
	AssertAPIOK(t, router, "DELETE", s("%s/%d", productPrefix, id1), "",
		s(`{"id": %d, "name": "newvlc", "description": "A new cool multimedia player"}`, id1))
	// Cannot Modify that does not exists
	AssertAPIErrorNotFound(t, router, "PUT", s("%s/%d", productPrefix, id1), `{"name": "test12422", "description": ""}`)
	// Cannot Delete again
	AssertAPIErrorNotFound(t, router, "DELETE", s("%s/%d", productPrefix, id1), "")
	// Create another product
	id1 = AssertAPIOKAndGetID(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`,
		`{"id": %d, "name": "vlc", "description": "A cØØl multimedia player"}`)
	// List final products
	AssertAPIOK(t, router, "GET", productPrefix, "",
		s(`[{"id": %d, "name": "vlc", "description": "A cØØl multimedia player"}]`, id1))
}
