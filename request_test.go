package main

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestRequests(t *testing.T) {
	router := initTest(t)

	// Create product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create release
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))

	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// add a private key
	AssertAPIOK(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")

	// Sign release
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s"}`, finger1))

	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))
	// Create Channel2
	AssertAPIStatusOK(t, router, "POST", channelPrefix, s(`{"name": "test2", "product": %d}`, productID1))

	updateChannel1 := s("/api/v1/update/%s?channel=test1", "vlc")
	updateChannel2 := s("/api/v1/update/%s?channel=test2", "vlc")
	release1URL := s("/api/v1/release/%d/manifest", releaseID1)

	// no rule, no chocolate
	AssertAPIErrorNotFound(t, router, "GET", updateChannel2+"&os=Windows", "")

	// Create empty rule
	release1ChannelStr := s(`"release": %d, "channel": %d`, releaseID1, channelID1)
	ruleID := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 1000, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 1000, `+release1ChannelStr+` }`)
	fmt.Println("The ID: ", ruleID)

	// An empty rule matches anything
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	time.Sleep(50 * time.Millisecond) // wait for request stat store
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=macOS", "", release1URL)
	time.Sleep(50 * time.Millisecond) // wait for request stat store
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10", "", release1URL)
	time.Sleep(50 * time.Millisecond) // wait for request stat store
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_arch=x86_64", "", release1URL)
	time.Sleep(50 * time.Millisecond) // wait for request stat store
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.10", "", release1URL)
	time.Sleep(50 * time.Millisecond) // wait for request stat store

	// Get all 6 requests. retrieve createdAt times
	body := AssertAPIStatusOK(t, router, "GET", requestPrefix, "")
	var j map[string]interface{}
	json.Unmarshal([]byte(body), &j)
	var createdAt [6]string
	for i := 0; i < 6; i++ {
		createdAt[i] = j["result"].([]interface{})[i].(map[string]interface{})["created_at"].(string)
	}
	var requestStr [6]string
	requestStr[0] = s(`{"channel":"test1","os":"Windows","os_version":"","os_arch":"","product":"vlc","product_version":"3.0.10","ip_address":"","matched_rule":%d,"matched_release":%d,"created_at":"%s"}`, ruleID, releaseID1, createdAt[0])
	requestStr[1] = s(`{"channel":"test1","os":"Windows","os_version":"","os_arch":"x86_64","product":"vlc","product_version":"","ip_address":"","matched_rule":%d,"matched_release":%d,"created_at":"%s"}`, ruleID, releaseID1, createdAt[1])
	requestStr[2] = s(`{"channel":"test1","os":"Windows","os_version":"10","os_arch":"","product":"vlc","product_version":"","ip_address":"","matched_rule":%d,"matched_release":%d,"created_at":"%s"}`, ruleID, releaseID1, createdAt[2])
	requestStr[3] = s(`{"channel":"test1","os":"macOS","os_version":"","os_arch":"","product":"vlc","product_version":"","ip_address":"","matched_rule":%d,"matched_release":%d,"created_at":"%s"}`, ruleID, releaseID1, createdAt[3])
	requestStr[4] = s(`{"channel":"test1","os":"Windows","os_version":"","os_arch":"","product":"vlc","product_version":"","ip_address":"","matched_rule":%d,"matched_release":%d,"created_at":"%s"}`, ruleID, releaseID1, createdAt[4])
	requestStr[5] = s(`{"channel":"test2","os":"Windows","os_version":"","os_arch":"","product":"vlc","product_version":"","ip_address":"","created_at":"%s"}`, createdAt[5])
	require.JSONEq(t, s(`{"total":6,"limit":100,"offset":0,"result":[%s,%s,%s,%s,%s,%s]}`,
		requestStr[0], requestStr[1], requestStr[2], requestStr[3], requestStr[4], requestStr[5]),
		body, "Should get six requests")
	// Use limit filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?limit=2", "", s(`{"total":6,"limit":2,"offset":0,"result":[%s,%s]}`, requestStr[0], requestStr[1]))
	// Use offest filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?limit=2&offset=2", "", s(`{"total":6,"limit":2,"offset":2,"result":[%s,%s]}`, requestStr[2], requestStr[3]))
	// Use offest >> 1 filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?limit=2&offset=1000", "", `{"total":6,"limit":2,"offset":1000,"result":[]}`)
	// Use channel filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?filter[channel]=test2", "", s(`{"total":1,"limit":100,"offset":0,"result":[%s]}`, requestStr[5]))
	// Use os filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?filter[os]=macOS", "", s(`{"total":1,"limit":100,"offset":0,"result":[%s]}`, requestStr[3]))
	// Use os_arch filter
	AssertAPIOK(t, router, "GET", requestPrefix+"?filter[os_arch]=x86_64", "", s(`{"total":1,"limit":100,"offset":0,"result":[%s]}`, requestStr[1]))

	// Default Channel
	// No default channel on product -> no match if channel is not specified
	updateNoChannel := s("/api/v1/update/%s", "vlc")
	AssertAPIErrorNotFound(t, router, "GET", updateNoChannel, "")
	// Set channel1 to default channel
	AssertAPIOK(t, router, "PUT", s("%s/%d", productPrefix, productID1),
		s(`{"name": "vlc", "description": "A cØØl multimedia player", "default_channel": %d}`, channelID1),
		s(`{"id": %d, "name": "vlc", "description": "A cØØl multimedia player", "default_channel": %d}`, productID1, channelID1))
	// Now this matches the default channel
	AssertAPIRedirectOK(t, router, "GET", updateNoChannel, "", release1URL)

	// Test Cache limit
	for i := 0; i < 50; i++ {
		AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	}
	time.Sleep(50 * time.Millisecond) // wait for request stat store
	body = AssertAPIStatusOK(t, router, "GET", requestPrefix, "")
	json.Unmarshal([]byte(body), &j)
	require.Equal(t, testingConfigMaxRequests, int(j["total"].(float64)), "Number of requests should be limited to configuration settings")

}
