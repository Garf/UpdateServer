package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
)

var (
	configFile          string
	configMu                           = &sync.Mutex{}
	globalConfiguration *Configuration = nil
)

func init() {
	binaryAbsolute, err := filepath.Abs(os.Args[0])
	if err != nil {
		log.Fatal("not able to find absolute path to config")
	}

	defaultConfigPath := filepath.Join(filepath.Dir(binaryAbsolute), "config.json")

	flag.StringVar(&configFile, "config", defaultConfigPath, "Configuration file")
}

// Configuration reflects the structure of the configuration file format.
// The configuration file is a simple json file.
type Configuration struct {
	Admin struct {
		Username     string `json:"username"`
		PasswordHash string `json:"passwordhash"`
	} `json:"admin"`
	Redis struct {
		Host     string `json:"host"`
		Port     string `json:"port"`
		Password string `json:"password"`
		DBIndex  int    `json:"dbindex"`
	} `json:"redisinfo"`
	Cache struct {
		MaxRequests int64 `json:"maxrequests"`
	} `json:"cache"`
	HTTP struct {
		Verbose bool `json:"verbose"`
	} `json:"http"`
}

// Parse parses the given configuration filename
func (config *Configuration) Parse(filename string) error {
	ConfigJSON, err := ioutil.ReadFile(filename)

	if err != nil {
		return err
	}
	return json.Unmarshal(ConfigJSON, config)

}

// Get the global configuration setup
func Get() (*Configuration, error) {
	configMu.Lock()
	defer configMu.Unlock()
	if globalConfiguration != nil {
		return globalConfiguration, nil
	}
	config := &Configuration{}
	err := config.Parse(configFile)
	if err == nil {
		globalConfiguration = config
	}
	return config, err
}

// Set writes/overrides the global configuration
func Set(configuration *Configuration) {
	configMu.Lock()
	defer configMu.Unlock()
	globalConfiguration = configuration
}
