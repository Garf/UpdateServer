package main

import (
	"testing"
)

// Test public API to get release manifest and signature
func TestPublicRelease(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create a release
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))

	// Create signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// Sign release
	releaseSignature1 := getKeyFromFile("testdata/release_signature_with_this_key_is_for_tests_only.asc")
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s", "signature": "%s"}`, finger1, releaseSignature1))

	// Get release public manifest
	AssertAPIStringOK(t, router, "GET", s("/api/v1/release/%d/manifest", releaseID1), "",
		`{"title":"VLC 3.0.42","description":"","url":"http://lolilol","os":"Windows","os_version":"10","os_arch":"x86_64","product":"vlc","product_version":"3.0.42","hashes":{"sha256":"01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b","sha512":"be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}`)
	// Get release public signature
	// getKeyFromFile converts carriage returns to their \n representation, so to compare it, you have to convert it back
	AssertAPIStringOK(t, router, "GET", s("/api/v1/release/%d/manifest.asc", releaseID1), "",
		releaseSignature1)

}
