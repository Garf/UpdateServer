package main

import (
	"testing"
)

func TestConventions(t *testing.T) {
	router := initTest(t)

	prefix := "/api/v1/"

	// List OSArch
	AssertAPIOK(t, router, "GET", prefix+"osarch", "", `["x86", "x86_64", "armv7", "aarch64"]`)
	// List OS
	AssertAPIOK(t, router, "GET", prefix+"os", "", `["Windows", "macOS"]`)
	// List Operators
	AssertAPIOK(t, router, "GET", prefix+"operator", "", `["EQ", "NEQ", "LT", "LTE", "GT", "GTE"]`)
}
