
test:
	docker-compose run testapp
bench:
	docker-compose run benchapp
lint:
	docker-compose run lint
shell:
	docker-compose run --service-ports testapp bash
start:
	docker-compose up app -d
stop:
	docker-compose down
clean:
	docker-compose rm -fsv
coverage:
	go tool cover -html=cover.out -o coverage.html
redis:
	docker-compose exec redis redis-cli

.PHONY: test shell start stop clean coverage
