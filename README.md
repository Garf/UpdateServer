# UpdateServer

A fast-enough manifest-oriented update server written in golang.

Its main goal is to handle update requests from a client application.

## Installing
Nothing fancy, Just use the golang ```go get```

```
go get code.videolan.org/Garf/UpdateServer
```

## Configuration

The configuration comes in JSON format, you either edit the default config.json attached or give the path of the configuration file through flags 

Note: if you want to generate a password hash (to define an administration password), use the `-passwordhash` argument.

## Flags

The UpdateServer uses ```-port``` flag to customize port web application will be running on (8080 default)

and ```-config``` to add a path for the configuration file 
  
## Usage

### Launch the service

- `cd $GOPATH/src/code.videolan.org/Garf/UpdateServer`

- `go build` to build a binary UpdateServer

- `./UpdateServer` to run the Server

Optional flags: 
```
./UpdateServer -port 80
./UpdateServer -config $HOME/config.json
```

### First setup

- Go to `<host>/home.html`, `login`, then go to the `Administration` section (`<host>/admin/dashboard/home.html`)
- Create a new `Product`
  - Create a new `Channel`
  - Create a `Release`
  - Sign the release manifest
- Go back to the `channel` section
  - Add some `rule` that will match the clients request parameters
- Go the the `Requests` dashboard (`<host>/admin/dashboard/requests.html`) to monitor the clients requests

Note: you can test the server interaction from the `<host>/home.html` page.

### Release manifest signing

There are two methods to sign a release manifest.

- The **secure** method: add only public key, and sign the metadata locally (the interface will help you on that).
    
- The **less secure** method: add both public and private keys, so that the server will be able to auto-sign the releases.

**WARNING**: The **auto-sign** method is only meant for automatic release publication, like nightly/beta programs. Do not use this for official/stable releases.


## Terms and Update process

### Terms

- **client**: the application that asks for an update. It will ask the update server if there is a new release available.
- **product**: the application product. Includes all the version of the product on any platform (ex: VLC, Firefox, etc.)
- **channel**: a publication channel (ex: stable, beta, nightly, etc.). Each channel is related to only one *product*, and defines a release publication policy.
- **release**: a product release (ex: "VLC 4.12.42 for Windows 10 x86"). The release installer should be available on a specific URL (the update server does not host release files).
- **manifest**: a file describing a release, and used by the client to find the correct release installer to download. The manifest is a JSON file including at least:
  - the download URL of the release
  - the release file checksums
- **signature**: a release *manifest* PGP signature. It is used by the client to verify the manifest is authentic - **NOT** the release installer.
- **rule**: a channel update rule that points to a release. When a client is asking for a release manifest, the server will check the channel rules to try to match a proper release.

### Client/Server interaction

The client (the application that asks for an update) is supposed to request the server with the following process:

- The *client* Request the following URL with parameters reflecting its current state: `https://<host>/api/v1/update/<product>`
  - with the `POST` method, with the parameters as form data:
    Example : `https://<host>/api/v1/update/<product>' with form data: `channel=stable&os_arch=x86_64&os=Windows&os_version=10&product_version=3.0.12`
  - with the `GET` method, with the parameters as URL parameters:
    Example : `https://<host>/api/v1/update/<product>?channel=stable&os_arch=x86_64&os=Windows&os_version=10&product_version=3.0.12`

- The server will reply either by:
  - a `404 Not Found` response (if no proper release has been found),
  - or a `302 Found` with the URL to the proper available release manifest (ex: `<host>/api/v1/release/42/manifest`)

- The *client* then should download:
  - The found *manifest* (ex: `<host>/api/v1/release/42/manifest`)
  - THe *manifest* signature (ex: `<host>/api/v1/release/42/manifest.asc`)

- The *client* should verify the signature of the manifest

- Then the *client* should extract the information needed to actually download the release.

```mermaid

sequenceDiagram
    participant Client
    participant Server
    
    Client->>Server: GET/POST <host>/api/v1/update/<product>
    activate Server
    Server-->>Client: 302 Found <host>/api/v1/release/42/manifest
    deactivate Server

    Client->>Server: GET <host>/api/v1/release/42/manifest
    activate Server
    Server-->>Client: {"title": "Product 4.12.42", url="...", "hashes": {"sha512":...} ... }
    deactivate Server
    
    Client->>Server: GET <host>/api/v1/release/42/manifest.asc
    activate Server
    Server-->>Client: -----BEGIN PGP SIGNATURE----- ...
    deactivate Server

```

## Docker compose

Using the [docker-compose](https://docs.docker.com/compose/) tool will spawn a complete test instance of the `UpdateServer` service:

To launch the service:
```
/path/to/the/project$ docker-compose up
```

By default, the `updateserver` listens to the `8080` port.

To stop the service:
```
/path/to/the/project$ docker-compose down
```

To get an interactive golang+database environment:
```
/path/to/the/project$ docker-compose run --service-ports app bash
```

To get an interactive golang+database **test** environment:
```
/path/to/the/project$ docker-compose run --service-ports testapp bash
```

Note: you can use the makefile configuration to launch the common docker-compose commands:
```
$ make start # launches the main app and db
$ make stop  # stop all containers
$ make clean # stop and clean all containers
$ make test  # launch go test inside the test environment
$ make shell # launch an interactive container on the test environment
```

## Notes

### Why do you sign release manifests ?

In a ideal situation, the application *client* would be able to check the TLS validity of the `HTTPS` server and that would be sufficient - and yes, you need `HTTPS` connection to the server.

Unfortunately, there are some cases where the client cannot do that verification properly, for example when the operating system is old or not updated for a long time.

Signing the release manifest is a **mitigation** for such cases: it does not provide the same protection as a valid `HTTPs` connection, but it will protect the client from manifest forging (bad url, checksum, version, etc.).

For this reason, when trying to reach the server, the client *should*:
- ask for `HTTPS` connection and check the `TLS` validity
- **if** the `TLS` is considered invalid (unknown CA, expired/invalid date certificate, etc.), it should:
  - Send a warning message to the *user* explaining the issue and the consequences
  - If the *user* agrees, get the manifest and check its validity with the signature
- If the `TLS` is valid, the client can also check for the signature as well
- In case both `TLS` and `signature` are not valid, abort the update and send a message to the *user*.

Not having a proper `HTTPS` connection can imply:
- server usurpation, leading to:
  - downgrade attacks (if the client is not protected against this scenario, cf. below)
  - update deny (forcing the client to keep an old version of the application)
- client information leaking - the one sent to identify an update (OS version, architecture, product version, etc.)

### Manifest signing VS Release signing

The (installer) release **must** also be certified (signed) properly.

The release manifest signature is only meant to mitigate `HTTPS` failures.

### Downgrade version attack

The update server is not designed to protect from downgrade attacks: as the manifests and signatures are public, any attacker that will be able to usurpate the server will be able to send an old release manifest and valid signature.

The **client** should implement such a protection on its side (comparing the new release version and the old one, and launching the upgrade process only if the new release version is strictly greater than the old one).

## Performance

You can easily reach 2-3k "full" update requests per second on a modest machine. (*FULL*: `GET/POST` to get the manifest URL + `GET` the manifest and `GET` the signature).

```
goos: linux
goarch: amd64
pkg: code.videolan.org/videolan/updateserver
cpu: Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
BenchmarkBench/Request/Update-20-WORSTRules-4             	   2902	   379412 ns/op
BenchmarkBench/Request/FULL-20-WORSTRules-4               	   2430	   464117 ns/op
PASS
ok  	code.videolan.org/videolan/updateserver	32.951s
```

Here:
- `Update-20-WORSTRules` is a benchmark on 20 "worst" rules on a single channel (every check is parsed, and the 20 rules are parsed)
- `FULL-20-WORSTRules` is the same benchmark, including the manifest and signature retrieval

