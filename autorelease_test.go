package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAutoRelease(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create a release
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))
	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	AssertAPIStatusOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1))
	AssertAPIOK(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")
	// Get again
	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))
	// Create Rule
	ruleID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", rulePrefix,
		s(`{"priority": 10, "release": %d, "channel": %d}`, releaseID1, channelID1)))
	// List (empty)
	AssertAPIOK(t, router, "GET", s("%s/%d/autorelease", channelPrefix, channelID1), "", "[]")

	// Create AutoRelease
	autoreleaseStr := s(` "channel": %d, "rule": %d, "signature_key": "%s" `, channelID1, ruleID1, finger1)
	autoReleaseID1 := AssertAPIOKAndGetID(t, router, "POST", autoreleasePrefix,
		s(`{"description": "an autorelease", "schedule": "* * * * *", "url": "http://notthere/vlc/nightly-win32/", "release_base_title": "VLC 4.0 Nightly", "release_os": "macOS", "release_os_version": "10.14", "release_os_architecture": "aarch64", "release_product_default_version": "4.0.0", "release_filename_regex": "\\.*\\.exe", %s}`, autoreleaseStr),
		`{"id": %d, "description": "an autorelease", "schedule": "* * * * *", "url": "http://notthere/vlc/nightly-win32/", "release_base_title": "VLC 4.0 Nightly", "release_os": "macOS", "release_os_version": "10.14", "release_os_architecture": "aarch64", "release_product_default_version": "4.0.0", "release_filename_regex": "\\.*\\.exe", `+autoreleaseStr+`}`)
	// Must set a valid cron
	AssertAPIErrorInvalid(t, router, "POST", autoreleasePrefix,
		s(`{"description": "an autorelease", "schedule": "THISISNOTAVALIDCRONSCHEDULE", "url": "http://nightlies/vlc/nightly-win32/", "release_base_title": "VLC 4.0 Nightly", "release_os": "macOS", "release_os_version": "10.14", "release_os_architecture": "aarch64", "release_product_default_version": "4.0.0", "release_filename_regex": "\\.*\\.exe", %s}`, autoreleaseStr))
	// Check will fail because url is bad
	AssertAPIErrorGeneric(t, router, "PUT", s("%s/%d/check", autoreleasePrefix, autoReleaseID1), "")
	// Modify
	AssertAPIOK(t, router, "PUT", s("%s/%d", autoreleasePrefix, autoReleaseID1),
		s(`{"description": "an autorelease2", "schedule": "* * * * */5", "url": "http://nightlies/vlc/nightly-win64/", "release_base_title": "VLC 4.1 Nightly", "release_os": "Windows", "release_os_version": "10", "release_os_architecture": "x86_64", "release_product_default_version": "4.1.0", "release_filename_regex": "^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$", %s}`, autoreleaseStr),
		s(`{"id": %d, "description": "an autorelease2", "schedule": "* * * * */5", "url": "http://nightlies/vlc/nightly-win64/", "release_base_title": "VLC 4.1 Nightly", "release_os": "Windows", "release_os_version": "10", "release_os_architecture": "x86_64", "release_product_default_version": "4.1.0", "release_filename_regex": "^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$", %s}`, autoReleaseID1, autoreleaseStr))
	// Must set a valid cron
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%d", autoreleasePrefix, autoReleaseID1),
		s(`{"description": "an autorelease2", "schedule": "THISISNOTAVALIDCRONSCHEDULE", "url": "http://nightlies/vlc/nightly-win64/", "release_base_title": "VLC 4.1 Nightly", "release_os": "Windows", "release_os_version": "10", "release_os_architecture": "x86_64", "release_product_default_version": "4.1.0", "release_filename_regex": "^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$", %s}`, autoreleaseStr))
	// List again
	AssertAPIOK(t, router, "GET", s("%s/%d/autorelease", channelPrefix, channelID1), "",
		s(`[{"id": %d, "description": "an autorelease2", "schedule": "* * * * */5", "url": "http://nightlies/vlc/nightly-win64/", "release_base_title": "VLC 4.1 Nightly", "release_os": "Windows", "release_os_version": "10", "release_os_architecture": "x86_64", "release_product_default_version": "4.1.0", "release_filename_regex": "^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$", %s}]`, autoReleaseID1, autoreleaseStr))

	// Check
	AssertAPIStatusOK(t, router, "PUT", s("%s/%d/check", autoreleasePrefix, autoReleaseID1), "")
	// Get Rule Release
	var r struct {
		Release uint `json:"release"`
	}
	getStructFromJson(t, &r, AssertAPIStatusOK(t, router, "GET", s("%s/%d", rulePrefix, ruleID1), ""))
	generatedReleaseID := r.Release
	// Get Generated Release
	generatedReleaseBody := AssertAPIStatusOK(t, router, "GET", s("%s/%d", releasePrefix, generatedReleaseID), "")
	// retrieve signature (cannot be anticipated)
	var sig struct {
		Signature string `json:"signature"`
	}
	getStructFromJson(t, &sig, generatedReleaseBody)
	require.JSONEq(t, s(`{"id": %d,"title": "VLC 4.1 Nightly 4.0.0-dev-win64-e2a62698", "description": "VLC 4.1 Nightly 4.0.0-dev-win64-e2a62698", "url": "http://nightlies/vlc/nightly-win64/20200818-0434/vlc-4.0.0-dev-win64-e2a62698.exe", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "4.0.0-dev-win64-e2a62698", "signature_key": "705815352252CBF0F880B219259A183BA7C31427", "signature": "%s", "hashes": {"sha256": "d2ea3853068065f29475add4de0c86b7370170aa29e39d38b58fe487a5df6f74", "sha512": "d85c5493fcb22ea1c6174837d9393a14dfe78e6f1cc3c2cdd579e69f4c5a3f482210a51f965a8b313b22e89912ca7164997b5b47a8be2483f6c787b6d4a58bc5"} }`, generatedReleaseID, productID1, strings.ReplaceAll(sig.Signature, "\n", "\\n")), generatedReleaseBody, "Generated Release should have all the fields correct")

	// Delete
	AssertAPIOK(t, router, "DELETE", s("%s/%d", autoreleasePrefix, autoReleaseID1), "",
		s(`{"id": %d, "description": "an autorelease2", "schedule": "* * * * */5", "url": "http://nightlies/vlc/nightly-win64/", "release_base_title": "VLC 4.1 Nightly", "release_os": "Windows", "release_os_version": "10", "release_os_architecture": "x86_64", "release_product_default_version": "4.1.0", "release_filename_regex": "^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$", %s}`, autoReleaseID1, autoreleaseStr))

}
