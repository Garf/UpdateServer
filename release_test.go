package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRelease(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	product1ReleasePrefix := s("%s/%d/release", productPrefix, productID1)

	// List (empty)
	AssertAPIOK(t, router, "GET", product1ReleasePrefix, "", "[]")
	// Get that does not exists
	AssertAPIErrorNotFound(t, router, "GET", s("%s/%d", releasePrefix, 1242), "")
	// Create
	body := AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle < > \" $ & é ", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}`, productID1))
	releaseID1 := getIDFromJSON(t, body)
	require.JSONEq(t, s(`{"id": %d, "title": "testtitle < > \" $ & é ", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "signature": "", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1), body, s("POST %s", releasePrefix))
	// Cannot remove a product with an associated release
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%d", productPrefix, productID1), "")
	// Cannot create a release without a product
	AssertAPIErrorNotFound(t, router, "POST", releasePrefix,
		`{"title": "testtitle < > \" $ & é ", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`)
	// Cannot create a release with anything except description empty
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	AssertAPIErrorRequired(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	// Cannot create a release with a fictive OS
	AssertAPIErrorInvalid(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "Fenetre", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	// Cannot create a release with a fictive arch
	AssertAPIErrorInvalid(t, router, "POST", releasePrefix,
		s(`{"title": "testtitle", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_65", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	// Get
	AssertAPIOK(t, router, "GET", s("%s/%d", releasePrefix, releaseID1), "",
		s(`{"id": %d,"title": "testtitle < > \" $ & é ", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "signature": "", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1))
	// Get Manifest
	AssertAPIStringOK(t, router, "GET", s("%s/%d/manifest", releasePrefix, releaseID1), "",
		`{"title":"testtitle < > \" $ & é ","description":"","url":"http://lolilol","os":"Windows","os_version":"10","os_arch":"x86_64","product":"vlc","product_version":"3.0.42","hashes":{"sha256":"01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b","sha512":"be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}`)
	// Modify
	AssertAPIOK(t, router, "PUT", s("%s/%d", releasePrefix, releaseID1),
		s(`{"id": %d,"title": "testtitle2", "description": "yolo", "url": "http://lolilol2", "os": "macOS", "os_version": "X", "os_arch": "x86", "product": %d, "product_version": "3.0.43", "hashes": {"sha256": "42ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "42688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1),
		s(`{"id": %d,"title": "testtitle2", "description": "yolo", "url": "http://lolilol2", "os": "macOS", "os_version": "X", "os_arch": "x86", "product": %d, "product_version": "3.0.43", "signature": "", "hashes": {"sha256": "42ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "42688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1))
	// Cannot Modify that does not exists
	AssertAPIErrorNotFound(t, router, "PUT", s("%s/%d", releasePrefix, releaseID1-1),
		s(`{"id": %d,"title": "testtitle2", "description": "yolo", "url": "http://lolilol2", "os": "macOS", "os_version": "X", "os_arch": "x86", "product": %d, "product_version": "3.0.43", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1-1, productID1))
	// Delete
	AssertAPIOK(t, router, "DELETE", s("%s/%d", releasePrefix, releaseID1), "",
		s(`{"id": %d,"title": "testtitle2", "description": "yolo", "url": "http://lolilol2", "os": "macOS", "os_version": "X", "os_arch": "x86", "product": %d, "product_version": "3.0.43", "signature": "", "hashes": {"sha256": "42ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "42688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1))
	// Cannot Delete again
	AssertAPIErrorNotFound(t, router, "DELETE", s("%s/%d", releasePrefix, releaseID1), "")

	// Signing process
	// Recreate the release
	body = AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1))
	releaseID1 = getIDFromJSON(t, body)
	// Create signatures without a private key
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	pubkey2 := getKeyFromFile("testdata/this_key_is_for_tests_only2.pub")
	finger2 := "1EBAA20875557370D4E845A8E47DA2BA7935DEAF"
	// Release signature generated from: echo -n '{"title":"VLC 3.0.42","description":"","url":"http://lolilol","os":"Windows","os_version":"10","os_arch":"x86_64","product":"vlc","product_version":"3.0.42","hashes":{"sha256":"01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b","sha512":"be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}' | gpg --default-key 705815352252CBF0F880B219259A183BA7C31427 --detach-sign -a
	releaseSignature1 := getKeyFromFile("testdata/release_signature_with_this_key_is_for_tests_only.asc")
	releaseSignature2 := getKeyFromFile("testdata/release_signature_with_this_key_is_for_tests_only2.asc")
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey2),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey2, finger2))

	AssertAPIOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s", "signature": "%s"}`, finger1, releaseSignature1),
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}, "signature": "%s", "signature_key": "%s"}`, releaseID1, productID1, releaseSignature1, finger1))

	// Cannot remove a signaturekey with an associated release
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "")
	// Cannot sign with a signature from another key
	AssertAPIErrorInvalid(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s", "signature": "%s"}`, finger1, releaseSignature2))
	// Cannot sign without a signature
	AssertAPIErrorInvalid(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s"}`, finger1))
	// Can sign with another key
	AssertAPIOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s", "signature": "%s"}`, finger2, releaseSignature2),
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}, "signature": "%s", "signature_key": "%s"}`, releaseID1, productID1, releaseSignature2, finger2))
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%d", releasePrefix, releaseID1), "",
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "signature": "%s", "signature_key": "%s", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1, releaseSignature2, finger2))
	// Modifying the release removes the signature
	AssertAPIOK(t, router, "PUT", s("%s/%d", releasePrefix, releaseID1),
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "a new description", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1),
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "a new description", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "signature": "", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1))
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%d", releasePrefix, releaseID1), "",
		s(`{"id": %d, "title": "VLC 3.0.42", "description": "a new description", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "signature": "", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, releaseID1, productID1))
	// Modify signature1: add the private key
	AssertAPIOK(t, router, "POST", s(`%s/%s/privatekey`, signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")
	// Can sign with a priv/pub couple key
	// The signature can be valid and change its content, so let's only check the return status
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s"}`, finger1))
	// Delete again
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", releasePrefix, releaseID1), "")
	// Now we can remove product
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", productPrefix, productID1), "")
	// Now we can remove signaturekey
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "")
}
