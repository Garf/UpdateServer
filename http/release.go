package http

import (
	"net/http"
	"strconv"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// Release REST API

// APIListReleases List All Releases
func APIListReleases(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	releases, err := core.GetReleases(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, releases)
}

// APINewRelease Create new release
func APINewRelease(c *gin.Context) {
	var release model.Release
	c.BindJSON(&release)
	if err := core.NewRelease(&release); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, release)
}

func stringToUint(s string) (uint, error) {
	u, err := strconv.ParseUint(s, 10, 0)
	if err != nil {
		return 0, model.ErrorInvalid("id", err.Error())
	}
	return uint(u), nil
}

// APIGetRelease gets a release by id
func APIGetRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	release, err := core.GetRelease(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, release)
}

// APIGetReleaseManifest sends the release manifest in text/string format
func APIGetReleaseManifest(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	manifest, err := core.GetReleaseManifest(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	data := []byte(manifest)
	c.Data(http.StatusOK, "text/string", data)
}

// APIGetReleaseSignature sends the release signature
func APIGetReleaseSignature(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	signature, err := core.GetReleaseSignature(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	data := []byte(signature)
	c.Data(http.StatusOK, "text/string", data)
}

// APIModifyRelease Modify existing release
func APIModifyRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var release model.Release
	c.BindJSON(&release)
	release.ID = id
	if err := core.ModifyRelease(&release); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, release)
}

// APIDeleteRelease deletes a release
func APIDeleteRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	release, err := core.DeleteRelease(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, release)
}

// SignRequest reflects the json format to the SignRelease request
type SignRequest struct {
	Fingerprint string `json:"fingerprint"`
	Signature   string `json:"signature,omitempty"`
}

// APISignRelease checks and store the signature of a release
func APISignRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var req SignRequest
	c.BindJSON(&req)
	release, err := core.SignRelease(id, req.Fingerprint, req.Signature)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, release)
}
