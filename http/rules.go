package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// Rule REST API

// APIListRules List All Rules for a channel
func APIListRules(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	rules, err := core.GetRules(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rules)
}

// APINewRule Create new rule for a channel
func APINewRule(c *gin.Context) {
	var rule model.Rule
	c.BindJSON(&rule)
	if err := core.NewRule(&rule); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}

// APIGetRule gets a rule by id
func APIGetRule(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	rule, err := core.GetRule(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}

// APIModifyRule Modify existing channel
func APIModifyRule(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var rule model.Rule
	c.BindJSON(&rule)
	rule.ID = id
	if err := core.ModifyRule(&rule); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}

// APIDeleteRule deletes a rule
func APIDeleteRule(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	rule, err := core.DeleteRule(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}

// APIUpRule raise a rule priority
func APIUpRule(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	rule, err := core.UpRule(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}

// APIDownRule lowers a rule priority
func APIDownRule(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	rule, err := core.DownRule(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, rule)
}
