package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

func APIListAutoReleases(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	autoreleases, err := core.GetAutoReleases(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, autoreleases)
}

func APINewAutoRelease(c *gin.Context) {
	var autorelease model.AutoRelease
	c.BindJSON(&autorelease)
	if err := core.NewAutoRelease(&autorelease); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, autorelease)
}

func APIGetAutoRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	autorelease, err := core.GetAutoRelease(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, autorelease)
}

func APIModifyAutoRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var autorelease model.AutoRelease
	c.BindJSON(&autorelease)
	autorelease.ID = id
	if err := core.ModifyAutoRelease(&autorelease); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, autorelease)
}

func APIDeleteAutoRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	autorelease, err := core.DeleteAutorelease(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, autorelease)
}

func APICheckNewRelease(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	if err := core.CheckNewRelease(id); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}
