package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// Channel REST API

// APIListChannels lists All Channels
func APIListChannels(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	channels, err := core.GetChannels(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, channels)
}

// APINewChannel creates a new channel
func APINewChannel(c *gin.Context) {
	var channel model.Channel
	c.BindJSON(&channel)
	if err := core.NewChannel(&channel); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, channel)
}

// APIGetChannel provides a channel per id
func APIGetChannel(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	channel, err := core.GetChannel(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, channel)
}

// APIModifyChannel modifies existing channel
func APIModifyChannel(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var channel model.Channel
	c.BindJSON(&channel)
	channel.ID = id
	if err := core.ModifyChannel(&channel); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, channel)
}

// APIDeleteChannel deletes a channel
func APIDeleteChannel(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	channel, err := core.DeleteChannel(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, channel)
}
