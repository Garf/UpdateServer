package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// SignatureKey REST API

// APIListSignatureKeys List All SignatureKeys
func APIListSignatureKeys(c *gin.Context) {
	keys, err := core.GetSignatureKeys()
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, keys)
}

// APINewSignatureKey Create new key
func APINewSignatureKey(c *gin.Context) {
	var key model.SignatureKey
	c.BindJSON(&key)
	if err := core.NewSignatureKey(&key); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, key)
}

// APIGetSignatureKey gets a SignatureKey by fingerprint
func APIGetSignatureKey(c *gin.Context) {
	key, err := core.GetSignatureKey(c.Param("fingerprint"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, key)
}

// APIModifySignatureKey modifies an existing key
func APIModifySignatureKey(c *gin.Context) {
	var key model.SignatureKey
	c.BindJSON(&key)
	// check key exists
	if err := core.ModifySignatureKey(c.Param("fingerprint"), &key); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, key)
}

// APISetSignaturePrivateKey stores a private key
func APISetSignaturePrivateKey(c *gin.Context) {
	var privateKeyRequest struct {
		PrivateKey string `json:"privatekey"`
	}
	c.BindJSON(&privateKeyRequest)
	if err := core.SetSignaturePrivateKey(c.Param("fingerprint"), privateKeyRequest.PrivateKey); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func APIRemoveSignaturePrivateKey(c *gin.Context) {
	if err := core.RemoveSignaturePrivateKey(c.Param("fingerprint")); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

// APIDeleteSignatureKey delete a key
func APIDeleteSignatureKey(c *gin.Context) {
	key, err := core.DeleteSignatureKey(c.Param("fingerprint"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, key)
}
