package http

import (
	"log"
	"net/http"
	"strings"

	"code.videolan.org/videolan/updateserver/cache"
	"code.videolan.org/videolan/updateserver/config"
	"code.videolan.org/videolan/updateserver/utils"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/time/rate"
)

const (
	loginMaxRequestsPerSec      = 1
	loginMaxBurstRequestsPerSec = 3
)

// AuthRequired is a gin middleware that denies access to a HTTP request if the client does not have a valid authentication cookie
func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		sessionID := session.Get("session")
		if sessionID != nil {
			if s := cache.GetSession(sessionID.(string)); s != nil {
				// Session is present in cache, user is authenticated
				// Continue down the chain to handler etc
				c.Next()
				return
			}
		}
		// cannot find an existing user session
		// Redirect to login page
		c.HTML(http.StatusUnauthorized, "unauthorized.html", gin.H{})
		c.Abort()
	}
}

var limiter = rate.NewLimiter(loginMaxRequestsPerSec, loginMaxBurstRequestsPerSec)

func login(c *gin.Context) {
	// Hash is slow and resource consuming, so we limit the number of requests
	if !limiter.Allow() {
		c.JSON(http.StatusTooManyRequests, gin.H{"error": "Too many simultaneous requests"})
		return
	}

	session := sessions.Default(c)
	var creds struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	c.BindJSON(&creds)

	if strings.Trim(creds.Username, " ") == "" || strings.Trim(creds.Password, " ") == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
		return
	}
	conf, err := config.Get()
	if err != nil {
		panic(err)
	}
	if creds.Username != conf.Admin.Username {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
		return
	}
	ok, err := utils.ComparePasswordAndHash(creds.Password, conf.Admin.PasswordHash)
	if err != nil {
		log.Println("ERROR in Hash comparison:", err)
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
		return
	}
	if ok {
		s := cache.Session{User: conf.Admin.Username}
		sessionID := uuid.New().String()
		cache.SetSession(sessionID, s)
		session.Set("session", sessionID)
		session.Options(sessions.Options{
			SameSite: http.SameSiteStrictMode,
			Path:     "/",
		})
		err := session.Save()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to generate session token"})
		} else {
			c.JSON(http.StatusOK, gin.H{"message": "Successfully authenticated user"})
		}
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
	}
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	sessionID := session.Get("session")
	if sessionID == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid session token"})
	} else {
		cache.DeleteSession(sessionID.(string))
		session.Clear()
		session.Save()
		c.JSON(http.StatusOK, gin.H{"message": "Successfully logged out"})
	}
}
