package http

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"code.videolan.org/videolan/updateserver/utils"
	"github.com/gin-gonic/gin"
)

// PaginatedRequestResp reflects the json response structure to paginated request request
type PaginatedRequestResp struct {
	Total  uint                  `json:"total"`
	Limit  uint                  `json:"limit"`
	Offset uint                  `json:"offset"`
	Result []model.UpdateRequest `json:"result"`
}

// APIListPaginatedRequests list the requests (paginated)
func APIListPaginatedRequests(c *gin.Context) {
	limit, err := stringToUint(c.DefaultQuery("limit", "100"))
	if err != nil {
		limit = 100
	}
	offset, err := stringToUint(c.DefaultQuery("offset", "0"))
	if err != nil {
		offset = 0
	}
	filterMap := c.QueryMap("filter")
	var filter model.UpdateRequest
	if channel, ok := filterMap["channel"]; ok {
		filter.Channel = channel
	}
	if os, ok := filterMap["os"]; ok {
		filter.OS = utils.OS(os)
	}
	if arch, ok := filterMap["os_arch"]; ok {
		filter.OsArchitecture = utils.OSArch(arch)
	}
	requests, count, err := core.GetPaginatedRequests(filter, limit, offset)
	r := PaginatedRequestResp{Total: count, Limit: limit, Offset: offset, Result: *requests}
	c.JSON(http.StatusOK, r)
}

// Update returns the "actual" response to an app client.
// based on the information sent by a client, Update will redirect to the most appropriate release manifest (or 404 if not found)
func Update(c *gin.Context) {
	var request model.UpdateRequest
	c.Bind(&request)
	request.Product = c.Param("product_name")
	ForwardHeader := c.Request.Header.Get("X-Forwarded-For")
	ForwardedFields := strings.Split(ForwardHeader, ",")
	if len(ForwardedFields) > 0 {
		request.IP = ForwardedFields[0]
	} else {
		request.IP = ""
	}
	request.CreatedAt = time.Now()
	request.MatchedRuleID = nil
	request.MatchedReleaseID = nil
	// push the request in cache at the end of the Update function
	// Do not wait for the result to
	defer func() {
		go core.AddRequest(&request)
	}()

	// GetRulesCache will never include rules that point to an unsigned release
	rules, err := core.GetRulesCache(request.Product, request.Channel)
	if err != nil {
		SendErrorToHTTP(c, model.ErrorNotFound())
		return
	}

	for _, rule := range *rules {
		ok, err := rule.Check(&request)
		if err != nil {
			SendErrorToHTTP(c, model.ErrorGeneric(err.Error()))
			return
		}
		if ok {
			// Found a matching rule
			// Redirect to the public API for release manifests
			ruleID := rule.ID
			releaseID := rule.ReleaseID
			request.MatchedRuleID = &ruleID
			request.MatchedReleaseID = &releaseID
			c.Redirect(http.StatusFound, fmt.Sprintf("/api/v1/release/%d/manifest", rule.ReleaseID))
			return
		}
	}
	// Not found
	SendErrorToHTTP(c, model.ErrorNotFound())
}
