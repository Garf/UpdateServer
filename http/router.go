package http

import (
	"crypto/rand"
	"io"
	"net/http"
	"os"

	"code.videolan.org/videolan/updateserver/config"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func generateRandomKey(length int) []byte {
	k := make([]byte, length)
	if _, err := io.ReadFull(rand.Reader, k); err != nil {
		panic(err.Error())
	}
	return k
}

// RouterInit function initiate the gin engine main handler
func RouterInit(configuration *config.Configuration) *gin.Engine {
	if !configuration.HTTP.Verbose {
		f, _ := os.Create(os.DevNull)
		gin.DefaultWriter = io.MultiWriter(f)
	}
	router := gin.Default()
	// Add a session management to the router
	store := cookie.NewStore(generateRandomKey(32), generateRandomKey(32))
	router.Use(sessions.Sessions("UpdateServerSession", store))

	router.LoadHTMLFiles("html/unauthorized.html")

	// Front
	router.StaticFile("/favicon.ico", "./html/static/favicon.ico")
	router.Static("/third", "./html/third")
	router.Static("/components", "./html/components")
	router.StaticFile("/home.html", "./html/home.html")
	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/home.html")
	})

	// Login/Logout
	router.StaticFile("/login.html", "./html/login.html")
	router.POST("/login", login)
	router.GET("/logout", logout)

	// Login/Logout

	// Public API
	pub := router.Group("/api/v1")
	{
		// Public API

		pub.GET("/product", APIListProducts)
		pub.GET("/product/:id/channel", APIListChannels)
		pub.GET("/update/:product_name", Update)
		pub.POST("/update/:product_name", Update)
		pub.GET("/release/:id/manifest", APIGetReleaseManifest)
		pub.GET("/release/:id/manifest.asc", APIGetReleaseSignature)
		// OS & Arch conventions
		pub.GET("/osarch", APIListOSArch)
		pub.GET("/os", APIListOS)
		pub.GET("/operator", APIListOperator)
	}

	// Admin
	//admin := router.Group("/admin", gin.BasicAuth(gin.Accounts{"admin": "admin"}))
	admin := router.Group("/admin")
	admin.Use(AuthRequired())
	admin.Static("/dashboard", "./html/admin")
	adminAPIV1 := admin.Group("/api/v1")
	{
		// Admin API

		// Product
		adminAPIV1.GET("/product", APIListProducts)
		adminAPIV1.GET("/product/:id", APIGetProduct)
		adminAPIV1.POST("/product", APINewProduct)
		adminAPIV1.PUT("/product/:id", APIModifyProduct)
		adminAPIV1.DELETE("/product/:id", APIDeleteProduct)
		// SignatureKey
		adminAPIV1.GET("/signaturekey", APIListSignatureKeys)
		adminAPIV1.POST("/signaturekey", APINewSignatureKey)
		adminAPIV1.GET("/signaturekey/:fingerprint", APIGetSignatureKey)
		adminAPIV1.PUT("/signaturekey/:fingerprint", APIModifySignatureKey)
		adminAPIV1.POST("/signaturekey/:fingerprint/privatekey", APISetSignaturePrivateKey)
		adminAPIV1.DELETE("/signaturekey/:fingerprint/privatekey", APIRemoveSignaturePrivateKey)
		adminAPIV1.DELETE("/signaturekey/:fingerprint", APIDeleteSignatureKey)
		// Channel
		adminAPIV1.GET("/channel/:id", APIGetChannel)
		adminAPIV1.POST("/channel", APINewChannel)
		adminAPIV1.PUT("/channel/:id", APIModifyChannel)
		adminAPIV1.DELETE("/channel/:id", APIDeleteChannel)
		// Channel list is deeply linked to product
		adminAPIV1.GET("/product/:id/channel", APIListChannels)
		// Release
		adminAPIV1.GET("/release/:id", APIGetRelease)
		adminAPIV1.GET("/release/:id/manifest", APIGetReleaseManifest)
		adminAPIV1.POST("/release", APINewRelease)
		adminAPIV1.PUT("/release/:id", APIModifyRelease)
		adminAPIV1.DELETE("/release/:id", APIDeleteRelease)
		adminAPIV1.POST("/release/:id/sign", APISignRelease)
		// Release list is deeply linked to Product
		adminAPIV1.GET("/product/:id/release", APIListReleases)
		// Rule
		adminAPIV1.GET("/rule/:id", APIGetRule)
		adminAPIV1.POST("/rule", APINewRule)
		adminAPIV1.PUT("/rule/:id", APIModifyRule)
		adminAPIV1.PUT("/rule/:id/up", APIUpRule)
		adminAPIV1.PUT("/rule/:id/down", APIDownRule)
		adminAPIV1.DELETE("/rule/:id", APIDeleteRule)
		// Rule list is deeply linked to Channel
		adminAPIV1.GET("/channel/:id/rule", APIListRules)
		// Requests
		adminAPIV1.GET("/request/paginated", APIListPaginatedRequests)
		// AutoRelease
		adminAPIV1.GET("/autorelease/:id", APIGetAutoRelease)
		adminAPIV1.POST("/autorelease", APINewAutoRelease)
		adminAPIV1.PUT("/autorelease/:id", APIModifyAutoRelease)
		adminAPIV1.DELETE("/autorelease/:id", APIDeleteAutoRelease)
		adminAPIV1.PUT("/autorelease/:id/check", APICheckNewRelease)
		// AutoRelease list is deeply linked to Channel
		adminAPIV1.GET("/channel/:id/autorelease", APIListAutoReleases)
	}

	return router
}
