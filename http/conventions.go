package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/utils"
	"github.com/gin-gonic/gin"
)

// APIListOSArch list all possible architectures
func APIListOSArch(c *gin.Context) {
	var osarch utils.OSArch
	c.JSON(http.StatusOK, osarch.List())
}

// APIListOS list all possible OS
func APIListOS(c *gin.Context) {
	var os utils.OS
	c.JSON(http.StatusOK, os.List())
}

// APIListOperator list all possible operators
func APIListOperator(c *gin.Context) {
	var op utils.Operator
	c.JSON(http.StatusOK, op.List())
}
