package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// APIListProducts List All Products
func APIListProducts(c *gin.Context) {
	products, err := core.GetProducts()
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, products)
}

// APINewProduct Create new product
func APINewProduct(c *gin.Context) {
	var product model.Product
	c.BindJSON(&product)
	if err := core.NewProduct(&product); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, product)
}

// APIGetProduct gets a product by id
func APIGetProduct(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	product, err := core.GetProduct(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, product)
}

// APIModifyProduct modifies existing product
func APIModifyProduct(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	var product model.Product
	c.BindJSON(&product)
	product.ID = id
	if err := core.ModifyProduct(&product); err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, product)
}

// APIDeleteProduct deletes a product
func APIDeleteProduct(c *gin.Context) {
	id, err := stringToUint(c.Param("id"))
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	product, err := core.DeleteProduct(id)
	if err != nil {
		SendErrorToHTTP(c, err)
		return
	}
	c.JSON(http.StatusOK, product)
}
