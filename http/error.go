package http

import (
	"net/http"

	"code.videolan.org/videolan/updateserver/model"
	"github.com/gin-gonic/gin"
)

// SendErrorToHTTP converts an error to a HTTP response.
// a model.Error sends its own HTTP status code
func SendErrorToHTTP(c *gin.Context, err error) {
	if modelError, ok := err.(model.Error); ok {
		c.JSON(modelError.HTTPStatus, modelError)
	} else {
		c.JSON(http.StatusInternalServerError, model.ErrorGeneric(err.Error()))
	}
}
