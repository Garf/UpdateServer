package main

import (
	"io/ioutil"
	nethttp "net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestLogin(t *testing.T) {
	router := initTest(t)

	bytes, err := ioutil.ReadFile("html/unauthorized.html")
	if err != nil {
		panic("Cannot read html/unauthorized.html")
	}
	unauthorizedPage := string(bytes)

	// Remove old global cookie
	oldCookie := globalCookie
	globalCookie = ""

	// Admin API needs a login cookie
	AssertRequest(t, router, "GET", productPrefix, "", nethttp.StatusUnauthorized, unauthorizedPage)
	//AssertAPIFoundOK(t, router, "GET", productPrefix, "", "/login.html")
	// After a burst of 3 requests -> TooManyRequests Error
	time.Sleep(1 * time.Second)
	AssertAPIErrorUnauthorized(t, router, "POST", loginPrefix, `{"username": "getmein", "password": "pleaaaaase"}`)
	AssertAPIErrorUnauthorized(t, router, "POST", loginPrefix, `{"username": "getmein", "password": "pleaaaaase"}`)
	AssertAPIErrorUnauthorized(t, router, "POST", loginPrefix, `{"username": "getmein", "password": "pleaaaaase"}`)
	AssertAPIErrorTooManyRequests(t, router, "POST", loginPrefix, `{"username": "getmein", "password": "pleaaaaase"}`)
	// Wait for one second
	time.Sleep(1 * time.Second)
	// Login and get cookie
	cookie := AssertAPIOKAndGetCookie(t, router, "POST", loginPrefix, s(`{"username": "%s", "password": "%s"}`, username, password),
		`{"message":"Successfully authenticated user"}`)
	require.Truef(t, strings.HasPrefix(cookie, cookiePrefix), "Should retrieve a session cookie")
	// Set the cookie
	globalCookie = cookie
	// List Products (empty)
	AssertAPIOK(t, router, "GET", productPrefix, "", "[]")
	// Logout
	AssertAPIOK(t, router, "GET", logoutPrefix, "", `{"message": "Successfully logged out"}`)
	// Unauthorized again
	//AssertAPIFoundOK(t, router, "GET", productPrefix, "", "/login.html")
	AssertRequest(t, router, "GET", productPrefix, "", nethttp.StatusUnauthorized, unauthorizedPage)
	// remove globalCookie
	globalCookie = ""
	// Unauthorized again again
	//AssertAPIFoundOK(t, router, "GET", productPrefix, "", "/login.html")
	AssertRequest(t, router, "GET", productPrefix, "", nethttp.StatusUnauthorized, unauthorizedPage)
	// Put back the old cookie
	globalCookie = oldCookie
}
