#!/bin/sh -x

set -e

# Third party libraries dir
THIRD_DIR=./html/third

get_third () {
    url=$1
    destination=$2
    wget -O "${THIRD_DIR}/$2" "$1"
}

# Prepare
mkdir -p "${THIRD_DIR}/css"
mkdir -p "${THIRD_DIR}/js"

# Boostrap dependencies
get_third "https://code.jquery.com/jquery-3.3.1.slim.min.js" "js/jquery.min.js"
get_third "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" "js/popper.min.js"
# Bootstrap
get_third "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" "css/bootstrap.min.css"
get_third "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css.map" "css/bootstrap.min.css.map"
get_third "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" "js/bootstrap.min.js"
# Vue
get_third "https://unpkg.com/vue@2.6.10/dist/vue.min.js" "js/vue.min.js"
