package cache

import (
	"sync"

	"code.videolan.org/videolan/updateserver/config"
	"code.videolan.org/videolan/updateserver/model"
)

// Session includes user session information
type Session struct {
	User string
}

var (
	requestsMu          = &sync.RWMutex{}
	requests            = make([](*model.UpdateRequest), 0)
	requestsMax   int64 = 1000
	sessionMap          = sync.Map{}
	privateKeyMap       = sync.Map{}
)

// Config configures the cache module with the given config
func Config(c *config.Configuration) {
	requestsMu.Lock()
	defer requestsMu.Unlock()
	requestsMax = c.Cache.MaxRequests
}

// GetPaginatedRequests get requests from cache, filtered and paginated.
// Returns the requested array and the total number of requests that match the filter
func GetPaginatedRequests(filter model.UpdateRequest, limit uint, offset uint) ([](*model.UpdateRequest), uint) {
	requestsMu.RLock()
	defer requestsMu.RUnlock()
	res := make([](*model.UpdateRequest), 0)
	var innerIndex uint = 0
	for _, request := range requests {
		if filter.Channel != "" && filter.Channel != request.Channel {
			continue
		}
		if filter.OS != "" && filter.OS != request.OS {
			continue
		}
		if filter.OsArchitecture != "" && filter.OsArchitecture != request.OsArchitecture {
			continue
		}
		// request is matching filters
		if innerIndex >= offset && len(res) < int(limit) {
			res = append(res, request)
		}
		innerIndex++
	}
	return res, innerIndex
}

// AddRequest adds a request to the cached requests
func AddRequest(request *model.UpdateRequest) {
	requestsMu.Lock()
	defer requestsMu.Unlock()
	if int64(len(requests)) == requestsMax {
		requests = requests[:len(requests)-1]
	}
	requests = append([](*model.UpdateRequest){request}, requests...)
}

// ClearRequests clears the requests cache
func ClearRequests() {
	requestsMu.Lock()
	defer requestsMu.Unlock()
	requests = make([](*model.UpdateRequest), 0)
}

// GetSession returns a cached session if found, nil otherwise
func GetSession(id string) *Session {
	s, ok := sessionMap.Load(id)
	if ok {
		session, ok := s.(Session)
		if ok {
			return &session
		}
	}
	return nil
}

// SetSession adds a (id -> session) tuple to the cached session map
func SetSession(id string, session Session) {
	sessionMap.Store(id, session)
}

// DeleteSession removes a session from the cached session map
func DeleteSession(id string) {
	sessionMap.Delete(id)
}

// CleaClearSessions removes all sessions stored in cache
func ClearSessions() {
	sessionMap.Range(func(key interface{}, value interface{}) bool {
		sessionMap.Delete(key)
		return true
	})
}

// GetPrivateKey safely provides private keys from a signature key stored in cache.
// returns (privateKey, true) or ("", false)
func GetPrivateKey(fingerprint string) (string, bool) {
	privateKey, ok := privateKeyMap.Load(fingerprint)
	if ok {
		return privateKey.(string), ok
	}
	return "", false
}

// SetPrivateKey set the private key in cache.
// A privateKey will not be stored in file - only in memory
func SetPrivateKey(fingerprint string, privateKey string) {
	privateKeyMap.Store(fingerprint, privateKey)
}

// DeletePrivateKey removes a private key from cache
func DeletePrivateKey(fingerprint string) {
	privateKeyMap.Delete(fingerprint)
}

// CleaClearPrivateKeys removes all privatekeys from cache
func ClearPrivateKeys() {
	privateKeyMap.Range(func(key interface{}, value interface{}) bool {
		privateKeyMap.Delete(key)
		return true
	})
}
