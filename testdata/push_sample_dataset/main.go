package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const root_url = "http://localhost:8080/admin/api/v1"
const login_url = "http://localhost:8080/login"

var cookie = ""

func askCredentials() (string, string) {
	reader := bufio.NewReader(os.Stdin)
	fd := int(os.Stdin.Fd())

	fmt.Print("Enter Username: ")
	username, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	fmt.Print("Enter Password: ")
	bytePassword, err := terminal.ReadPassword(fd)
	if err != nil {
		panic(err)
	}
	password := string(bytePassword)

	return strings.TrimSpace(username), strings.TrimSpace(password)
}

func request(method string, url string, body string, verbose bool) (int, string, string) {
	client := &http.Client{}
	req, _ := http.NewRequest(method, url, strings.NewReader(body))
	if cookie != "" {
		req.Header.Set("Cookie", cookie)
	}
	resp, _ := client.Do(req)
	resp_body, _ := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if verbose {
		fmt.Println(method, " ", url, ": ", resp.StatusCode, " ", string(resp_body))
	}
	return resp.StatusCode, string(resp_body), resp.Header.Get("Set-Cookie")
}

func requestLogin(username string, password string) (int, string, string) {
	return request("POST", login_url, fmt.Sprintf(`{"username": "%s", "password": "%s"}`, username, password), false)
}

func requestAPI(method string, relative_url string, body string) (int, string, string) {
	return request(method, root_url+relative_url, body, true)
}

func getKeyFromFile(filename string) string {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("Cannot read key")
	}
	return strings.ReplaceAll(string(bytes), "\n", "\\n")
}

func getIdFromJson(jsonString string) (uint, error) {
	var s struct {
		Id uint `json:"id"`
	}
	if err := json.Unmarshal([]byte(jsonString), &s); err != nil {
		return 0, err
	}
	return s.Id, nil
}

func main() {
	fmt.Println("Pushing a sample dataset to updateserver")

	username, password := askCredentials()
	fmt.Println()
	// Get cookie
	var code int
	code, _, cookie = requestLogin(username, password)
	if code != 200 {
		panic("Login failed!")
	}

	// Create a product
	_, body, _ := requestAPI("POST", "/product", `{"name": "vlc", "description": "A cØØl multimedia player"}`)
	productId1, _ := getIdFromJson(body)

	// Create two Channels
	_, body, _ = requestAPI("POST", "/channel", fmt.Sprintf(`{"name": "stable", "product": %d}`, productId1))
	channelId1, _ := getIdFromJson(body)
	requestAPI("POST", "/channel", fmt.Sprintf(`{"name": "beta", "product": %d}`, productId1))

	// Set channel1 to default channel
	requestAPI("PUT", fmt.Sprintf("/product/%d", productId1), fmt.Sprintf(`{"name": "vlc", "description": "A cØØl multimedia player", "default_channel": %d}`, channelId1))

	// Create two releases
	_, body, _ = requestAPI("POST", "/release",
		fmt.Sprintf(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}`, productId1))
	releaseId1, _ := getIdFromJson(body)

	requestAPI("POST", "/release",
		fmt.Sprintf(`{"title": "testtitle2", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.12", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"}}`, productId1))

	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	//privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	requestAPI("POST", "/signaturekey",
		fmt.Sprintf(`{"pubkey": "%s", "privatekey": "", "description": "a description"}`, pubkey1))

	// Sign release1
	release_signature1 := getKeyFromFile("testdata/release_signature_with_this_key_is_for_tests_only.asc")
	requestAPI("POST", fmt.Sprintf("/release/%d/sign", releaseId1),
		fmt.Sprintf(`{"fingerprint": "%s", "signature": "%s"}`, finger1, release_signature1))

	// Create a rule
	requestAPI("POST", "/rule", fmt.Sprintf(`{"priority": 1000, "release": %d, "channel": %d}`, releaseId1, channelId1))

}
