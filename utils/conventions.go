package utils

import (
	"errors"

	"github.com/hashicorp/go-version"
)

// OSArch represents an OS architecture
type OSArch string

// OSArch enum
const (
	OSArchX86    OSArch = "x86"
	OSArchX86_64        = "x86_64"
	OSArchArm           = "armv7"
	OSArchArm64         = "aarch64"
)

// List provides all enums
func (osarch OSArch) List() []OSArch {
	return []OSArch{OSArchX86, OSArchX86_64, OSArchArm, OSArchArm64}
}

// IsValid checks the validity of a given OSArch
func (osarch OSArch) IsValid() error {
	for _, arch := range osarch.List() {
		if osarch == arch {
			return nil
		}
	}
	return errors.New("Invalid OS Architecture")
}

// OS represents an Operating System
type OS string

// OS enums
const (
	OSWindows OS = "Windows"
	OSMacos      = "macOS"
)

// List provides all enums
func (os OS) List() []OS {
	return []OS{OSWindows, OSMacos}
}

// IsValid checks the validity of a given OS
func (os OS) IsValid() error {
	for _, ref := range os.List() {
		if os == ref {
			return nil
		}
	}
	return errors.New("Invalid OS")
}

// Operator represent an operator
type Operator string

// Operator enums
const (
	OperatorEQ  Operator = "EQ"
	OperatorNEQ          = "NEQ"
	OperatorLT           = "LT"
	OperatorLTE          = "LTE"
	OperatorGT           = "GT"
	OperatorGTE          = "GTE"
)

// List provides all enums
func (op Operator) List() []Operator {
	return []Operator{OperatorEQ, OperatorNEQ, OperatorLT, OperatorLTE, OperatorGT, OperatorGTE}
}

// IsValid checks the validity of a given operator
func (op Operator) IsValid() error {
	for _, ref := range op.List() {
		if op == ref {
			return nil
		}
	}
	return errors.New("Invalid Operator")
}

// VersionCheck cheks "v1 <operator> v2"
func (op Operator) VersionCheck(v1 string, v2 string) (bool, error) {
	version1, err := version.NewVersion(v1)
	if err != nil {
		return false, err
	}
	version2, err := version.NewVersion(v2)
	if err != nil {
		return false, err
	}
	switch op {
	case OperatorEQ:
		return version1.Equal(version2), nil
	case OperatorNEQ:
		return !version1.Equal(version2), nil
	case OperatorGT:
		return version1.GreaterThan(version2), nil
	case OperatorGTE:
		return version1.GreaterThanOrEqual(version2), nil
	case OperatorLT:
		return version1.LessThan(version2), nil
	case OperatorLTE:
		return version1.LessThanOrEqual(version2), nil
	default:
		return false, errors.New("Operator not implemented")
	}
}
