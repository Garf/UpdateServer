package utils

import (
	"bufio"
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"code.videolan.org/videolan/updateserver/config"
	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/ssh/terminal"
)

// Errors related to password management
var (
	ErrInvalidHash         = errors.New("the encoded hash is not in the correct format")
	ErrIncompatibleVersion = errors.New("incompatible version of argon2")
)

type params struct {
	memory      uint32
	iterations  uint32
	parallelism uint8
	saltLength  uint32
	keyLength   uint32
}

var (
	defaultParams = params{
		memory:      64 * 1024, // 64MB
		iterations:  12,        // the idea is to get roughly 1sec to hash on the target server
		parallelism: 2,         // target is 2 cores for hashing
		saltLength:  16,
		keyLength:   32,
	}
)

// GenerateEncodedHashPassword generates a hash from a given password with the default params
func GenerateEncodedHashPassword(password string) (encodedHash string, err error) {
	return generateFromPassword(password, &defaultParams)
}

func generateFromPassword(password string, p *params) (encodedHash string, err error) {
	// Generate a cryptographically secure random salt.
	salt, err := generateRandomBytes(p.saltLength)
	if err != nil {
		return "", err
	}

	// Pass the plaintext password, salt and parameters to the argon2.IDKey
	// function. This will generate a hash of the password using the Argon2id
	// variant.
	hash := argon2.IDKey([]byte(password), salt, p.iterations, p.memory, p.parallelism, p.keyLength)

	// Base64 encode the salt and hashed password.
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	// Return a string using the standard encoded hash representation.
	encodedHash = fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s", argon2.Version, p.memory, p.iterations, p.parallelism, b64Salt, b64Hash)

	return encodedHash, nil
}

func generateRandomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

// ComparePasswordAndHash compares a given password with a hash
func ComparePasswordAndHash(password, encodedHash string) (match bool, err error) {
	// Extract the parameters, salt and derived key from the encoded password
	// hash.
	p, salt, hash, err := decodeHash(encodedHash)
	if err != nil {
		return false, err
	}

	// Derive the key from the other password using the same parameters.
	otherHash := argon2.IDKey([]byte(password), salt, p.iterations, p.memory, p.parallelism, p.keyLength)

	// Check that the contents of the hashed passwords are identical. Note
	// that we are using the subtle.ConstantTimeCompare() function for this
	// to help prevent timing attacks.
	if subtle.ConstantTimeCompare(hash, otherHash) == 1 {
		return true, nil
	}
	return false, nil
}

func decodeHash(encodedHash string) (p *params, salt, hash []byte, err error) {
	vals := strings.Split(encodedHash, "$")
	if len(vals) != 6 {
		return nil, nil, nil, ErrInvalidHash
	}

	var version int
	_, err = fmt.Sscanf(vals[2], "v=%d", &version)
	if err != nil {
		return nil, nil, nil, err
	}
	if version != argon2.Version {
		return nil, nil, nil, ErrIncompatibleVersion
	}

	p = &params{}
	_, err = fmt.Sscanf(vals[3], "m=%d,t=%d,p=%d", &p.memory, &p.iterations, &p.parallelism)
	if err != nil {
		return nil, nil, nil, err
	}

	salt, err = base64.RawStdEncoding.DecodeString(vals[4])
	if err != nil {
		return nil, nil, nil, err
	}
	p.saltLength = uint32(len(salt))

	hash, err = base64.RawStdEncoding.DecodeString(vals[5])
	if err != nil {
		return nil, nil, nil, err
	}
	p.keyLength = uint32(len(hash))

	return p, salt, hash, nil
}

func askCredentials() (string, string) {
	reader := bufio.NewReader(os.Stdin)
	fd := int(os.Stdin.Fd())

	fmt.Print("Enter Username: ")
	username, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	fmt.Print("Enter Password: ")
	bytePassword, err := terminal.ReadPassword(fd)
	if err != nil {
		panic(err)
	}
	password := string(bytePassword)

	return strings.TrimSpace(username), strings.TrimSpace(password)
}

// PasswordHashPrompt launchs a prompt that generate an example of config
func PasswordHashPrompt() {
	fmt.Println("Password Hash generator mode")
	fmt.Println("This mode will help you generate the 'admin' section of the updateserver configuration file.")
	fmt.Println("Note: UpdateServer is using the argon2 algorithm to store password in its configuration file.")
	fmt.Println("")
	user, password := askCredentials()
	encodedHash, err := generateFromPassword(password, &defaultParams)
	if err != nil {
		panic(err)
	}
	var configSample config.Configuration
	configSample.Admin.Username = user
	configSample.Admin.PasswordHash = encodedHash

	configPrint, err := json.MarshalIndent(configSample, "", "    ")
	if err != nil {
		panic(err)
	}

	fmt.Println("Here is what you should include in your configuration file")
	fmt.Println("")
	fmt.Println(string(configPrint))
	fmt.Println("")
}
