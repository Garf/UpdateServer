package utils

import (
	"bytes"
	"encoding/hex"
	"strings"

	"golang.org/x/crypto/openpgp"
)

// CheckSignature checks the signature of a given signed string
func CheckSignature(pubkey string, signature string, signed string) error {
	keyRingFile := strings.NewReader(pubkey)
	signedFile := strings.NewReader(signed)
	signatureFile := strings.NewReader(signature)

	keyRing, err := openpgp.ReadArmoredKeyRing(keyRingFile)
	if err != nil {
		return err
	}
	_, err = openpgp.CheckArmoredDetachedSignature(keyRing, signedFile, signatureFile)
	return err
}

// Sign generates a signature of a given string
func Sign(privateKey string, signed string) (string, error) {
	keyRingFile := strings.NewReader(privateKey)

	signer, err := openpgp.ReadArmoredKeyRing(keyRingFile)
	if err != nil {
		return "", err
	}

	message := strings.NewReader(signed)
	w := new(bytes.Buffer)

	if err = openpgp.ArmoredDetachSign(w, signer[0], message, nil); err != nil {
		return "", err
	}

	return w.String(), nil
}

// GetFingerprint extracts a signature fingerprint
func GetFingerprint(key string) (string, error) {
	if key == "" {
		return "", nil
	}
	keyRingFile := strings.NewReader(key)
	keyRing, err := openpgp.ReadArmoredKeyRing(keyRingFile)
	if err != nil {
		return "", err
	}
	fingerprint := strings.ToUpper(hex.EncodeToString(keyRing[0].PrimaryKey.Fingerprint[:]))
	return fingerprint, nil
}

// CheckPGPKey checks the validity of a given armored key
func CheckPGPKey(key string) error {
	keyRingFile := strings.NewReader(key)
	_, err := openpgp.ReadArmoredKeyRing(keyRingFile)
	return err
}
