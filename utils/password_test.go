package utils

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPasswordHash(t *testing.T) {
	params := &defaultParams

	examplePassword := "Qwerty0"
	differentPassword := "password123"

	encodedHash, err := generateFromPassword(examplePassword, params)
	if err != nil {
		assert.NoError(t, err, "Generate password hash")
	}
	fmt.Println("Hash:", encodedHash)

	match, err := ComparePasswordAndHash(differentPassword, encodedHash)
	assert.NoError(t, err, "Should compare two passwords without error")
	assert.False(t, match, "Should not match two different password")

	match, err = ComparePasswordAndHash(examplePassword, encodedHash)
	assert.NoError(t, err, "Should compare two passwords without error")
	assert.True(t, match, "Should not match two different password")
}
