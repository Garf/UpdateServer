package model

import ()

func checkNonEmptyFields(m map[string]string) error {
	for field, value := range m {
		if value == "" {
			return ErrorRequired(field)
		}
	}
	return nil
}
