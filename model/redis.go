package model

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"code.videolan.org/videolan/updateserver/config"
	"github.com/go-redis/redis/v8"
	jsoniter "github.com/json-iterator/go"
)

type RedisCompliant interface {
	GetUniqueTuples() []string
	GetForeignKeyTuples() []string
	GetRedisListRoot() string
	GetIdentifier() string
}

// Object to store with uint as ID (primary key)
// During creation, the ID will be autoincremented
type RedisCompliantWithID interface {
	RedisCompliant
	GetAutoIncrementKey() string
}

type Redis struct {
	Client *redis.Client
	ctx    *context.Context
}

func (r *Redis) Connect(cfg *config.Configuration) {
	r.Client = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.Redis.Host, cfg.Redis.Port),
		Password: cfg.Redis.Password,
		DB:       cfg.Redis.DBIndex,
	})
	c := context.Background()
	r.ctx = &c
}

// RedisCompliant management

func (r *Redis) New(object RedisCompliant) (string, error) {

	var script = redis.NewScript(`
local listKey = KEYS[1]
local identifier = tostring(ARGV[1])
local json = tostring(ARGV[2])
local nbUArgs = tonumber(ARGV[3])
local nbFKArgs = tonumber(ARGV[4])
local startU = 5
local startFK = startU + nbUArgs

-- Handle unique fields
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  for i, v in ipairs(redis.call("HVALS", "/unique"..key)) do
    if v == value then
      return {err = "UniqueConstraint"}
    end
  end
end

-- check if the key already exists
if redis.call("GET", identifier) then
  return {err = "UniqueConstraint"}
end

redis.call("SET", identifier, json)

-- Inject unique values in sets
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("HSET", "/unique"..key, identifier, value)
end

-- Add object key to the root element (containing all the keys)
redis.call("SADD", listKey, identifier)

-- Handle foreign keys
for i=startFK, startFK+nbFKArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("HSET", "/fk"..identifier, key, value)
  if not (value == "") then
    redis.call("SADD", "/ref"..value, identifier..key)
  end
end

return json
`)
	// keys
	keys := []string{object.GetRedisListRoot()}
	// args
	s, err := toJSONStr(object)
	if err != nil {
		return "", err
	}
	args := []interface{}{object.GetIdentifier(), s, len(object.GetUniqueTuples()), len(object.GetForeignKeyTuples())}
	for _, u := range object.GetUniqueTuples() {
		args = append(args, u)
	}
	for _, f := range object.GetForeignKeyTuples() {
		args = append(args, f)
	}

	result, err := script.Run(*r.ctx, r.Client, keys, args...).Result()
	if err != nil {
		return "", ErrorFromRedis(err)
	}
	return result.(string), nil
}

func (r *Redis) Modify(object RedisCompliant) error {

	var script = redis.NewScript(`
local identifier = tostring(ARGV[1])
local json = tostring(ARGV[2])
local nbUArgs = tonumber(ARGV[3])
local nbFKArgs = tonumber(ARGV[4])
local startU = 5
local startFK = startU + nbUArgs

-- Handle unique fields
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local newValue = tostring(ARGV[i+1])
  local oldValue = redis.call("HGET", "/unique"..key, identifier)
  if not (newValue == oldValue) then
    for i, v in ipairs( redis.call("HVALS", "/unique"..key) ) do
      if v == newValue then
        return {err = "UniqueConstraint"}
      end
    end
  end
end

redis.call("SET", identifier, json)

-- Inject unique values in sets
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local new_value = tostring(ARGV[i+1])
  redis.call("HSET", "/unique"..key, identifier, new_value)
end

-- Handle Foreign Keys
for i=startFK, startFK+nbFKArgs-1, 2 do
  local key = tostring(ARGV[i])
  local newValue = tostring(ARGV[i+1])
  local oldValue = redis.call("HGET", "/fk"..identifier, key)
  if not (newValue == oldValue) then
    redis.call("HSET", "/fk"..identifier, key, newValue)
    if not (oldValue == "") then
      redis.call("SREM", "/ref"..oldValue, identifier..key)
    end
    if not (newValue == "") then
      redis.call("SADD", "/ref"..newValue, identifier..key)
    end
  end
end

return 1
`)
	// keys
	keys := []string{}
	// args
	s, err := toJSONStr(object)
	if err != nil {
		return err
	}
	args := []interface{}{object.GetIdentifier(), s, len(object.GetUniqueTuples()), len(object.GetForeignKeyTuples())}
	for _, u := range object.GetUniqueTuples() {
		args = append(args, u)
	}
	for _, f := range object.GetForeignKeyTuples() {
		args = append(args, f)
	}

	_, err = script.Run(*r.ctx, r.Client, keys, args...).Bool()
	if err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) Delete(object RedisCompliant) error {

	var script = redis.NewScript(`
local listKey = KEYS[1]
local identifier = tostring(ARGV[1])
local nbUArgs = tonumber(ARGV[2])
local nbFKArgs = tonumber(ARGV[3])
local startU = 4
local startFK = startU + nbUArgs

-- FK constraints: check if this object is referenced elsewhere
if redis.call("SCARD", "/ref"..identifier) > 0 then
  return {err = "ForeignKeyConstraint"}
end

redis.call("DEL", identifier)

-- Handle unique fields
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("HDEL", "/unique"..key, identifier)
end

-- Handle foreign keys
for i=startFK, startFK+nbFKArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("SREM", "/ref"..value, identifier..key)
end
redis.call("DEL", "/fk"..identifier)

-- remove object key to the root element (containing all the keys)
redis.call("SREM", listKey, identifier)

return 1
`)
	// keys
	keys := []string{object.GetRedisListRoot()}
	// args
	args := []interface{}{object.GetIdentifier(), len(object.GetUniqueTuples()), len(object.GetForeignKeyTuples())}
	for _, u := range object.GetUniqueTuples() {
		args = append(args, u)
	}
	for _, f := range object.GetForeignKeyTuples() {
		args = append(args, f)
	}

	_, err := script.Run(*r.ctx, r.Client, keys, args...).Bool()
	if err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

// RedisCompliantWithID management

func (r *Redis) NewWithID(object RedisCompliantWithID) (string, error) {

	var script = redis.NewScript(`
local listKey = KEYS[1]
local autoIncrementKey = KEYS[2]
local identifier = tostring(ARGV[1])
local json = tostring(ARGV[2])
local nbUArgs = tonumber(ARGV[3])
local nbFKArgs = tonumber(ARGV[4])
local startU = 5
local startFK = startU + nbUArgs

-- Handle unique fields
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  for _, v in ipairs(redis.call("HVALS", "/unique"..key)) do
    if v == value then
      return {err = "UniqueConstraint"}
    end
  end
end

-- Get new integer ID, inject it, then store the object
local newID = redis.call("INCR", autoIncrementKey)
identifier = string.format(identifier, newID)
json = '{"id":'..tostring(newID)..string.sub(json, 8, -1)
redis.call("SET", identifier, json)

-- Inject unique values in sets
for i=startU, startU+nbUArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("HSET", "/unique"..key, identifier, value)
end

-- Add object key to the root element (containing all the keys)
redis.call("SADD", listKey, identifier)

-- Handle foreign keys
for i=startFK, startFK+nbFKArgs-1, 2 do
  local key = tostring(ARGV[i])
  local value = tostring(ARGV[i+1])
  redis.call("HSET", "/fk"..identifier, key, value)
  if not (value == "") then
    redis.call("SADD", "/ref"..value, identifier..key)
  end
end

return json
`)
	// keys
	keys := []string{object.GetRedisListRoot(), object.GetAutoIncrementKey()}
	// args
	s, err := toJSONStr(object)
	if err != nil {
		return "", err
	}
	// Check json starts with '{"id":0,'
	if !strings.HasPrefix(s, `{"id":0,`) {
		return "", errors.New("Unable to replace 'id' in json: " + s)
	}

	args := []interface{}{object.GetIdentifier(), s, len(object.GetUniqueTuples()), len(object.GetForeignKeyTuples())}
	for _, u := range object.GetUniqueTuples() {
		args = append(args, u)
	}
	for _, f := range object.GetForeignKeyTuples() {
		args = append(args, f)
	}

	result, err := script.Run(*r.ctx, r.Client, keys, args...).Result()
	if err != nil {
		return "", ErrorFromRedis(err)
	}
	return result.(string), nil
}

func (r *Redis) GetJson(key string, object interface{}) error {
	val, err := r.Client.Get(*r.ctx, key).Result()
	if err != nil {
		return ErrorFromRedis(err)
	}
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	return json.Unmarshal([]byte(val), object)
}

func (r *Redis) StoreJson(key string, object interface{}) error {
	s, err := toJSONStr(object)
	if err != nil {
		return ErrorFromRedis(err)
	}
	if _, err = r.Client.Set(*r.ctx, key, s, 0).Result(); err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) List(object RedisCompliant) ([]string, error) {
	var script = redis.NewScript(`
local listKey = KEYS[1]
local result = {}
for _, id in ipairs(redis.call("SMEMBERS", listKey)) do
  table.insert(result, redis.call("GET", id))
end
return result
`)
	keys := []string{object.GetRedisListRoot()}
	args := []interface{}{}
	resultStr, err := script.Run(*r.ctx, r.Client, keys, args...).StringSlice()
	if err != nil {
		return resultStr, ErrorFromRedis(err)
	}
	return resultStr, nil
}

// Generic redis functions

// non existent keys will be ignored
func (r *Redis) RenameKeys(renameMap map[string]string) error {
	var script = redis.NewScript(`
for i=1, #KEYS, 2 do
  redis.call("RENAME", KEYS[i], KEYS[i+1])
end
return 1
`)
	keys := []string{}
	for k, v := range renameMap {
		keys = append(keys, k, v)
	}
	args := []interface{}{}
	_, err := script.Run(*r.ctx, r.Client, keys, args...).Bool()
	if err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) DeleteKey(key string) error {
	_, err := r.Client.Del(*r.ctx, key).Result()
	if err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) Store(key string, value string) error {
	if _, err := r.Client.Set(*r.ctx, key, value, 0).Result(); err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) AddInList(key string, value string) error {
	if _, err := r.Client.LPush(*r.ctx, key, value).Result(); err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) GetList(key string, start int64, stop int64) ([]string, error) {
	l, err := r.Client.LRange(*r.ctx, key, start, stop).Result()
	if err != nil {
		return []string{}, ErrorFromRedis(err)
	}
	return l, nil
}

func (r *Redis) TrimList(key string, start int64, stop int64) error {
	if _, err := r.Client.LTrim(*r.ctx, key, start, stop).Result(); err != nil {
		return ErrorFromRedis(err)
	}
	return nil
}

func (r *Redis) GetListLen(key string) (int64, error) {
	l, err := r.Client.LLen(*r.ctx, key).Result()
	if err != nil {
		return -1, ErrorFromRedis(err)
	}
	return l, nil
}

func (r *Redis) Get(key string) (string, error) {
	value, err := r.Client.Get(*r.ctx, key).Result()
	if err != nil {
		return "", ErrorFromRedis(err)
	}
	return value, nil
}

func (r *Redis) Flush() error {
	_, err := r.Client.FlushDB(*r.ctx).Result()
	return err
}

// ErrorFromGorm wraps a Gorm error in a Error one
func ErrorFromRedis(err error) Error {
	if err == redis.Nil {
		return ErrorNotFound()
	}
	switch err.Error() {
	case "NotFound":
		return ErrorNotFound()
	case "ForeignKeyConstraint":
		return ErrorForeignKeyConstraint()
	case "UniqueConstraint":
		return ErrorUniqueConstraint()
	default:
		return ErrorGeneric(err.Error())
	}
}

func getIdentifierGeneric(template string, id uint) string {
	if id == 0 {
		return template
	} else {
		return fmt.Sprintf(template, id)
	}
}

func toJSONStr(object interface{}) (string, error) {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	b, err := json.Marshal(object)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
