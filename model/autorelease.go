package model

import (
	"encoding/json"

	"code.videolan.org/videolan/updateserver/utils"
)

// AutoRelease represents all the information needed to perform an automatic release injection in a rule
// This is a typical Nightly workflow: Every <schedule> period:
// - use the <URL> to find out if there is a new Release
// - if so, create a new Release in database
// - sign its manifest (SignatureKey should include a private key)
// - set the new release in the selected <Rule>
type AutoRelease struct {
	ID                             uint         `json:"id"`
	Channel                        Channel      `json:"-"`
	ChannelID                      uint         `json:"channel"`
	Description                    string       `json:"description"`
	Rule                           Rule         `json:"-"`
	RuleID                         uint         `json:"rule"`
	Schedule                       string       `json:"schedule"`
	URL                            string       `json:"url"`
	ReleaseFilenameRegex           string       `json:"release_filename_regex"`
	ReleaseBaseTitle               string       `json:"release_base_title"`
	ReleaseOS                      utils.OS     `json:"release_os"`
	ReleaseOsVersion               string       `json:"release_os_version"`
	ReleaseOsArchitecture          utils.OSArch `json:"release_os_architecture"`
	ReleaseProductDefaultVersion   string       `json:"release_product_default_version"`
	ReleaseSignatureKey            SignatureKey `json:"-"`
	ReleaseSignatureKeyFingerprint string       `json:"signature_key,omitempty"`
}

// CheckFields checks the validity of the fields
func (a *AutoRelease) CheckFields() error {
	if err := checkNonEmptyFields(map[string]string{"url": a.URL, "release_filename_regex": a.ReleaseFilenameRegex, "release_title": a.ReleaseBaseTitle, "release_os": string(a.ReleaseOS), "release_os_version": a.ReleaseOsVersion, "release_os_architecture": string(a.ReleaseOsArchitecture), "release_product_default_version": a.ReleaseProductDefaultVersion}); err != nil {
		return err
	}
	if a.ReleaseOS.IsValid() != nil {
		return ErrorInvalid("release_os", "OS type is invalid")
	}
	if a.ReleaseOsArchitecture.IsValid() != nil {
		return ErrorInvalid("os_architecture", "OS Architecture is invalid")
	}
	return nil
}

func (a AutoRelease) FromJsonStr(str string) (AutoRelease, error) {
	var object AutoRelease
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}

func (a *AutoRelease) GetUniqueTuples() []string {
	return []string{}
}

func (a *AutoRelease) GetForeignKeyTuples() []string {
	channel := Channel{ID: a.ChannelID}
	if a.ReleaseSignatureKeyFingerprint != "" {
		signatureKey := SignatureKey{Fingerprint: a.ReleaseSignatureKeyFingerprint}
		return []string{"channel", channel.GetIdentifier(), "signature_key", signatureKey.GetIdentifier()}
	} else {
		return []string{"channel", channel.GetIdentifier(), "signature_key", ""}
	}
}

func (a *AutoRelease) GetRedisListRoot() string {
	// autorelease can be listed per channel or for all channels.
	return "/autorelease/"
}

func (a *AutoRelease) GetAutoIncrementKey() string {
	return "/id/autorelease/"
}

func (a *AutoRelease) GetIdentifier() string {
	return getIdentifierGeneric("/autorelease/%d/", a.ID)
}
