package model

import (
	"fmt"
	"net/http"
	"regexp"
)

// Model Error Type Enums
const (
	ErrorTypeNotFound             = "Object not found"
	ErrorTypeInvalid              = "Invalid Argument"
	ErrorTypeRequired             = "Field cannot be empty"
	ErrorTypeReadOnly             = "Field is read-only"
	ErrorTypeUniqueConstraint     = "Already found a similar object"
	ErrorTypeForeignKeyConstraint = "This object is referenced elsewhere"
	ErrorTypeGeneric              = "Error"
)

// Error is a generic error structure that can be translated to HTTP status.
type Error struct {
	ErrorType   string `json:"error"`
	Field       string `json:"field"`
	Description string `json:"description"`
	HTTPStatus  int
}

// Error interface compliance
func (e Error) Error() string {
	return fmt.Sprintf("%s (%s): %s", e.ErrorType, e.Field, e.Description)
}

// ErrorNotFound generates an NotFound error
func ErrorNotFound() Error {
	return Error{ErrorTypeNotFound, "", "", http.StatusNotFound}
}

// ErrorInvalid generates an Invalid argument error
func ErrorInvalid(field string, description string) Error {
	return Error{ErrorTypeInvalid, field, description, http.StatusBadRequest}
}

// ErrorRequired generates a mandatory field error
func ErrorRequired(field string) Error {
	return Error{ErrorTypeRequired, field, "", http.StatusBadRequest}
}

// ErrorReadOnly generates a read-only field error
func ErrorReadOnly(field string) Error {
	return Error{ErrorTypeReadOnly, field, "", http.StatusConflict}
}

// ErrorUniqueConstraint generatesa unique constraint error
func ErrorUniqueConstraint() Error {
	return Error{ErrorTypeUniqueConstraint, "", "", http.StatusBadRequest}
}

// ErrorForeignKeyConstraint generates a foreign key constraint error
func ErrorForeignKeyConstraint() Error {
	return Error{ErrorTypeForeignKeyConstraint, "", "", http.StatusConflict}
}

// ErrorGeneric generates a generic error
func ErrorGeneric(description string) Error {
	return Error{ErrorTypeGeneric, "", description, http.StatusInternalServerError}
}

func matchAllStrings(patterns []string, s string) (bool, error) {
	for _, p := range patterns {
		if matched, err := regexp.MatchString(p, s); err != nil {
			return false, err
		} else if matched {
			return true, nil
		}
	}
	return false, nil
}
