package model

import (
	"time"

	"code.videolan.org/videolan/updateserver/utils"
)

// UpdateRequest gathers all information sent to rules to find a matching release
type UpdateRequest struct {
	Channel          string       `form:"channel" json:"channel"`
	OS               utils.OS     `form:"os" json:"os"`
	OsVersion        string       `form:"os_version" json:"os_version"`
	OsArchitecture   utils.OSArch `form:"os_arch" json:"os_arch"`
	ProductVersion   string       `form:"product_version" json:"product_version"`
	Product          string       `form:"-" json:"product"`
	IP               string       `form:"-" json:"ip_address"`
	MatchedRuleID    *uint        `form:"-" json:"matched_rule,omitempty"`
	MatchedReleaseID *uint        `form:"-" json:"matched_release,omitempty"`
	CreatedAt        time.Time    `form:"-" json:"created_at"`
}

func (u *UpdateRequest) GetRedisListRoot() string {
	return "/updaterequest/"
}
