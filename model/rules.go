package model

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"time"

	"code.videolan.org/videolan/updateserver/utils"
)

// Rule represents a set of criteria that will match a release for a given update request.
// Each Channel includes a list of Rules.
// Anytime a request is asked on a channel, each Rule is parsed - sorted by Priority.
// The first Rule that matches the request defines the Release proposed.
// A rule matches if all the checks return true
// Null checks are considered true.
type Rule struct {
	ID                 uint            `json:"id"`
	ChannelID          uint            `json:"channel"`
	Priority           int64           `json:"priority"`
	TimeCheck          *TimeCheck      `json:"timecheck,omitempty"`
	ArchCheck          *ArchCheck      `json:"archcheck,omitempty"`
	OSCheck            *OSCheck        `json:"oscheck,omitempty"`
	OSVersionCheck     *OSVersionCheck `json:"osversioncheck,omitempty"`
	VersionCheck       *VersionCheck   `json:"versioncheck,omitempty"`
	NetworkSourceCheck *NetworkCheck   `json:"networksourcecheck,omitempty"`
	RollCheck          *RollCheck      `json:"rollcheck,omitempty"`
	ReleaseID          uint            `json:"release"`
}

// RuleCheck is a check related to a rule
type RuleCheck interface {
	Check(request *UpdateRequest) (bool, error)
}

func recCheck(request *UpdateRequest, check RuleCheck, err *error) bool {
	if check == nil {
		return true
	}
	ok, e := check.Check(request)
	if e != nil {
		*err = e
		return false
	}
	if !ok {
		return false
	}
	return true
}

// Check evaluates whether the update request matches the rule or not
func (r *Rule) Check(request *UpdateRequest) (bool, error) {
	if r.TimeCheck != nil {
		ok, err := r.TimeCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.ArchCheck != nil {
		ok, err := r.ArchCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.OSCheck != nil {
		ok, err := r.OSCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.OSVersionCheck != nil {
		ok, err := r.OSVersionCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.VersionCheck != nil {
		ok, err := r.VersionCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.NetworkSourceCheck != nil {
		ok, err := r.NetworkSourceCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	if r.RollCheck != nil {
		ok, err := r.RollCheck.Check(request)
		if err != nil {
			return ok, err
		} else if !ok {
			return false, nil
		}
	}
	return true, nil
}

// TimeCheck is a condition on present time (now).
type TimeCheck struct {
	StartTime time.Time `json:"starttime"`
	EndTime   time.Time `json:"endtime"`
}

// Check do the actual check for the given RuleCheck
func (c *TimeCheck) Check(request *UpdateRequest) (bool, error) {
	now := time.Now()
	if now.Before(c.EndTime) && now.After(c.StartTime) {
		return true, nil
	}
	return false, nil
}

// ArchCheck checks the update request OS architecture
type ArchCheck struct {
	Architecture utils.OSArch `json:"architecture"`
}

// Check do the actual check for the given RuleCheck
func (c *ArchCheck) Check(request *UpdateRequest) (bool, error) {
	if request.OsArchitecture == c.Architecture {
		return true, nil
	}
	return false, nil
}

// OSCheck checks the OS/platform
type OSCheck struct {
	OS utils.OS `json:"os"`
}

// Check do the actual check for the given RuleCheck
func (c *OSCheck) Check(request *UpdateRequest) (bool, error) {
	if request.OS == c.OS {
		return true, nil
	}
	return false, nil
}

// OSVersionCheck checks the OS version
type OSVersionCheck struct {
	OSVersion string         `json:"os_version"`
	Operator  utils.Operator `json:"operator"` // EQ, NEQ, LT, LTE, GT, GTE
}

// Check do the actual check for the given RuleCheck
func (c *OSVersionCheck) Check(request *UpdateRequest) (bool, error) {
	if request.OsVersion == "" {
		return false, nil
	}
	return c.Operator.VersionCheck(request.OsVersion, c.OSVersion)
}

// VersionCheck checks the product version
type VersionCheck struct {
	ProductVersion string         `json:"productversion"`
	Operator       utils.Operator `json:"operator"` // EQ, NEQ, LT, LTE, GT, GTE
}

// Check do the actual check for the given RuleCheck
func (c *VersionCheck) Check(request *UpdateRequest) (bool, error) {
	if request.ProductVersion == "" {
		return false, nil
	}
	return c.Operator.VersionCheck(request.ProductVersion, c.ProductVersion)
}

// RollCheck is a simple rolling release check
type RollCheck struct {
	RollingPercentage int `json:"rollingpercentage"`
}

// Check do the actual check for the given RuleCheck
func (c *RollCheck) Check(request *UpdateRequest) (bool, error) {
	if rand.Intn(100) <= c.RollingPercentage {
		return true, nil
	}
	return false, nil
}

// NetworkCheck checks the incoming IP address
type NetworkCheck struct {
	Network string `json:"network"`
	Netmask uint   `json:"netmask"`
}

// Check do the actual check for the given RuleCheck
func (c *NetworkCheck) Check(request *UpdateRequest) (bool, error) {
	_, ipnet, _ := net.ParseCIDR(fmt.Sprintf("%s/%d", c.Network, c.Netmask))
	requestIP := net.ParseIP(request.IP)
	if ipnet.Contains(requestIP) {
		return true, nil
	}
	return false, nil
}

func (r Rule) FromJsonStr(str string) (Rule, error) {
	var object Rule
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}

func (r *Rule) GetUniqueTuples() []string {
	return []string{fmt.Sprintf("/channel/%d/rule/priority", r.ChannelID), fmt.Sprintf("%d", r.Priority)}
}

func (r *Rule) GetForeignKeyTuples() []string {
	channel := Channel{ID: r.ChannelID}
	release := Release{ID: r.ReleaseID}
	return []string{"channel", channel.GetIdentifier(), "release", release.GetIdentifier()}
}

func (r *Rule) GetRedisListRoot() string {
	// rule can only be listed per channel
	return fmt.Sprintf("/rule/%d/channel/", r.ChannelID)
}

func (a *Rule) GetAutoIncrementKey() string {
	return "/id/rule/"
}

func (a *Rule) GetIdentifier() string {
	return getIdentifierGeneric("/rule/%d/", a.ID)
}
