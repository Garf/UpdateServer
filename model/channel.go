package model

import (
	"encoding/json"
	"fmt"
)

// Channel database model
// The signature key will be used only for beta channels (auto-sign)
type Channel struct {
	ID                      uint    `json:"id"`
	Name                    string  `json:"name"`
	ProductID               uint    `json:"product"`
	SignatureKeyFingerprint *string `json:"signature_key,omitempty"`
}

// CheckFields checks the validity of the release fields
func (c *Channel) CheckFields() error {
	return checkNonEmptyFields(map[string]string{"name": c.Name})
}

func (c Channel) FromJsonStr(str string) (Channel, error) {
	var object Channel
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}

func (c *Channel) GetUniqueTuples() []string {
	return []string{"/channel/name", c.Name}
}

func (c *Channel) GetForeignKeyTuples() []string {
	product := Product{ID: c.ProductID}
	if c.SignatureKeyFingerprint != nil {
		signatureKey := SignatureKey{Fingerprint: *c.SignatureKeyFingerprint}
		return []string{"product", product.GetIdentifier(), "signature_key", signatureKey.GetIdentifier()}
	} else {
		return []string{"product", product.GetIdentifier(), "signature_key", ""}
	}
}

func (c *Channel) GetRedisListRoot() string {
	// channels will only be retrieved by ProductID
	// So use it for storing them
	return fmt.Sprintf("/product/%d/channel/", c.ProductID)
}

func (c *Channel) GetAutoIncrementKey() string {
	return "/id/channel/"
}

func (c *Channel) GetIdentifier() string {
	return getIdentifierGeneric("/channel/%d/", c.ID)
}
