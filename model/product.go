package model

import (
	"encoding/json"
)

// Product database model
// A product represents the "real" application product.
// Channels and releases will all be linked to a Product
// Name is important, as it will be used to access the update service
type Product struct {
	ID               uint     `json:"id"`
	Name             string   `json:"name"`
	Description      string   `json:"description"`
	DefaultChannel   *Channel `json:"-"`
	DefaultChannelID *uint    `json:"default_channel,omitempty"`
}

// CheckFields checks the validity of the release fields
func (p *Product) CheckFields() error {
	return checkNonEmptyFields(map[string]string{"name": p.Name})
}

func (p Product) FromJsonStr(str string) (Product, error) {
	var object Product
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}

func (p *Product) GetUniqueTuples() []string {
	return []string{"/product/name", p.Name}
}
func (p *Product) GetForeignKeyTuples() []string {
	if p.DefaultChannelID != nil {
		channel := Channel{ID: *p.DefaultChannelID}
		return []string{"default_channel", channel.GetIdentifier()}
	} else {
		return []string{"default_channel", ""}
	}
}

func (p *Product) GetRedisListRoot() string {
	return "/product/"
}

func (p *Product) GetAutoIncrementKey() string {
	return "/id/product/"
}

func (p *Product) GetIdentifier() string {
	return getIdentifierGeneric("/product/%d/", p.ID)
}
