package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"regexp"

	"code.videolan.org/videolan/updateserver/utils"
)

// ReleaseHashes includes the release hashes in hexadecimal string representation.
type ReleaseHashes struct {
	Sha256 string `json:"sha256"`
	Sha512 string `json:"sha512"`
}

// Release database model
// SignatureKey will store the SignatureKey used to sign the Release
// Signature will store the signature after the signing process (so this field is automatically filled/reset)
type Release struct {
	ID                      uint          `json:"id"`
	Title                   string        `json:"title"`
	Description             string        `json:"description"`
	URL                     string        `json:"url"`
	OS                      utils.OS      `json:"os"`
	OsVersion               string        `json:"os_version"`
	OsArchitecture          utils.OSArch  `json:"os_arch"`
	ProductID               uint          `json:"product"`
	Product                 *Product      `json:"-"`
	ProductVersion          string        `json:"product_version"`
	SignatureKeyFingerprint *string       `json:"signature_key,omitempty"`
	Signature               string        `json:"signature"`
	Hashes                  ReleaseHashes `json:"hashes"`
}

// CheckFields checks the validity of the release fields
func (r *Release) CheckFields() error {
	if err := checkNonEmptyFields(map[string]string{"title": r.Title, "url": r.URL, "os": string(r.OS), "os_version": r.OsVersion, "os_architecture": string(r.OsArchitecture), "product_version": r.ProductVersion}); err != nil {
		return err
	}
	if err := r.OS.IsValid(); err != nil {
		return ErrorInvalid("os", "OS type is invalid")
	}
	if err := r.OsArchitecture.IsValid(); err != nil {
		return ErrorInvalid("os_architecture", "OS Architecture is invalid")
	}
	if match, _ := regexp.MatchString("^[A-Fa-f0-9]{64}$", r.Hashes.Sha256); !match {
		return ErrorInvalid("hashes", "sha256 is not valid")
	}
	if match, _ := regexp.MatchString("^[A-Fa-f0-9]{128}$", r.Hashes.Sha512); !match {
		return ErrorInvalid("hashes", "sha512 is not valid")
	}
	return nil
}

// ResetSignature resets the fields related to the signature
func (r *Release) ResetSignature() {
	r.Signature = ""
	r.SignatureKeyFingerprint = nil
}

// ReleaseFormat defines the fields that will be used as release "Manifest"
type ReleaseFormat struct {
	Title          string              `json:"title"`
	Description    string              `json:"description"`
	URL            string              `json:"url"`
	OS             string              `json:"os"`
	OsVersion      string              `json:"os_version"`
	OsArchitecture string              `json:"os_arch"`
	Product        string              `json:"product"`
	ProductVersion string              `json:"product_version"`
	Hashes         ReleaseHashesFormat `json:"hashes"`
}

// ReleaseHashesFormat defines the fields that will be used as release hashes "Manifest"
type ReleaseHashesFormat struct {
	Sha256 string `json:"sha256"`
	Sha512 string `json:"sha512"`
}

// GetManifest provides the public representation of a Release.
// This should be used as release "content" for signing processes
func (r *Release) GetManifest() (string, error) {
	if r.Product == nil {
		return "", ErrorGeneric("Product has not been retrieved")
	}
	// Use a custom encoder for the manifest version of the release:
	// json.Marshal forces escapeHTML to true, so some charaters like <, > can be converted to their numerical translation...
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "")
	// Choose the fields that will be used as the "content" of the release
	if err := enc.Encode(ReleaseFormat{Title: r.Title, Description: r.Description, URL: r.URL, OS: string(r.OS), OsVersion: r.OsVersion, OsArchitecture: string(r.OsArchitecture), Product: r.Product.Name, ProductVersion: r.ProductVersion, Hashes: ReleaseHashesFormat{Sha256: r.Hashes.Sha256, Sha512: r.Hashes.Sha512}}); err != nil {
		return "", err
	}
	manifest := buf.Bytes()
	// golang json.Encoder.Encode automatically add a \n at the end of the byte stream
	if len(manifest) > 0 && manifest[len(manifest)-1] == '\n' {
		manifest = manifest[:len(manifest)-1]
	}
	return string(manifest), nil
}

func (r Release) FromJsonStr(str string) (Release, error) {
	var object Release
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}

func (r *Release) GetUniqueTuples() []string {
	return []string{}
}
func (r *Release) GetForeignKeyTuples() []string {
	product := Product{ID: r.ProductID}
	if r.SignatureKeyFingerprint != nil {
		signatureKey := SignatureKey{Fingerprint: *r.SignatureKeyFingerprint}
		return []string{"product", product.GetIdentifier(), "signature_key", signatureKey.GetIdentifier()}
	} else {
		return []string{"product", product.GetIdentifier(), "signature_key", ""}
	}
}

func (r *Release) GetRedisListRoot() string {
	// releases will only be retrieved by ProductID
	// So use it for storing them
	return fmt.Sprintf("/product/%d/release/", r.ProductID)
}

func (r *Release) GetAutoIncrementKey() string {
	return "/id/release/"
}

func (r *Release) GetIdentifier() string {
	return getIdentifierGeneric("/release/%d/", r.ID)
}

func (r *Release) GetManifestKey() string {
	return r.GetIdentifier() + "manifest"
}

func (r *Release) GetSignatureKey() string {
	return r.GetIdentifier() + "signature"
}
