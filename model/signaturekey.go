package model

import (
	"encoding/json"
	"errors"
	"fmt"

	"code.videolan.org/videolan/updateserver/utils"
)

// SignatureKey database model
type SignatureKey struct {
	Fingerprint   string `json:"fingerprint"`
	PublicKey     string `json:"pubkey"`
	HasPrivateKey bool   `json:"has_privatekey"`
	Description   string `json:"description"`
}

// UpdateFingerprint set the pubkey fingerprint in the Fingerprint field
func (s *SignatureKey) UpdateFingerprint() error {
	fingerprint, err := utils.GetFingerprint(s.PublicKey)
	if err != nil {
		return err
	}
	s.Fingerprint = fingerprint
	return nil
}

// CheckPrivateKey checks whether PrivateKey can sign something and be verified by public key
func (s *SignatureKey) CheckPrivateKey(privateKey string) error {
	if privateKey == "" {
		return nil
	}
	dummyMessage := "I am a very dummy message"
	signature, err := utils.Sign(privateKey, dummyMessage)
	if err != nil {
		return err
	}
	return utils.CheckSignature(s.PublicKey, signature, dummyMessage)
}

// AutoSign signs the release with the internal key (for beta/nightly channel releases only)
func (s *SignatureKey) AutoSign(release *Release, privateKey string) (string, string, error) {
	if privateKey == "" {
		return "", "", errors.New("Cannot autosign without a private key")
	}
	manifest, err := release.GetManifest()
	if err != nil {
		return "", "", err
	}
	signature, err := utils.Sign(privateKey, manifest)
	if err != nil {
		return "", "", err
	}
	release.SignatureKeyFingerprint = &s.Fingerprint
	return manifest, signature, nil
}

// ExternalSign uses the given signature to sign the release
func (s *SignatureKey) ExternalSign(release *Release, signature string) (string, string, error) {
	manifest, err := release.GetManifest()
	if err != nil {
		return "", "", err
	}
	err = utils.CheckSignature(s.PublicKey, signature, manifest)
	if err != nil {
		return "", "", ErrorInvalid("signature", fmt.Sprintf("Error while checking signature: %s", err.Error()))
	}
	release.SignatureKeyFingerprint = &s.Fingerprint
	return manifest, signature, nil
}

func (s *SignatureKey) GetUniqueTuples() []string {
	return []string{} // Fingerprint is the primary key, it will be unique no matter what
}
func (s *SignatureKey) GetForeignKeyTuples() []string {
	return []string{}
}

func (s *SignatureKey) GetRedisListRoot() string {
	return "/signaturekey/"
}

func (s *SignatureKey) GetIdentifier() string {
	return fmt.Sprintf("/signaturekey/%s/", s.Fingerprint)
}

func (s SignatureKey) FromJsonStr(str string) (SignatureKey, error) {
	var object SignatureKey
	if err := json.Unmarshal([]byte(str), &object); err != nil {
		return object, err
	}
	return object, nil
}
