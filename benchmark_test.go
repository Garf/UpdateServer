package main

import (
	//"encoding/json"
	//"net/http"
	//"sync"

	//"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	//"github.com/stretchr/testify/require"
)

// We cannot use
func simpleAssert(assumption bool, message string) {
	if !assumption {
		panic(message)
	}
}

func BenchmarkBench(b *testing.B) {
	dummyT := testing.T{}
	t := &dummyT

	router := initBench(t)

	logfile, _ := os.Open(os.DevNull)
	router.Use(gin.LoggerWithWriter(io.MultiWriter(logfile)))

	// Create product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create release
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))

	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// add a private key
	AssertAPIOK(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")

	// Sign release
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s"}`, finger1))

	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))
	// Create Channel2
	AssertAPIStatusOK(t, router, "POST", channelPrefix, s(`{"name": "test2", "product": %d}`, productID1))

	updateChannel1 := s("/api/v1/update/%s?channel=test1", "vlc")
	//updateChannel2 := s("/api/v1/update/%s?channel=test2", "vlc")
	release1URL := s("/api/v1/release/%d/manifest", releaseID1)

	// no rule, no chocolate
	//AssertAPIErrorNotFound(t, router, "GET", updateChannel2+"&os=Windows", "")

	// Create empty rule
	release1ChannelStr := s(`"release": %d, "channel": %d`, releaseID1, channelID1)
	ruleID := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 1000, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 1000, `+release1ChannelStr+` }`)
	//fmt.Println("The ID: ", ruleID)

	// Default Channel
	// No default channel on product -> no match if channel is not specified
	// Set channel1 to default channel
	AssertAPIOK(t, router, "PUT", s("%s/%d", productPrefix, productID1),
		s(`{"name": "vlc", "description": "A cØØl multimedia player", "default_channel": %d}`, channelID1),
		s(`{"id": %d, "name": "vlc", "description": "A cØØl multimedia player", "default_channel": %d}`, productID1, channelID1))
	// Now this matches the default channel

	// Test Cache limit
	b.Run("Request/Update", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	b.Run("Request/Manifest", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})
	b.Run("Request/Signature", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})
	b.Run("Request/Full-unmarshal-001-rules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})

	type m map[string]interface{}
	// create other rules
	otherRuleIDs := []uint{}
	for i := 1; i <= 9; i++ {
		code, body, _ := requestJSON(router, "POST", rulePrefix, nil, m{"priority": 1000 + i, "release": releaseID1, "channel": channelID1})
		simpleAssert(code == http.StatusOK, "Caramba")
		otherRuleIDs = append(otherRuleIDs, getIDFromJSON(t, body))
	}
	b.Run("Request/Full-unmarshal-010-rules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})

	for i := 1; i <= 90; i++ {
		code, body, _ := requestJSON(router, "POST", rulePrefix, nil, m{"priority": 1009 + i, "release": releaseID1, "channel": channelID1})
		simpleAssert(code == http.StatusOK, "Caramba")
		otherRuleIDs = append(otherRuleIDs, getIDFromJSON(t, body))
	}
	b.Run("Request/Full-unmarshal-100-rules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})
	// Clean
	for _, id := range otherRuleIDs {
		requestJSON(router, "DELETE", s(rulePrefix+"/%d", id), nil, "")
	}
	b.Run("Request/Full-unmarshal-001-rules-bis", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})
	requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")

	ipHeader := map[string]string{"X-Forwarded-For": "10.0.0.42"}
	rule := m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["oscheck"] = m{"os": "Windows"}
	_, body, _ := requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-OSRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ := requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["archcheck"] = m{"architecture": "x86_64"}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-ARCHRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	now := time.Now().Format(time.RFC3339)
	nowPlus30Seconds := time.Now().Add(30 * time.Second).Format(time.RFC3339)
	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["timecheck"] = m{"starttime": now, "endtime": nowPlus30Seconds}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-TIMERule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["osversioncheck"] = m{"os_version": "10.0.0", "operator": "GTE"}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-OSVERSIONRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["versioncheck"] = m{"productversion": "3.0.10", "operator": "EQ"}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-PRODUCTVERSIONRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["rollcheck"] = m{"rollingpercentage": 50}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-ROLLINGRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, nil, "")
			simpleAssert(code == http.StatusFound || code == http.StatusNotFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["networksourcecheck"] = m{"network": "10.0.0.0", "netmask": 24}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-NETWORKSOURCERule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	// FULL
	now = time.Now().Format(time.RFC3339)
	nowPlus30Seconds = time.Now().Add(30 * time.Second).Format(time.RFC3339)
	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["oscheck"] = m{"os": "Windows"}
	rule["archcheck"] = m{"architecture": "x86_64"}
	rule["timecheck"] = m{"starttime": now, "endtime": nowPlus30Seconds}
	rule["osversioncheck"] = m{"os_version": "10.0.0", "operator": "GTE"}
	rule["versioncheck"] = m{"productversion": "3.0.10", "operator": "EQ"}
	rule["rollcheck"] = m{"rollingpercentage": 100}
	rule["networksourcecheck"] = m{"network": "10.0.0.0", "netmask": 24}
	_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
	ruleID = getIDFromJSON(t, body)
	b.Run("Request/Update-WORSTRule", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	code, _, _ = requestJSON(router, "DELETE", s(rulePrefix+"/%d", ruleID), nil, "")
	simpleAssert(code == http.StatusOK, "Caramba")

	ruleIDs := []uint{}
	now = time.Now().Format(time.RFC3339)
	nowPlus30Seconds = time.Now().Add(30 * time.Second).Format(time.RFC3339)
	rule = m{"priority": 1000, "release": releaseID1, "channel": channelID1}
	rule["oscheck"] = m{"os": "Windows"}
	rule["archcheck"] = m{"architecture": "x86_64"}
	rule["timecheck"] = m{"starttime": now, "endtime": nowPlus30Seconds}
	rule["osversioncheck"] = m{"os_version": "10.0.0", "operator": "GTE"}
	rule["versioncheck"] = m{"productversion": "3.0.10", "operator": "EQ"}
	rule["networksourcecheck"] = m{"network": "10.0.0.0", "netmask": 24}
	// create 10 rules
	for i := 0; i < 10; i++ {
		// last check in list is roll check, so it's worst case scenario
		if i == 0 {
			rule["rollcheck"] = m{"rollingpercentage": 100}
		} else {
			rule["rollcheck"] = m{"rollingpercentage": 0}
		}
		rule["priority"] = 1000 - i
		_, body, _ = requestJSON(router, "POST", rulePrefix, nil, rule)
		ruleIDs = append(ruleIDs, getIDFromJSON(t, body))
	}
	b.Run("Request/Update-10-WORSTRules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	b.Run("Request/Full-10-WORSTRules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})
	// create 10 more rules
	for i := 0; i < 10; i++ {
		// last check in list is roll check, so it's worst case scenario
		rule["rollcheck"] = m{"rollingpercentage": 0}
		rule["priority"] = 990 - i
		_, body, _ = requestJSON(router, "POST", rulePrefix, &ipHeader, rule)
		ruleIDs = append(ruleIDs, getIDFromJSON(t, body))
	}

	// use the 20
	b.Run("Request/Update-20-WORSTRules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	// use the 20 - with POST method
	b.Run("Request/Update-20-WORSTRules-POSTMethod", func(b *testing.B) {
		updateURL := s("/api/v1/update/%s", "vlc")
		form := url.Values{}
		form.Add("channel", "test1")
		form.Add("os", "Windows")
		form.Add("os_version", "10")
		form.Add("os_arch", "x86_64")
		form.Add("product_version", "3.0.10")
		headers := map[string]string{"X-Forwarded-For": "10.0.0.42", "Content-Type": "application/x-www-form-urlencoded"}
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "POST", updateURL, &headers, form.Encode())
			simpleAssert(code == http.StatusFound, "Caramba")
		}
	})
	// full worst case scenario with 20 rules
	b.Run("Request/FULL-20-WORSTRules", func(b *testing.B) {
		url := updateChannel1 + "&os=Windows&os_version=10&os_arch=x86_64&product_version=3.0.10"
		for i := 0; i < b.N; i++ {
			code, _, _ := request(router, "GET", url, &ipHeader, "")
			simpleAssert(code == http.StatusFound, "Caramba")
			code, _, _ = request(router, "GET", release1URL, nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
			code, _, _ = request(router, "GET", release1URL+".asc", nil, "")
			simpleAssert(code == http.StatusOK, "Caramba")
		}
	})

}
