
// Vue rulemodal component
Vue.component('rulemodal', {
  props: ['channel', 'releases'],
  mixins: [fetchMixin, apiMixin, adminAPIMixin, modalMixin],
  data() {
    return {
      ref: "ruleModal",
      rule: {},
      checks: {},
    }
  },
  template: modalMixin.templateHead + `
    <div class="form-group row">
      <label for="rulePriority" class="col-sm-2 col-form-label">Priority</label>
      <div class="col-sm-10"><input id="rulePriority" type="number" class="form-control" placeholder="Rule priority" v-model="rule.priority"></div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Time</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.time"></div></div>
    </div>
    <div class="form-group row" v-if="checks.time">
      <label for="startTimeCheck" class="col-sm-2 col-form-label">Start Time</label>
      <div class="col-sm-10"><input id="startTimeCheck" type="text" class="form-control" placeholder="1970-01-01T00:00:00Z" v-model="rule.timecheck.starttime"></div>
      <label for="endTimeCheck" class="col-sm-2 col-form-label">End Time</label>
      <div class="col-sm-10"><input id="endTimeCheck" type="text" class="form-control" placeholder="1970-01-01T00:00:00Z" v-model="rule.timecheck.endtime"></div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Architecture</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.arch"></div></div>
    </div>
    <div class="form-group row" v-if="checks.arch">
      <label for="archCheck" class="col-sm-2 col-form-label">Architecture</label>
      <div class="col-sm-10">
        <select id="archCheck" class="form-control" placeholder="Architecture" v-model="rule.archcheck.architecture">
          <option v-for="arch in osarchs">{{arch}}</option>
        </select>
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Operating System</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.os"></div></div>
    </div>
    <div class="form-group row" v-if="checks.os">
      <label for="osCheck" class="col-sm-2 col-form-label">OS</label>
      <div class="col-sm-10">
        <select id="osCheck" class="form-control" placeholder="OS" v-model="rule.oscheck.os">
          <option v-for="os in oses">{{os}}</option>
        </select>
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">OS Version</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.osversion"></div></div>
    </div>
    <div class="form-group row" v-if="checks.osversion">
      <label for="osversionCheck" class="col-sm-2 col-form-label">OS Version</label>
      <div class="col-sm-8"><input id="osversionCheck" type="text" class="form-control" placeholder="OS version" v-model="rule.osversioncheck.os_version"></div>
      <div class="col-sm-2">
        <select id="osversionCheckOperator" class="form-control" placeholder="Operator" v-model="rule.osversioncheck.operator">
          <option v-for="op in operators">{{op}}</option>
        </select>
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Product Version</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.version"></div></div>
    </div>
    <div class="form-group row" v-if="checks.version">
      <label for="versionCheck" class="col-sm-2 col-form-label">Version</label>
      <div class="col-sm-8"><input id="versionCheck" type="text" class="form-control" placeholder="Product Version" v-model="rule.versioncheck.productversion"></div>
      <div class="col-sm-2">
        <select id="versionCheckOperator" class="form-control" placeholder="Operator" v-model="rule.versioncheck.operator">
          <option v-for="op in operators">{{op}}</option>
        </select>
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Network Source</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.networksource"></div></div>
    </div>
    <div class="form-group row" v-if="checks.networksource">
      <label for="networksourceCheck" class="col-sm-2 col-form-label">Network/netmask</label>
      <div class="col-sm-7"><input id="networksourceCheck" type="text" class="form-control" placeholder="Network" v-model="rule.networksourcecheck.network"></div>
      <div class="input-group col-sm-3">
        <div class="input-group-prepend">
          <div class="input-group-text">/</div>
        </div>
        <input type="number" class="form-control" placeholder="Mask" v-model="rule.networksourcecheck.netmask" min="0" max="32">
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <div class="col-sm-2">Rolling</div>
      <div class="col-sm-10"><div class="form-check"><input type="checkbox" class="form-check-input" v-model="checks.roll"></div></div>
    </div>
    <div class="form-group row" v-if="checks.roll">
      <label for="rollCheck" class="col-sm-2 col-form-label">Rolling %</label>
      <div class="col-sm-10">
        <input id="rollCheck" type="number" class="form-control" placeholder="Rolling percentage" v-model="rule.rollcheck.rollingpercentage" min="0" max="100">
      </div>
    </div>
    <hr class="separator">
    <div class="form-group row">
      <label for="ruleRelease" class="col-sm-2 col-form-label">Release</label>
      <div class="col-sm-10">
        <select id="ruleRelease" class="form-control" placeholder="Select Release" v-model="rule.release">
          <option v-for="release in releases" v-bind:value="release.id">
            {{release.title}} - {{release.os}} {{release.os_version}} {{release.os_arch}} {{release.signature == '' ? '(Warning: not signed)': ''}}
          </option>
        </select>
      </div>
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let realRule = {
        priority: parseInt(this.rule.priority),
        release: this.rule.release,
        channel: this.channel.id}
      if (this.checks.arch) {
        realRule.archcheck = this.rule.archcheck
      }
      if (this.checks.time) {
        realRule.timecheck = this.rule.timecheck
      }
      if (this.checks.os) {
        realRule.oscheck = this.rule.oscheck
      }
      if (this.checks.osversion) {
        realRule.osversioncheck = this.rule.osversioncheck
      }
      if (this.checks.version) {
        realRule.versioncheck = this.rule.versioncheck
      }
      if (this.checks.networksource) {
        realRule.networksourcecheck = this.rule.networksourcecheck
        realRule.networksourcecheck.netmask = parseInt(realRule.networksourcecheck.netmask)
      }
      if (this.checks.roll) {
        realRule.rollcheck = this.rule.rollcheck
        realRule.rollcheck.rollingpercentage = parseInt(realRule.rollcheck.rollingpercentage)
      }
      let params = {url: '/admin/api/v1/rule', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/rule/${this.rule.id}`, method: 'PUT'}
      } 
      this.fetchOrThrow(params.url, params.method, realRule).then(res => {
	    this.$emit('reload-rule-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(rule) {
      this.resetError()
      this.fetchOperators()
      this.fetchOSes()
      this.fetchOSArchs()
      this.checks = {
        time: false,
        arch: false,
        os: false,
        osversion: false,
        version: false,
        roll: false,
        networksource: false,
      }
      if (typeof(rule) != "undefined") {
        this.modaltype = "modify"
        this.rule = JSON.parse(JSON.stringify(rule)) // clone object
        this.checks = {
          time: (typeof(this.rule.timecheck) != "undefined"),
          arch: (typeof(this.rule.archcheck) != "undefined"),
          os: (typeof(this.rule.oscheck) != "undefined"),
          osversion: (typeof(this.rule.osversioncheck) != "undefined"),
          version: (typeof(this.rule.versioncheck) != "undefined"),
          roll: (typeof(this.rule.rollcheck) != "undefined"),
          networksource: (typeof(this.rule.networksourcecheck) != "undefined"),
        }
        if (typeof(this.rule.timecheck) == "undefined") { this.rule.timecheck = {} }
        if (typeof(this.rule.archcheck) == "undefined") { this.rule.archcheck = {} }
        if (typeof(this.rule.oscheck) == "undefined") { this.rule.oscheck = {} }
        if (typeof(this.rule.osversioncheck) == "undefined") { this.rule.osversioncheck = {} }
        if (typeof(this.rule.versioncheck) == "undefined") { this.rule.versioncheck = {} }
        if (typeof(this.rule.rollcheck) == "undefined") { this.rule.rollcheck = {} }
        if (typeof(this.rule.networksourcecheck) == "undefined") { this.rule.networksourcecheck = {} }
      } else {
        this.modaltype = "create"
        this.rule = {
          priority: 1000,
          timecheck: {},
          archcheck: {},
          oscheck: {},
          osversioncheck: {},
          versioncheck: {},
          rollcheck: {},
          networksourcecheck: {},
        }
      }
      this.show()
    },
  },
  computed: {
    disableAction: function() {
      return !this.rule.release
    },
    title: function() {
      return (this.modaltype == "create") ? "New Rule" : `Modify Rule ${this.rule.id}`
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    },
  }
})


