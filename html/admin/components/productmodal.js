
// Vue productmodal component
Vue.component('productmodal', {
  mixins: [fetchMixin, adminAPIMixin, modalMixin],
  data() {
    return {
      ref: "productModal",
      product: {},
    }
  },
  template: modalMixin.templateHead + `
    <div class="form-group">
      <label for="productName">Name</label>
      <input id="productName" type="text" class="form-control" placeholder="Product name" v-model="product.name">
    </div>
    <div class="form-group">
      <label for="productDescription">Description</label>
      <input id="productDescription" type="text" class="form-control" placeholder="Product Description" v-model="product.description">
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let params = {url: '/admin/api/v1/product', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/product/${this.product.id}`, method: 'PUT'}
      }
      this.fetchOrThrow(params.url, params.method, this.product).then(res => {
        this.$emit('reload-product-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(product) {
      this.resetError()
      if (typeof(product) != "undefined") {
        this.modaltype = "modify"
        this.product = JSON.parse(JSON.stringify(product)) // clone
      } else {
        this.modaltype = "create"
        this.product = {}
      }
      $("#productModal").modal("show")
    },
  },
  computed: {
    title: function() {
      return (this.modaltype == "create") ? "New Product" : `Modify Product ${this.product.id} (${this.product.name})`
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    }
  }
})

