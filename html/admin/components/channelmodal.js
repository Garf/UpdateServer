
// Vue channelmodal component
Vue.component('channelmodal', {
  props: ['product'],
  mixins: [fetchMixin, adminAPIMixin, modalMixin],
  data() {
    return {
      ref: "channelModal",
      channel: {},
    }
  },
  template: modalMixin.templateHead + `
    <div class="form-group">
      <label for="channelName">Name</label>
      <input id="channelName" type="text" class="form-control" placeholder="Channel name" v-model="channel.name">
    </div>
    <div class="form-group">
      <label for="channelSignature">Signature Key</label>
      <select id="channelSignature" class="form-control" placeholder="Public key" v-model="channel.signature_key">
        <option :value="null">No signature</option>
        <option v-for="key in signaturekeys" v-bind:value="key.fingerprint">
          {{key.fingerprint}} ({{key.description}})
        </option>
      </select>
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let params = {url: '/admin/api/v1/channel', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/channel/${this.channel.id}`, method: 'PUT'}
      } 
      this.fetchOrThrow(params.url, params.method, this.channel).then(res => {
	    this.$emit('reload-channel-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(channel) {
      this.resetError()
      this.fetchSignatureKeys()
      if (typeof(channel) != "undefined") {
        this.modaltype = "modify"
        this.channel = JSON.parse(JSON.stringify(channel)) // clone object
      } else {
        this.modaltype = "create"
        this.channel = {name: "", signature_key: null, product: this.product.id}
      }
      this.show()
    },
  },
  computed: {
    title: function() {
      return (this.modaltype == "create") ? "New Channel" : `Modify Channel ${this.channel.id} (${this.channel.name})`
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    },
  }
})

