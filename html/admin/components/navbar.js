
Vue.component('navbar', {
  mixins: [fetchMixin, loginMixin],
  template: `
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/home.html">Update Server</a>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="home.html">Products</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="signaturekeys.html">Signature Keys</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="requests.html">Requests</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" v-if="!isLoggedIn()" href="/login.html">Login</a>
          <a class="nav-link" v-if="isLoggedIn()" href="#" v-on:click="logout">Logout</button>
        </li>
      </ul>
    </div>
  </nav>`,
})


