// regroup most used admin api fetchs in a mixin
// /!\ requires fetchMixin
var adminAPIMixin = {
  data() {
    return {
      product: {},
      channel: {},
      signaturekeys: [],
      products: [],
      channels: [],
      rules: [],
      releaseMap: {},
    }
  },
  methods: {
    fetchSignatureKeys: function() {
      return this.fetchOrThrow('/admin/api/v1/signaturekey', 'GET')
                 .then(data => {this.signaturekeys = data})
                 .catch(console.error)
    },
    fetchProduct: function(id) {
      return this.fetchOrThrow(`/admin/api/v1/product/${id}`, 'GET')
                 .then(data => {this.product = data})
                 .catch(console.error)
    },
    fetchProducts: function() {
      return this.fetchOrThrow('/admin/api/v1/product', 'GET')
                 .then(data => {this.products = data})
                 .catch(console.error)
    },
    fetchChannel: function(id) {
      return this.fetchOrThrow(`/admin/api/v1/channel/${id}`, 'GET')
                 .then(data => {this.channel = data})
                 .catch(console.error)
    },
    fetchChannels: function(id) {
      return this.fetchOrThrow(`/admin/api/v1/product/${id}/channel`, 'GET')
                 .then(data => {this.channels = data})
                 .catch(console.error)
    },
    fetchRules: function(id) {
      return this.fetchOrThrow(`/admin/api/v1/channel/${id}/rule`, 'GET')
                 .then(data => {this.rules = data})
                 .catch(console.error)
    },
    fetchReleaseMap: function(id) {
      return this.fetchOrThrow(`/admin/api/v1/product/${id}/release`, 'GET')
                 .then(data => {
                   let map = {}
                   for (release of data) {               
                     map[""+release.id] = release
                   }
                   this.releaseMap = map
                 })
                 .catch(console.error)
    },
  }
}

// modal prerequisites
var modalMixin = {
  data() {
    return {
      ref: "",
      modaltype: "create",
    }
  },
  methods: {
    hide: function() {
      $(`#${this.ref}`).modal("hide")
    },
    show: function() { 
      $(`#${this.ref}`).modal("show")
    }, 
  },
  computed: {
    disableAction: function() {return false}
  },
  templateHead: `
    <div class="modal fade" :id="ref" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
    	    <h5 class="modal-title" id="exampleModalLongTitle">{{title}}</h5>
    	    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    	      <span aria-hidden="true">&times;</span>
    	    </button>
          </div>
          <form @submit.prevent="action">
    	    <div class="modal-body">
    	      <div class="alert alert-danger" role="alert" v-if="errorText.length > 0">{{errorText}}</div>`,
  templateFoot: `
    	    </div>
    	    <div class="modal-footer">
    	      <button type="submit" class="btn btn-primary" :disabled="disableAction">{{actiontitle}}</button>
    	    </div>
          </form>
        </div>
      </div>
    </div>`,
}
