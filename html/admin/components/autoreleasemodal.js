
// Vue autoreleasemodal component
Vue.component('autoreleasemodal', {
  props: ['channel', 'rules'],
  mixins: [fetchMixin, apiMixin, adminAPIMixin, modalMixin],
  data() {
    return {
      ref: "autoReleaseModal",
      autorelease: {},
    }
  },
  template: modalMixin.templateHead + `

    <div class="form-group">
      <label for="autoreleaseDescription">Description</label>
      <input id="autoreleaseDescription" type="text" class="form-control" placeholder="Description" v-model="autorelease.description">
    </div>
    <div class="form-group">
      <label for="autoreleaseRule">Rule: new discovered releases will be set to this rule</label>
      <select id="autoreleaseRule" class="form-control" placeholder="Rule" v-model="autorelease.rule">
        <option v-for="rule in rules" v-bind:value="rule.id">{{rule.id}} (Priority {{rule.priority}})</option>
      </select>
    </div>
    <div class="form-group">
      <label for="autoreleaseSchedule">Schedule: set to nothing to disable, put a cron format otherwise (ie: "* * * * */5")</label>
      <input id="autoreleaseSchedule" type="text" class="form-control" placeholder="Schedule in cron format" v-model="autorelease.schedule">
    </div>
    <div class="form-group">
      <label for="autoreleaseURL">URL: the root url where the new releases should be uploaded.</label>
      <input id="autoreleaseURL" type="text" class="form-control" placeholder="root URL" v-model="autorelease.url">
    </div>
    <div class="form-group">
      <label for="autoreleaseRegex">Filename regex: should match the new release filename.<br>
        You can add a {{regexpVersion}} submatch to match the new release version. <br>
        Example: {{regexpExampleFull}} will get any {{filenameExample}} file and version will be set to {{versionExample}}.</label>
      <input id="autoreleaseRegex" type="text" class="form-control" placeholder="filename regex" v-model="autorelease.release_filename_regex">
    </div>
    <div class="form-group">
      <label for="autoreleaseReleaseTitle">Release Base Title: new releases will get this title with the version concatenated.</label>
      <input id="autoreleaseReleaseTitle" type="text" class="form-control" placeholder="release base title" v-model="autorelease.release_base_title">
    </div>
    <div class="form-group">
      <label for="autoreleaseReleaseOS">Release OS: new releases will get this OS.</label>
      <select id="autoreleaseReleaseOS" class="form-control" placeholder="release OS" v-model="autorelease.release_os">
        <option v-for="os in oses">{{os}}</option>
      </select>
    </div>
    <div class="form-group">
      <label for="autoreleaseReleaseOSVersion">Release OS Version: new releases will get this OS version.</label>
      <input id="autoreleaseReleaseOSVersion" type="text" class="form-control" placeholder="release OS Version" v-model="autorelease.release_os_version">
    </div>
    <div class="form-group">
      <label for="autoreleaseReleaseOSArch">Release OS Architecture: new releases will get this OS Architecture.</label>
      <select id="autoreleaseReleaseOSArch" class="form-control" placeholder="Architecture" v-model="autorelease.release_os_architecture">
        <option v-for="arch in osarchs">{{arch}}</option>
      </select>
    </div>
    <div class="form-group">
      <label for="autoreleaseDefaultVersion">Release Default Version: new releases will get this version if not found through filename regex.</label>
      <input id="autoreleaseDefaultVersion" type="text" class="form-control" placeholder="release default version" v-model="autorelease.release_product_default_version">
    </div>
    <div class="form-group">
      <label for="signatureKey">Signature Key: will be used to sign the release (needs a private key!)</label>
      <select id="signatureKey" class="form-control" placeholder="Public key" v-model="autorelease.signature_key">
        <option :value="null">Select a signature key</option>
        <option v-for="key in signaturekeys" v-bind:value="key.fingerprint">
          {{key.fingerprint}} ({{key.description}})
        </option>
      </select>
    </div>
    ` + modalMixin.templateFoot,
  methods: {
    action: function() {
      let params = {url: '/admin/api/v1/autorelease', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/autorelease/${this.autorelease.id}`, method: 'PUT'}
      } 
      this.fetchOrThrow(params.url, params.method, this.autorelease).then(res => {
	    this.$emit('reload-autorelease-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(ar) {
      this.resetError()
      this.fetchSignatureKeys()
      this.fetchOSes()
      this.fetchOSArchs()
      if (typeof(ar) != "undefined") {
        this.modaltype = "modify"
        this.autorelease = JSON.parse(JSON.stringify(ar)) // clone object
      } else {
        this.modaltype = "create"
        this.autorelease = {channel: this.channel.id}
      }
      this.show()
    },
    verbatim: function(input) {
      input = input.replace(/&/g, '&amp;');
      input = input.replace(/</g, '&lt;');
      input = input.replace(/>/g, '&gt;');
      return input;
    }
  },
  computed: {
    title: function() {
      return (this.modaltype == "create") ? "New AutoRelease" : `Modify AutoRelease ${this.autorelease.id}`
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    },
    regexpVersion: function() {return "'(?P<version>...)'"},
    regexpExampleFull: function() {return "'^vlc-(?P<version>4\\.0\\.0-[a-zA-Z0-9\\-]*)\\.exe$'"},
    filenameExample: function() {return "'vlc-4.0.0-<alphanumerical>.exe'"},
    versionExample: function() {return "'4.0.0-<alphanumerical>'"},
  }
})


