
// Vue signaturekeymodal component
Vue.component('signaturekeymodal', {
  mixins: [modalMixin],
  mixins: [fetchMixin, modalMixin],
  data() {
    return {
      ref: "signaturekeyModal",
      signaturekey: {},
      oldFingerprint: null,
    }
  },
  template: modalMixin.templateHead + `
    <div class="form-group">
      <label for="signaturekeyDescription">Description</label>
      <input id="signaturekeyDescription" type="text" class="form-control" placeholder="SignatureKey Description" v-model="signaturekey.description">
    </div>
    <div class="form-group">
      <label for="signaturekeyPub">Public key</label>
      <textarea id="signaturekeyPub" class="form-control" placeholder="Public key" :readonly="modaltype == 'modify'" v-model="signaturekey.pubkey"></textarea>
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let params = {url: '/admin/api/v1/signaturekey', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/signaturekey/${this.oldFingerprint}`, method: 'PUT'}
      } 
      this.fetchOrThrow(params.url, params.method, this.signaturekey).then(res => {
	    this.$emit('reload-signaturekey-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(signaturekey) {
      this.resetError()
      if (typeof(signaturekey) != "undefined") {
        this.modaltype = "modify"
        this.signaturekey = JSON.parse(JSON.stringify(signaturekey))
        this.oldFingerprint = this.signaturekey.fingerprint
      } else {
        this.modaltype = "create"
        this.signaturekey = {description: null, pubkey: null}
        this.oldFingerprint = null
      }
      this.show()
    },
  },
  computed: {
    title: function() {
      return (this.modaltype == "create") ? "New Signature Key" : "Modify Signature Key"
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    }
  }
})


