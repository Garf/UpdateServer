// Vue signreleasemodal component
Vue.component('signreleasemodal', {
  mixins: [fetchMixin, adminAPIMixin, modalMixin],
  data() {
    return {
      ref: "signModal",
      release: {},
      signatureKey : null,
      signature: "",
      manifest: "",
      title: "Sign Release",
      actiontitle: "Sign"
    }
  },
  template: modalMixin.templateHead + `
    <p>
      You are about to sign the release <b>{{release.title}} ({{release.description}})</b>.<br>
    </p>
    <div class="form-group">
      <label for="releaseSignature">Release Manifest</label>
      <textarea readonly disabled id="manifest" class="form-control" rows=3 v-html="manifest"></textarea>
    </div>
    <div class="form-group">
      <label for="signatureKey">Signature Key</label>
      <select id="signatureKey" class="form-control" placeholder="Public key" v-model="signatureKey">
                <option :value="null">Select a signature key</option>
        <option v-for="key in signaturekeys" v-bind:value="key">
          {{key.fingerprint}} ({{key.description}})
        </option>
      </select>
    </div>
    <div v-if="signatureKey">
      <div class="form-group">
        <label for="releaseSignature">
          To sign with this key, open a terminal and lauch the following command:
          <button class="btn btn-secondary" v-on:click="copyGPGCmd(this)">Copy cmd</button>
        </label>
        <textarea readonly id="gpgCmdline" class="form-control" rows=5 v-html="commandline"></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="releaseSignature">Signature</label>
      <textarea id="releaseSignature" class="form-control" placeholder="Put here the result of the previous command line" v-model="signature"></textarea>
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let data = {fingerprint: this.signatureKey.fingerprint , signature: this.signature}
      this.fetchOrThrow(`/admin/api/v1/release/${this.release.id}/sign`, 'POST', data)
          .then(res => {
            this.$emit('reload-release-list')
            this.hide()})
          .catch(this.displayError)
    },
    showModal: function(release) {
      this.resetError()
      this.fetchSignatureKeys()
      this.signatureKey = null
      this.signature = ""
      this.manifest = ""
      if (typeof(release) != "undefined") {
	    this.release = JSON.parse(JSON.stringify(release))
        // Get release manifest (which should be considered as text)
        fetch(`/admin/api/v1/release/${this.release.id}/manifest`, {method: 'GET'})
          .then(res => {
            if (res.status != 200) {
              throw Error(res.statusText)
            }
            return res.text()})
          .then(data => {this.manifest = data})
          .catch(this.displayError)
      }
      this.show()
    },
    copyGPGCmd: function () {
      var textArea = document.getElementById("gpgCmdline");
      textArea.select()
      textArea.setSelectionRange(0, 99999) // for mobile device ?
      document.execCommand("copy")
    },
  },
  computed: {
    commandline: function() {
      return `echo -n '${this.manifest}' | gpg --default-key ${this.signatureKey.fingerprint} --detach-sign -a`
    },
    disableAction: function() {
      return (!this.signatureKey || this.signature == '')
    },
  }
})

