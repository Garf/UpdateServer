
// Vue releasemodal component
Vue.component('releasemodal', {
  props: ['product'],
  mixins: [fetchMixin, adminAPIMixin, apiMixin, modalMixin],
  data() {
    return {
      ref: "releaseModal",
      release: { hashes: {} },
    }
  },
  template: modalMixin.templateHead + `
    <div class="form-group">
      <label for="releaseTitle">Title</label>
      <input id="releaseTitle" type="text" class="form-control" placeholder="Release title" v-model="release.title">
    </div>
    <div class="form-group">
      <label for="releaseDescription">Description</label>
      <input id="releaseDescription" type="text" class="form-control" placeholder="Release description" v-model="release.description">
    </div>
    <div class="form-group">
      <label for="releaseURL">URL</label>
      <input id="releaseURL" type="text" class="form-control" placeholder="Release URL" v-model="release.url">
    </div>
    <div class="form-group">
      <label for="releaseOS">OS</label>
      <select id="releaseOS" class="form-control" placeholder="Release OS" v-model="release.os">
        <option v-for="o in oses">{{o}}</option>
      </select>
    </div>
    <div class="form-group">
      <label for="releaseOSVersion">OS Version</label>
      <input id="releaseOSVersion" type="text" class="form-control" placeholder="Release OS Version" v-model="release.os_version">
    </div>
    <div class="form-group">
      <label for="releaseOSArch">OS Architecture</label>
      <select id="releaseOSArch" class="form-control" placeholder="Release OS Architecture" v-model="release.os_arch">
        <option v-for="arch in osarchs">{{arch}}</option>
      </select>
    </div>
    <div class="form-group">
      <label for="releaseProductVersion">Product Version</label>
      <input id="releaseProductVersion" type="text" class="form-control" placeholder="Release product version" v-model="release.product_version">
    </div>
    <div class="form-group">
      <label for="releaseSha256">Hashes: sha256</label>
      <input id="releaseSha256" type="text" class="form-control" placeholder="Release sha256 checksum hash" v-model="release.hashes.sha256">
      <label for="releaseSha512">Hashes: sha512</label>
      <input id="releaseSha512" type="text" class="form-control" placeholder="Release sha512 checksum hash" v-model="release.hashes.sha512">
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      let params = {url: '/admin/api/v1/release', method: 'POST'} // create
      if (this.modaltype != "create") {
        params = {url: `/admin/api/v1/release/${this.release.id}`, method: 'PUT'}
      } 
      this.fetchOrThrow(params.url, params.method, this.release).then(res => {
        this.$emit('reload-release-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(release) {
      this.resetError()
      this.fetchOSes()
      this.fetchOSArchs()
      if (typeof(release) != "undefined") {
        this.modaltype = "modify"
        this.release = JSON.parse(JSON.stringify(release)) // clone object
      } else {
        this.modaltype = "create"
        this.release = {
          id: null,
          title: null,
          description: null,
          url: null,
          os: null,
          os_version: null,
          os_arch: null,
          product: this.product.id,
          product_version: null,
          hashes: {sha256: null, sha512: null}}
      }
      this.show()
    },
  },
  computed: {
    title: function() {
      return (this.modaltype == "create") ? "New Release" : `Modify Release ${this.release.id} (${this.release.title})`
    },
    actiontitle: function() {
      return (this.modaltype == "create") ? "Create" : "Modify"
    }
  }
})

