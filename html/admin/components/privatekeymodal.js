
// Vue privatekeymodal component
Vue.component('privatekeymodal', {
  mixins: [modalMixin],
  mixins: [fetchMixin, modalMixin],
  data() {
    return {
      ref: "privatekeyModal",
      signaturekey: {},
      privatekey: "",
      title: "Set a private key",
      actiontitle: "Set private key",
    }
  },
  template: modalMixin.templateHead + `
    <div class="alert alert-warning" role="alert">
      <p>Adding a private key is only meant for nightlies automatic signature.</p>
      <p>Do NOT add a private key for a stable/beta release here!</p>
      <p>Also note that the private key will not be persisted in database: it will be reset at next service restart.</p>
    </div>
    <div class="form-group">
      <label for="signaturekeyPriv">Private key</label>
      <textarea id="signaturekeyPriv" class="form-control" placeholder="Private key (not very secure, optional)" v-model="privatekey"></textarea>
    </div>`
    + modalMixin.templateFoot,
  methods: {
    action: function() {
      this.fetchOrThrow(`/admin/api/v1/signaturekey/${this.signaturekey.fingerprint}/privatekey`, 'POST', {privatekey: this.privatekey}).then(res => {
	    this.$emit('reload-signaturekey-list')
        this.hide()
      }).catch(this.displayError)
    },
    showModal: function(signaturekey) {
      this.resetError()
      this.signaturekey = JSON.parse(JSON.stringify(signaturekey))
      this.privatekey = ""
      this.show()
    },
  },
})


