// Mixin components

// Custom Error that includes a json
class CustomError extends Error {
  constructor(json, ...params) {
    super(...params)
    // Maintains proper stack trace
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError)
    }
    this.name = 'CustomError'
    this.json = json
  }
}

// A fetch + error message mixin
var fetchMixin = {
  data() {
    return {
      errorText: "",
    }
  },
  methods: {
    fetchOrThrow: function(url, method, json) {
      this.resetError()
      let fetchArgs = {method: method}
      if (typeof json !== "undefined") {
        fetchArgs.body = JSON.stringify(json)
      }
      return fetch(url, fetchArgs).then(res => {
	    if (!res.ok) { // throw error if code != 200
	      throw new CustomError(res.json().catch(console.warn), res.statusText)
	    }
	    return res.json()
      })
    },
    displayError: function(error) {
      error.json.then(j => {
        let text = (typeof(j.field) != "undefined" && j.field != "") ? `${j.error}: ${j.field}` : j.error
        if (typeof(j.field) != "undefined" && j.description != "") {
          text += ` (${j.description})`
        }
        this.errorText = text
      }).catch(error => {
        this.errorText = "" + error
      })
    },
    resetError: function() {this.errorText = ""},
  },
}


// regroup most used api fetchs in a mixin
// /!\ requires fetchMixin
var apiMixin = {
  data() {
    return {
      products: [],
      channels: [],
      oses: [],
      osarchs: [],
      operators: [],
    }
  },
  methods: {
    fetchPubProducts: function() {
      return this.fetchOrThrow('/api/v1/product', 'GET')
                 .then(data => {this.products = data})
                 .catch(console.error)
    },
    fetchPubChannels: function(id) {
      return this.fetchOrThrow(`/api/v1/product/${id}/channel`, 'GET')
                 .then(data => {this.channels = data})
                 .catch(console.error)
    },
    fetchOSes: function() {
      return this.fetchOrThrow('/api/v1/os', 'GET')
                 .then(data => {this.oses = data})
                 .catch(console.error)
    },
    fetchOSArchs: function() {
      return this.fetchOrThrow('/api/v1/osarch', 'GET')
                 .then(data => {this.osarchs = data})
                 .catch(console.error)
    },
    fetchOperators: function() {
      return this.fetchOrThrow('/api/v1/operator', 'GET')
                 .then(data => {this.operators = data})
                 .catch(console.error)
    },
  }
}

// A login/logout mixin
var loginMixin = {
  data() {
    return {
    }
  },
  methods: {
    logout: function () {
      console.log("trying to logout...")
      this.fetchOrThrow("/logout", "GET").then(res => {
        console.log("Logout: " + JSON.stringify(res))
        this.setCookie("UpdateServerSession", "", -1) // expire cookie
        window.location.replace("/home.html");
      }).catch(console.error)
    },
    isLoggedIn: function() {
        return (this.getCookie("UpdateServerSession") != "")
    },
    // cookie management, from https://www.w3schools.com/js/js_cookies.asp
    getCookie: function(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    },
    setCookie: function(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;sameSite=strict";
    },
  },
}


