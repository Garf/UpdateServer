
Vue.component('navbar', {
  mixins: [fetchMixin, loginMixin],
  template: `
  <nav class="navbar navbar-expand-md navbar-light" v-bind:style="navStyle" v-bind:class="navClass" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="/home.html">Update Server</a>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" v-if="getCookie('UpdateServerSession')" href="/admin/dashboard/home.html">Administration</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" v-if="!isLoggedIn()" href="/login.html">Login</a>
          <a class="nav-link" v-if="isLoggedIn()" href="#" v-on:click="logout">Logout</button>
        </li>
      </ul>
    </div>
  </nav>`,
  computed: {
    navStyle: function() {
      return this.isLoggedIn() ? {'background-color': '#e3f2fd'} : {}
    },
    navClass: function() {
      return this.isLoggedIn() ? "navbar navbar-expand-md navbar-dark bg-dark" : "navbar navbar-expand-md navbar-light" 
    }
  },
})


