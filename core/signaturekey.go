package core

import (
	"code.videolan.org/videolan/updateserver/cache"
	"code.videolan.org/videolan/updateserver/model"
)

// GetSignatureKeys return all keys recorded at the model orded by id
func GetSignatureKeys() (*[]model.SignatureKey, error) {
	keys := []model.SignatureKey{}
	results, err := redis.List(&model.SignatureKey{})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		key, err := model.SignatureKey{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		keys = append(keys, key)
	}
	for i := 0; i < len(keys); i++ {
		if _, found := cache.GetPrivateKey(keys[i].Fingerprint); found {
			keys[i].HasPrivateKey = true
		}
	}
	return &keys, nil
}

// GetSignatureKey returns a signature key based on its fingerprint
func GetSignatureKey(fingerprint string) (*model.SignatureKey, error) {
	key := model.SignatureKey{Fingerprint: fingerprint}
	if err := redis.GetJson(key.GetIdentifier(), &key); err != nil {
		return nil, err
	}
	if _, found := cache.GetPrivateKey(key.Fingerprint); found {
		key.HasPrivateKey = true
	}
	return &key, nil
}

// NewSignatureKey create a new key associated with a public key
func NewSignatureKey(key *model.SignatureKey) error {
	if key == nil {
		return model.ErrorInvalid("", "Cannot create empty key")
	}
	if key.PublicKey == "" {
		return model.ErrorInvalid("PublicKey", "Public key cannot be empty")
	}
	if err := key.UpdateFingerprint(); err != nil {
		return model.ErrorInvalid("PublicKey", "Public key cannot be parsed")
	}
	result, err := redis.New(key)
	if err != nil {
		return err
	}
	*key, err = model.SignatureKey{}.FromJsonStr(result)
	return err
}

// ModifySignatureKey modifies an existing key
func ModifySignatureKey(fingerprint string, key *model.SignatureKey) error {
	oldKey, err := GetSignatureKey(fingerprint)
	if err != nil {
		return err
	}
	if oldKey.PublicKey != key.PublicKey {
		return model.ErrorInvalid("PublicKey", "Public key cannot be changed")
	}
	key.Fingerprint = oldKey.Fingerprint
	return redis.Modify(key)
}

// SetSignaturePrivateKey checks and store signature private key in the cache
func SetSignaturePrivateKey(fingerprint string, privateKey string) error {
	key, err := GetSignatureKey(fingerprint)
	if err != nil {
		return err
	}
	if err := key.CheckPrivateKey(privateKey); err != nil {
		return model.ErrorInvalid("privatekey", err.Error())
	}
	cache.SetPrivateKey(fingerprint, privateKey)
	return nil
}

func RemoveSignaturePrivateKey(fingerprint string) error {
	_, err := GetSignatureKey(fingerprint)
	if err != nil {
		return err
	}
	cache.DeletePrivateKey(fingerprint)
	return nil
}

// DeleteSignatureKey removes a key. Return the deleted object to let the client perform some checks.
func DeleteSignatureKey(fingerprint string) (*model.SignatureKey, error) {
	key, err := GetSignatureKey(fingerprint)
	if err != nil {
		return nil, err
	}
	if err := redis.Delete(key); err != nil {
		return nil, err
	}
	cache.DeletePrivateKey(fingerprint)
	return key, nil
}
