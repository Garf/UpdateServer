package core

import (
	"crypto"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	nethttp "net/http"
	"regexp"
	"strings"

	"code.videolan.org/videolan/updateserver/cron"
	"code.videolan.org/videolan/updateserver/model"
	"golang.org/x/net/html"
)

func expandAutorelease(autorelease *model.AutoRelease) error {
	if channel, err := GetChannel(autorelease.ChannelID); err != nil {
		return err
	} else {
		autorelease.Channel = *channel
	}
	if key, err := GetSignatureKey(autorelease.ReleaseSignatureKeyFingerprint); err != nil {
		return err
	} else {
		autorelease.ReleaseSignatureKey = *key
	}
	if rule, err := GetRule(autorelease.RuleID); err != nil {
		return err
	} else {
		autorelease.Rule = *rule
	}
	return nil
}

func GetAutoReleases(channelID uint) (*[]model.AutoRelease, error) {
	_, err := GetChannel(channelID)
	if err != nil {
		return nil, err
	}
	result := []model.AutoRelease{}
	autoreleases, err := GetAllAutoReleases()
	if err != nil {
		return nil, err
	}
	for _, a := range *autoreleases {
		if a.ChannelID == channelID {
			result = append(result, a)
		}
	}
	return &result, nil
}

func GetAllAutoReleases() (*[]model.AutoRelease, error) {
	autoreleases := []model.AutoRelease{}
	results, err := redis.List(&model.AutoRelease{})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		a, err := model.AutoRelease{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		if err := expandAutorelease(&a); err != nil {
			return nil, err
		}
		autoreleases = append(autoreleases, a)
	}
	return &autoreleases, nil
}

func GetAutoRelease(id uint) (*model.AutoRelease, error) {
	autorelease := model.AutoRelease{ID: id}
	if err := redis.GetJson(autorelease.GetIdentifier(), &autorelease); err != nil {
		return nil, err
	}
	if err := expandAutorelease(&autorelease); err != nil {
		return nil, err
	}
	return &autorelease, nil
}

func fullCheckAutoRelease(autorelease *model.AutoRelease) error {
	if err := autorelease.CheckFields(); err != nil {
		return err
	}
	rule, err := GetRule(autorelease.RuleID)
	if err != nil {
		return setErrorField(err, "rule")
	}
	if _, err := GetChannel(autorelease.ChannelID); err != nil {
		return setErrorField(err, "channel")
	}
	if rule.ChannelID != autorelease.ChannelID {
		return model.ErrorInvalid("rule", "Rule's channel is not the same as autorelease's channel")
	}
	return nil
}

func NewAutoRelease(autorelease *model.AutoRelease) (err error) {
	if err := fullCheckAutoRelease(autorelease); err != nil {
		return err
	}
	autorelease.ID = 0
	result, err := redis.NewWithID(autorelease)
	if err != nil {
		return err
	}
	*autorelease, err = model.AutoRelease{}.FromJsonStr(result)
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			redis.Delete(autorelease)
		}
	}()
	if err := cron.RegisterAutoRelease(autorelease, func() { CheckNewRelease(autorelease.ID) }); err != nil {
		return model.ErrorInvalid("schedule", err.Error())
	}
	return nil
}

func ModifyAutoRelease(autorelease *model.AutoRelease) error {
	if _, err := GetAutoRelease(autorelease.ID); err != nil {
		return err
	}
	if err := fullCheckAutoRelease(autorelease); err != nil {
		return err
	}
	if err := autorelease.CheckFields(); err != nil {
		return err
	}
	if err := cron.RegisterAutoRelease(autorelease, func() { CheckNewRelease(autorelease.ID) }); err != nil {
		return model.ErrorInvalid("schedule", err.Error())
	}
	return redis.Modify(autorelease)
}

func DeleteAutorelease(id uint) (*model.AutoRelease, error) {
	autorelease, err := GetAutoRelease(id)
	if err != nil {
		return nil, err
	}
	if err := redis.Delete(autorelease); err != nil {
		return nil, err
	}
	cron.UnregisterAutoRelease(autorelease.ID)
	return autorelease, nil
}

func RegisterAllAutoRelease() error {
	allAutoReleases, err := GetAllAutoReleases()
	if err != nil {
		return err
	}
	for _, autoRelease := range *allAutoReleases {
		if err := cron.RegisterAutoRelease(&autoRelease, func() { CheckNewRelease(autoRelease.ID) }); err != nil {
			return err
		}
	}
	return nil
}

func CheckNewRelease(id uint) (err error) {
	defer func() {
		if err != nil {
			log.Println("OUCH", err.Error())
		}
	}()
	autorelease, err := GetAutoRelease(id)
	if err != nil {
		return err
	}
	log.Println("Checking URL ", autorelease.URL)
	rootURL, filename, version, err := getLastFilenameURL(autorelease.URL, autorelease.ReleaseFilenameRegex)
	releaseURL := rootURL + filename
	if err != nil {
		return model.ErrorGeneric(err.Error())
	}
	log.Println("Found last release URL: ", releaseURL)
	log.Println("Found last release version: ", version)
	if version == "" {
		version = autorelease.ReleaseProductDefaultVersion
	}
	product, err := GetProduct(autorelease.Channel.ProductID)
	if err != nil {
		return err
	}
	releases, err := GetReleases(product.ID)
	if err != nil {
		return err
	}
	var release *model.Release = nil
	for _, r := range *releases {
		if r.URL == releaseURL {
			release = &r
			log.Println("Already found a release with the same URL: ", release.ID)
			break
		}
	}
	if release == nil {
		// create release
		log.Println("Cannot find release in database, creating a new one")
		hashes, err := httpDownloadAndChecksum(releaseURL)
		if err != nil {
			return model.ErrorGeneric(err.Error())
		}
		// Check
		ok, err := checkHashes(rootURL, filename, *hashes)
		if err != nil {
			return model.ErrorGeneric(err.Error())
		}
		if !ok {
			return model.ErrorGeneric("File checks failed!")
		}
		log.Println("File Checks said OK")
		title := fmt.Sprintf("%s %s", autorelease.ReleaseBaseTitle, version)
		release = &model.Release{
			Title:          title,
			Description:    title,
			URL:            releaseURL,
			OS:             autorelease.ReleaseOS,
			OsVersion:      autorelease.ReleaseOsVersion,
			OsArchitecture: autorelease.ReleaseOsArchitecture,
			ProductID:      autorelease.Channel.ProductID,
			Product:        product,
			ProductVersion: version,
			Hashes:         *hashes,
		}
		if err := NewRelease(release); err != nil {
			return err
		}
		log.Println("Release created: ", release.ID)
	}
	rule := autorelease.Rule
	if rule.ReleaseID == release.ID {
		log.Println("Release is already set in Rule, nothing to do")
		return nil
	}
	log.Println("Signing release ", release.ID)
	release, err = SignRelease(release.ID, autorelease.ReleaseSignatureKeyFingerprint, "")
	if err != nil {
		return err
	}
	log.Println("Replacing the release in the appropriate rule")
	rule.ReleaseID = release.ID
	if err := ModifyRule(&rule); err != nil {
		return err
	}
	log.Println("Rule updated!")
	return nil
}

func checkHashes(rootURL string, filename string, hashes model.ReleaseHashes) (bool, error) {
	sha512sumFilename := "SHA512SUM"
	log.Println("Downloading ", rootURL+sha512sumFilename)
	resp, err := nethttp.Get(rootURL + sha512sumFilename)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return false, fmt.Errorf("URL %s returned status code %d", rootURL+sha512sumFilename, resp.StatusCode)
	}
	bbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	for _, line := range strings.Split(string(bbody), "\n") {
		fields := strings.Fields(line)
		// check if there is <sha512sum> <filename> or <sha512sum> "./<filename>"
		if len(fields) >= 2 && (fields[1] == filename || fields[1] == "./"+filename) {
			log.Println("Found ", filename, " in ", sha512sumFilename)
			if fields[0] == hashes.Sha512 {
				return true, nil
			} else {
				return false, nil
			}
		}
	}
	return false, fmt.Errorf("Cannot find a checksum file for %s", rootURL+filename)
}

func httpDownloadAndChecksum(url string) (*model.ReleaseHashes, error) {
	log.Println("Downloading...")
	resp, err := nethttp.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("URL %s returned status code %d", url, resp.StatusCode)
	}
	sha256 := crypto.SHA256.New()
	sha512 := crypto.SHA512.New()
	multipass := io.MultiWriter(sha256, sha512)
	io.Copy(multipass, resp.Body)
	sha256sum := fmt.Sprintf("%x", sha256.Sum(nil))
	sha512sum := fmt.Sprintf("%x", sha512.Sum(nil))
	log.Println("SHA256 should be ", sha256sum)
	log.Println("SHA512 should be ", sha512sum)

	return &model.ReleaseHashes{
		Sha256: sha256sum,
		Sha512: sha512sum,
	}, nil
}

func htmlparse(url string) (*html.Node, error) {
	resp, err := nethttp.Get(url)
	// handle the error if there is one
	if err != nil {
		return nil, err
	}
	// do this now so it won't be forgotten
	defer resp.Body.Close()
	// reads html as a slice of bytes
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("URL %s returned status code %d", url, resp.StatusCode)
	}
	return html.Parse(resp.Body)
}

func getAllLinks(node *html.Node) []string {
	// recursive parsing: get all <a href="..."></a>
	var links []string
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" {
					links = append(links, a.Val)
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(node)
	return links
}

// Returns <root_url>, <filename>, <detected_version>, <error>
func getLastFilenameURL(url string, filenamePattern string) (string, string, string, error) {
	exp, err := regexp.Compile(filenamePattern)
	if err != nil {
		return "", "", "", err
	}
	// ensure url ends with "/"
	if url[len(url)-1:] != "/" {
		url = url + "/"
	}
	doc, err := htmlparse(url)
	if err != nil {
		return "", "", "", err
	}
	links := getAllLinks(doc)
	// Extremely naive selection: get the first non "parent" link
	if len(links) == 0 || (len(links) == 1 && links[0] == "../") {
		return "", "", "", fmt.Errorf("Cannot find a valid link in the URL %s", url)
	}
	link := links[0]
	if link == "../" && len(link) > 0 {
		link = links[1]
	}
	log.Println("Found directory link: ", link)
	// Now get all files links in directory
	lastReleaseDirectoryURL := fmt.Sprintf("%s%s", url, link)
	if lastReleaseDirectoryURL[len(lastReleaseDirectoryURL)-1:] != "/" {
		lastReleaseDirectoryURL = lastReleaseDirectoryURL + "/"
	}
	doc, err = htmlparse(lastReleaseDirectoryURL)
	if err != nil {
		return "", "", "", err
	}
	filenames := getAllLinks(doc)
	for _, filename := range filenames {
		if exp.MatchString(filename) {
			log.Println("Found a matching filename: ", filename)
			// Try to determine the version: "version" should be in subexpnames
			version := ""
			for i, name := range exp.SubexpNames() {
				if i != 0 && name == "version" {
					match := exp.FindStringSubmatch(filename)
					version = match[i]
					break
				}
			}
			return lastReleaseDirectoryURL, filename, version, nil
		}
	}
	return "", "", "", fmt.Errorf("Cannot find a valid filename in %s", link)
}
