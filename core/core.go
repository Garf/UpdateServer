package core

import (
	"code.videolan.org/videolan/updateserver/config"
	"code.videolan.org/videolan/updateserver/model"
)

var (
	redis       model.Redis
	requestsMax int64 = 1000
)

// SetRedis set redis conn
func SetRedis(r model.Redis) {
	redis = r
}

// Config configures core variables
func Config(c *config.Configuration) {
	requestsMax = c.Cache.MaxRequests
}

// If the error is a Error, set the "field" field
func setErrorField(err error, field string) error {
	if modelError, ok := err.(model.Error); ok {
		modelError.Field = field
		return modelError
	}
	return err
}
