package core

import (
	"fmt"
	"sort"

	"code.videolan.org/videolan/updateserver/model"
	"github.com/hashicorp/go-version"
)

// GetRules function return all the rules associated with a channel
func GetRules(channelID uint) (*[]model.Rule, error) {
	rules := []model.Rule{}
	_, err := GetChannel(channelID)
	if err != nil {
		return nil, err
	}
	results, err := redis.List(&model.Rule{ChannelID: channelID})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		rule, err := model.Rule{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		rules = append(rules, rule)
	}
	// sort by priority asc
	sort.Slice(rules, func(i int, j int) bool {
		return rules[i].Priority < rules[j].Priority
	})
	return &rules, nil
}

func GetActiveRules(channelID uint) (*[]model.Rule, error) {
	rules, err := GetRules(channelID)
	if err != nil {
		return nil, err
	}
	isReleaseSigned := map[uint]bool{}
	activeRules := []model.Rule{}
	for _, rule := range *rules {
		isSigned, ok := isReleaseSigned[rule.ReleaseID]
		if !ok {
			release, err := GetRelease(rule.ReleaseID)
			if err != nil {
				return nil, err
			}
			isSigned = release.Signature != ""
			isReleaseSigned[release.ID] = isSigned
		}
		if isSigned {
			activeRules = append(activeRules, rule)
		}
	}
	return &activeRules, nil
}

// GetRule returns a rule based on its id
func GetRule(id uint) (*model.Rule, error) {
	rule := model.Rule{ID: id}
	if err := redis.GetJson(rule.GetIdentifier(), &rule); err != nil {
		return nil, err
	}
	return &rule, nil
}

func fullCheckRule(rule *model.Rule) error {
	if _, err := GetChannel(rule.ChannelID); err != nil {
		return setErrorField(err, "channel")
	}
	if _, err := GetRelease(rule.ReleaseID); err != nil {
		return setErrorField(err, "release")
	}
	if rule.OSVersionCheck != nil {
		if rule.OSVersionCheck.Operator.IsValid() != nil {
			return model.ErrorInvalid("osversioncheck", "Operator is invalid")
		}
		if _, err := version.NewVersion(rule.OSVersionCheck.OSVersion); err != nil {
			return model.ErrorInvalid("osversioncheck", "OS Version is invalid")
		}
	}
	if rule.VersionCheck != nil {
		if rule.VersionCheck.Operator.IsValid() != nil {
			return model.ErrorInvalid("versioncheck", "Operator is invalid")
		}
		if _, err := version.NewVersion(rule.VersionCheck.ProductVersion); err != nil {
			return model.ErrorInvalid("versioncheck", "OS Version is invalid")
		}
	}
	return nil
}

// NewRule function create a new rule
func NewRule(rule *model.Rule) (e error) {
	if err := fullCheckRule(rule); err != nil {
		return err
	}
	rule.ID = 0
	result, err := redis.NewWithID(rule)
	if err != nil {
		return err
	}
	*rule, err = model.Rule{}.FromJsonStr(result)
	if err == nil {
		UpdateRulesCache(rule.ChannelID)
	}
	return err
}

// ModifyRule modifies an existing rule
func ModifyRule(rule *model.Rule) (e error) {
	oldRule, err := GetRule(rule.ID)
	if err != nil {
		return err
	}
	if err := fullCheckRule(rule); err != nil {
		return err
	}
	if rule.ChannelID != oldRule.ChannelID {
		// Forbid channel switch
		return model.ErrorInvalid("channel", "You cannot change a rule's channel")
	}
	err = redis.Modify(rule)
	if err == nil {
		UpdateRulesCache(rule.ChannelID)
	}
	return err
}

// DeleteRule removes a rule
func DeleteRule(id uint) (r *model.Rule, e error) {
	rule, err := GetRule(id)
	if err != nil {
		return nil, err
	}
	if err := redis.Delete(rule); err != nil {
		return nil, err
	}
	UpdateRulesCache(rule.ChannelID)
	return rule, nil
}

func switchPriorities(rule1 *model.Rule, rule2 *model.Rule) (err error) {
	priority1 := rule1.Priority
	priority2 := rule2.Priority
	rule1.Priority = -1
	if err := redis.Modify(rule1); err != nil {
		return err
	}
	defer func() {
		if err != nil {
			rule1.Priority = priority1
			redis.Modify(rule1)
		}
	}()
	rule2.Priority = priority1
	if err := redis.Modify(rule2); err != nil {
		return err
	}
	defer func() {
		if err != nil {
			rule2.Priority = priority2
			redis.Modify(rule2)
		}
	}()
	rule1.Priority = priority2
	return redis.Modify(rule1)
}

// UpRule raises a Rule priority enough to be above the previous rule
func UpRule(id uint) (r *model.Rule, e error) {
	rule, err := GetRule(id)
	if err != nil {
		return nil, err
	}
	// list rules of that channel
	rules, err := GetRules(rule.ChannelID)
	if err != nil {
		return nil, err
	}
	var previousRule *model.Rule = nil
	for index, r := range *rules {
		if r.ID == rule.ID && index > 0 {
			previousRule = &(*rules)[index-1]
			break
		}
	}
	if previousRule != nil {
		if err := switchPriorities(rule, previousRule); err != nil {
			return nil, err
		}
	}
	UpdateRulesCache(rule.ChannelID)
	return rule, nil
}

// DownRule lowers a Rule priority enough to be below the next rule
func DownRule(id uint) (r *model.Rule, e error) {
	rule, err := GetRule(id)
	if err != nil {
		return nil, err
	}
	// list rules of that channel
	rules, err := GetRules(rule.ChannelID)
	if err != nil {
		return nil, err
	}
	var nextRule *model.Rule = nil
	rulesLen := len(*rules)
	for index, r := range *rules {
		if r.ID == rule.ID && index < (rulesLen-1) {
			nextRule = &(*rules)[index+1]
			break
		}
	}
	if nextRule != nil {
		if err := switchPriorities(rule, nextRule); err != nil {
			return nil, err
		}
	}
	UpdateRulesCache(rule.ChannelID)
	return rule, nil
}

// GetRulesByNames retrieve rules from product name and channel name
// use "" for channelName if you want to retrieve default channel rules
func GetRulesCache(productName string, channelName string) (*[]model.Rule, error) {
	var rules []model.Rule
	err := redis.GetJson(rulesByNamesKey(productName, channelName), &rules)
	if err != nil {
		return nil, err
	}
	return &rules, nil
}

// UpdateRulesCache update rules list stored in cache
// updates both:
// - cache by product name + channel name,
// - cache by product name for default channel if necessary
// Only store active rules (those related to signed releases), and in the right order
func UpdateRulesCache(channelID uint) error {
	channel, err := GetChannel(channelID)
	if err != nil {
		return err
	}
	product, err := GetProduct(channel.ProductID)
	if err != nil {
		return err
	}
	rules, err := GetActiveRules(channel.ID)
	if err != nil {
		return err
	}
	key := rulesByNamesKey(product.Name, channel.Name)
	if err := redis.StoreJson(key, rules); err != nil {
		return err
	}
	if product.DefaultChannelID != nil && *product.DefaultChannelID == channel.ID {
		if err := redis.StoreJson(key, rules); err != nil {
			return err
		}
		defaultKey := rulesByNamesKey(product.Name, "") // default channel
		if err := redis.StoreJson(defaultKey, rules); err != nil {
			return err
		}
	}
	return nil
}

func rulesByNamesKey(productName string, channelName string) string {
	return fmt.Sprintf("/rulesbynames/%s/%s/", productName, channelName)
}

func RulesCacheOnProductChange(oldProduct *model.Product, newProduct *model.Product) error {
	if oldProduct.Name == newProduct.Name &&
		(oldProduct.DefaultChannelID == nil && newProduct.DefaultChannelID == nil ||
			oldProduct.DefaultChannelID != nil && newProduct.DefaultChannelID != nil &&
				*oldProduct.DefaultChannelID == *newProduct.DefaultChannelID) {
		return nil
	}
	if oldProduct.Name != newProduct.Name {
		toMove := map[string]string{}
		channels, err := GetChannels(oldProduct.ID)
		if err != nil {
			return err
		}
		for _, c := range *channels {
			toMove[rulesByNamesKey(oldProduct.Name, c.Name)] = rulesByNamesKey(newProduct.Name, c.Name)
		}
		if err := redis.RenameKeys(toMove); err != nil {
			return err
		}
	}
	if newProduct.DefaultChannelID != nil {
		// new default channel, ok if product change, or oldProduct.DefaultChannelID != newProduct.DefaultChannelID
		UpdateRulesCache(*newProduct.DefaultChannelID)
	}
	if oldProduct.DefaultChannelID != nil {
		// old product default channel
		if err := redis.DeleteKey(rulesByNamesKey(oldProduct.Name, "")); err != nil {
			return err
		}
	}
	return nil
}

func RulesCacheOnChannelChange(oldChannel *model.Channel, newChannel *model.Channel) error {
	toMove := map[string]string{}
	if oldChannel.Name != newChannel.Name {
		product, err := GetProduct(oldChannel.ID)
		if err != nil {
			return err
		}
		toMove[rulesByNamesKey(product.Name, oldChannel.Name)] = rulesByNamesKey(product.Name, newChannel.Name)
		return redis.RenameKeys(toMove)
	}
	return nil
}

func RulesCacheOnChannelDelete(channel *model.Channel) error {
	product, err := GetProduct(channel.ID)
	if err != nil {
		return err
	}
	return redis.DeleteKey(rulesByNamesKey(product.Name, channel.Name))
}

func RulesCacheOnReleaseSignChange(release *model.Release) error {
	channels, err := GetChannels(release.ProductID)
	if err != nil {
		return err
	}
	for _, channel := range *channels {
		rules, err := GetRules(channel.ID)
		if err != nil {
			return err
		}
		for _, rule := range *rules {
			if rule.ReleaseID == release.ID {
				UpdateRulesCache(channel.ID)
				break
			}
		}
	}
	return nil
}
