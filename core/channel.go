package core

import (
	"code.videolan.org/videolan/updateserver/model"
)

// GetChannels returns all channels for a given product recorded at the database ordered by name
func GetChannels(productID uint) (*[]model.Channel, error) {
	channels := []model.Channel{}
	if _, err := GetProduct(productID); err != nil {
		return nil, err
	}
	results, err := redis.List(&model.Channel{ProductID: productID})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		channel, err := model.Channel{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		channels = append(channels, channel)
	}

	return &channels, nil
}

// GetChannel returns a channel based on its id
func GetChannel(id uint) (*model.Channel, error) {
	channel := model.Channel{ID: id}
	if err := redis.GetJson(channel.GetIdentifier(), &channel); err != nil {
		return nil, err
	}
	return &channel, nil
}

func fullCheckChannel(channel *model.Channel) error {
	if err := channel.CheckFields(); err != nil {
		return err
	}
	if _, err := GetProduct(channel.ProductID); err != nil {
		return err
	}
	if channel.SignatureKeyFingerprint != nil {
		if _, err := GetSignatureKey(*channel.SignatureKeyFingerprint); err != nil {
			return err
		}
	}
	return nil
}

// NewChannel create a new channel
func NewChannel(channel *model.Channel) error {
	if channel == nil {
		return model.ErrorInvalid("", "Cannot create empty channel")
	}
	if err := fullCheckChannel(channel); err != nil {
		return err
	}
	channel.ID = 0
	result, err := redis.NewWithID(channel)
	if err != nil {
		return err
	}
	*channel, err = model.Channel{}.FromJsonStr(result)
	return err
}

// ModifyChannel modifies an existing channel
func ModifyChannel(channel *model.Channel) (e error) {
	oldChannel, err := GetChannel(channel.ID)
	if err != nil {
		return err
	}
	if err := fullCheckChannel(channel); err != nil {
		return err
	}
	if channel.ProductID != oldChannel.ProductID {
		// Forbid product switch
		return model.ErrorInvalid("product", "You cannot change a channel's product")
	}
	err = redis.Modify(channel)
	if err == nil {
		RulesCacheOnChannelChange(oldChannel, channel)
	}
	return err
}

// DeleteChannel removes a channel.
// Return the deleted object to let the client perform some checks.
func DeleteChannel(id uint) (c *model.Channel, err error) {
	channel, err := GetChannel(id)
	if err != nil {
		return nil, err
	}
	product, err := GetProduct(channel.ProductID)
	if err != nil {
		return nil, err
	}
	if product.DefaultChannelID != nil && *(product.DefaultChannelID) == channel.ID {
		product.DefaultChannelID = nil
		if err := ModifyProduct(product); err != nil {
			return nil, err
		}
	}
	if err := redis.Delete(channel); err != nil {
		return nil, err
	}
	RulesCacheOnChannelDelete(channel)
	return channel, nil
}
