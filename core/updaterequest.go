package core

import (
	"encoding/json"

	"code.videolan.org/videolan/updateserver/model"
)

// AddRequest adds a request to the cached requests
func AddRequest(request *model.UpdateRequest) error {
	b, err := json.Marshal(request)
	if err != nil {
		return err
	}
	if err := redis.AddInList(request.GetRedisListRoot(), string(b)); err != nil {
		return err
	}
	// trim
	if err := redis.TrimList(request.GetRedisListRoot(), 0, requestsMax-1); err != nil {
		return err
	}
	return nil
}

// GetPaginatedRequests get requests from cache, filtered and paginated.
// Returns the requested array and the total number of requests that match the filter
func GetPaginatedRequests(filter model.UpdateRequest, limit uint, offset uint) (*[]model.UpdateRequest, uint, error) {
	res := []model.UpdateRequest{}
	var innerIndex uint = 0
	requestsStr, err := redis.GetList((&model.UpdateRequest{}).GetRedisListRoot(), 0, requestsMax)
	if err != nil {
		return nil, 0, err
	}
	for _, requestStr := range requestsStr {
		request := model.UpdateRequest{}
		if err := json.Unmarshal([]byte(requestStr), &request); err != nil {
			return nil, 0, err
		}
		if filter.Channel != "" && filter.Channel != request.Channel {
			continue
		}
		if filter.OS != "" && filter.OS != request.OS {
			continue
		}
		if filter.OsArchitecture != "" && filter.OsArchitecture != request.OsArchitecture {
			continue
		}
		// request is matching filters
		if innerIndex >= offset && len(res) < int(limit) {
			res = append(res, request)
		}
		innerIndex++
	}
	return &res, innerIndex, nil
}
