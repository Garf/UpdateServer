package core

import (
	"code.videolan.org/videolan/updateserver/cache"
	"code.videolan.org/videolan/updateserver/model"
)

func extendRelease(release *model.Release) error {
	if product, err := GetProduct(release.ProductID); err != nil {
		return err
	} else {
		release.Product = product
	}
	if signature, err := redis.Get(release.GetSignatureKey()); err == nil {
		release.Signature = signature
	}
	return nil
}

// GetReleases return all releases for a given product ordered by id
func GetReleases(productID uint) (*[]model.Release, error) {
	releases := []model.Release{}
	if _, err := GetProduct(productID); err != nil {
		return nil, err
	}
	results, err := redis.List(&model.Release{ProductID: productID})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		release, err := model.Release{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		if err := extendRelease(&release); err != nil {
			return nil, err
		}
		releases = append(releases, release)
	}
	return &releases, nil
}

// GetRelease returns a release based on its id
func GetRelease(id uint) (*model.Release, error) {
	release := model.Release{ID: id}
	if err := redis.GetJson(release.GetIdentifier(), &release); err != nil {
		return nil, err
	}
	if err := extendRelease(&release); err != nil {
		return nil, err
	}
	return &release, nil
}

func fullCheckRelease(release *model.Release) error {
	if err := release.CheckFields(); err != nil {
		return err
	}
	if _, err := GetProduct(release.ProductID); err != nil {
		return err
	}
	return nil
}

// NewRelease creates a new release
// Ignore release <-> signature/signaturekey association: it is managed during the signing process
func NewRelease(release *model.Release) error {
	release.ResetSignature()
	if err := fullCheckRelease(release); err != nil {
		return err
	}
	release.ID = 0
	result, err := redis.NewWithID(release)
	if err != nil {
		return err
	}
	*release, err = model.Release{}.FromJsonStr(result)
	return err
}

// ModifyRelease modifies an existing release
// Ignore release <-> signaturekey association: it is managed during the signing process
func ModifyRelease(release *model.Release) error {
	oldRelease, err := GetRelease(release.ID)
	if err != nil {
		return err
	}
	if err := fullCheckRelease(release); err != nil {
		return err
	}
	if release.ProductID != oldRelease.ProductID {
		// Forbid product switch
		return model.ErrorInvalid("product", "You cannot change a channel's product")
	}
	release.ResetSignature()
	err = redis.Modify(release)
	if err == nil {
		redis.DeleteKey(release.GetManifestKey())
		redis.DeleteKey(release.GetSignatureKey())
		RulesCacheOnReleaseSignChange(release)
	}
	return err
}

// DeleteRelease removes a release
func DeleteRelease(id uint) (*model.Release, error) {
	release, err := GetRelease(id)
	if err != nil {
		return nil, err
	}
	if err := redis.Delete(release); err != nil {
		return nil, err
	}
	redis.DeleteKey(release.GetManifestKey())
	redis.DeleteKey(release.GetSignatureKey())
	return release, nil
}

// SignRelease adds a valid signature to a given release
func SignRelease(id uint, fingerprint string, signature string) (r *model.Release, e error) {
	release, err := GetRelease(id)
	if err != nil {
		return nil, err
	}
	signatureKey, err := GetSignatureKey(fingerprint)
	if err != nil {
		return nil, err
	}
	var manifest string
	if signature == "" {
		// sign with the signatureKey private key if any
		privateKey, found := cache.GetPrivateKey(signatureKey.Fingerprint)
		if !found {
			return nil, model.ErrorInvalid("signature", "No private key given or found")
		}
		manifest, signature, err = signatureKey.AutoSign(release, privateKey)
		if err != nil {
			return nil, err
		}
	} else {
		manifest, signature, err = signatureKey.ExternalSign(release, signature)
		if err != nil {
			return nil, err
		}
	}
	if err := redis.Store(release.GetManifestKey(), manifest); err != nil {
		return nil, err
	}
	if err := redis.Store(release.GetSignatureKey(), signature); err != nil {
		return nil, err
	}
	if err := redis.Modify(release); err != nil {
		return nil, err
	}
	release.Signature = signature
	RulesCacheOnReleaseSignChange(release)
	return release, nil
}

func GetReleaseManifest(releaseId uint) (string, error) {
	r := model.Release{ID: releaseId}
	if manifest, err := redis.Get(r.GetManifestKey()); err == nil {
		return manifest, nil
	}
	release, err := GetRelease(releaseId)
	if err != nil {
		return "", err
	}
	return release.GetManifest()
}

func GetReleaseSignature(releaseId uint) (string, error) {
	r := model.Release{ID: releaseId}
	return redis.Get(r.GetSignatureKey())
}
