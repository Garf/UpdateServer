package core

import (
	"code.videolan.org/videolan/updateserver/model"
)

// GetProducts return all products recorded at the database orded by name
func GetProducts() (*[]model.Product, error) {
	products := []model.Product{}
	results, err := redis.List(&model.Product{})
	if err != nil {
		return nil, err
	}
	for _, r := range results {
		product, err := model.Product{}.FromJsonStr(r)
		if err != nil {
			return nil, err
		}
		products = append(products, product)
	}
	return &products, nil
}

// GetProduct returns a product by its id
func GetProduct(id uint) (*model.Product, error) {
	product := model.Product{ID: id}
	if err := redis.GetJson(product.GetIdentifier(), &product); err != nil {
		return nil, err
	}
	if product.DefaultChannelID != nil {
		channel, err := GetChannel(*product.DefaultChannelID)
		if err != nil {
			return nil, err
		}
		product.DefaultChannel = channel
	}
	return &product, nil
}

// NewProduct creates a new product
func NewProduct(product *model.Product) error {
	if product == nil {
		return model.ErrorInvalid("", "Cannot create empty product")
	}
	if err := product.CheckFields(); err != nil {
		return err
	}
	result, err := redis.NewWithID(product)
	if err != nil {
		return err
	}
	product.ID = 0
	*product, err = model.Product{}.FromJsonStr(result)
	return err
}

// ModifyProduct modifies an existing product
func ModifyProduct(product *model.Product) error {
	oldProduct, err := GetProduct(product.ID)
	if err != nil {
		return err
	}
	if err := product.CheckFields(); err != nil {
		return err
	}
	if product.DefaultChannelID != nil {
		channel, err := GetChannel(*product.DefaultChannelID)
		if err != nil {
			return setErrorField(err, "default_channel")
		}
		product.DefaultChannel = channel
	}
	//if err := db.DB.Save(&product).Error; err != nil {
	//	return model.ErrorFromGorm(err)
	//}
	err = redis.Modify(product)
	if err == nil {
		RulesCacheOnProductChange(oldProduct, product)
	}
	return err
}

// DeleteProduct deletes a product. Return the deleted object to let the client perform some checks.
func DeleteProduct(id uint) (*model.Product, error) {
	product, err := GetProduct(id)
	if err != nil {
		return nil, err
	}
	if err := redis.Delete(product); err != nil {
		return nil, err
	}
	return product, nil
}
