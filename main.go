package main

import (
	"flag"
	"math/rand"
	"time"

	"code.videolan.org/videolan/updateserver/cache"
	"code.videolan.org/videolan/updateserver/config"
	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/cron"
	"code.videolan.org/videolan/updateserver/http"
	"code.videolan.org/videolan/updateserver/model"
	"code.videolan.org/videolan/updateserver/utils"
)

var (
	addr  string
	redis model.Redis
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func startService(addr string) {
	// Get configuration
	c, err := config.Get()
	if err != nil {
		panic(err)
	}

	// Init cache
	cache.Config(c)

	// Init Redis
	redis.Connect(c)
	core.SetRedis(redis)

	// Init & Start Cron
	cron.Init()
	cron.Start()
	core.RegisterAllAutoRelease()

	// Launch Http server
	router := http.RouterInit(c)
	router.Run(":" + addr)
}

func main() {
	var generateHash bool
	flag.StringVar(&addr, "port", "8080", "The port server will be running on")
	flag.BoolVar(&generateHash, "passwordhash", false, "Password Hash generator mode. Will only help generate a valid configuration password format.")
	flag.Parse()

	if generateHash {
		utils.PasswordHashPrompt()
	} else {
		startService(addr)
	}

}
