package main

import (
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestUpdate(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create two releases
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))
	releaseID2 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.43", "description": "", "url": "http://lolilol2", "os": "macOS", "os_version": "10", "os_arch": "x86", "product": %d, "product_version": "3.0.43", "hashes": {"sha256": "42ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "42688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))

	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// add a private key
	AssertAPIOK(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")

	// Sign release1
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s"}`, finger1))

	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))

	test1RulePrefix := s("%s/%d/rule", channelPrefix, channelID1)

	// List (empty)
	AssertAPIOK(t, router, "GET", test1RulePrefix, "", "[]")

	updateChannel1 := s("/api/v1/update/%s?channel=test1", "vlc")
	release1URL := s("/api/v1/release/%d/manifest", releaseID1)
	release2URL := s("/api/v1/release/%d/manifest", releaseID2)

	// no rule, no chocolate
	AssertAPIErrorNotFound(t, router, "GET", updateChannel1+"&os=Windows", "")

	release1ChannelStr := s(`"release": %d, "channel": %d`, releaseID1, channelID1)
	release2ChannelStr := s(`"release": %d, "channel": %d`, releaseID2, channelID1)

	// Create empty rule
	id := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 1000, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 1000, `+release1ChannelStr+` }`)
	fmt.Println("The ID: ", id)
	// An empty rule matches anything
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)

	// A rule with an unsigned release never matches
	// Create an empty rule with release2
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 100, "release": %d, "channel": %d}`, releaseID2, channelID1),
		`{"id": %d, "priority": 100, `+release2ChannelStr+` }`)
	// matches lower rule == release1
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	// sign release2
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID2),
		s(`{"fingerprint": "%s"}`, finger1))
	// now matches release2
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release2URL)
	// clean
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "")

	// Rule Update CHECKS
	// Test process:
	// - creating a "any" rule that redirects to release1
	// - creating a rule to test with higher priority and that redirects to release2
	// - poking the update URL:
	//   - if it returns release1 -> no match
	//   - if it returns release2 -> match

	// A rule with a < priority level will match better
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 100, "release": %d, "channel": %d}`, releaseID2, channelID1),
		`{"id": %d, "priority": 100, `+release2ChannelStr+` }`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release2URL)
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "")

	// CHECKS
	firstPart := s(`{"priority": 100, "release": %d, "channel": %d, `, releaseID2, channelID1)
	firstPartID := firstPart + `"id": %d, `
	// Time Check
	// match
	now := time.Now().Format(time.RFC3339)
	nowPlus10Seconds := time.Now().Add(10 * time.Second).Format(time.RFC3339)
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"timecheck": {"starttime": "`+now+`", "endtime": "`+nowPlus10Seconds+`"}}`,
		firstPartID+`"timecheck": {"starttime": "`+now+`", "endtime": "`+nowPlus10Seconds+`"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release2URL)
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "")
	// no match
	nowMinus20Seconds := time.Now().Add(-20 * time.Second).Format(time.RFC3339)
	nowMinus10Seconds := time.Now().Add(-10 * time.Second).Format(time.RFC3339)
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"timecheck": {"starttime": "`+nowMinus20Seconds+`", "endtime": "`+nowMinus10Seconds+`"}}`,
		firstPartID+`"timecheck": {"starttime": "`+nowMinus20Seconds+`", "endtime": "`+nowMinus10Seconds+`"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "")

	// ArchCheck
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"archcheck": {"architecture": "x86_64"}}`,
		firstPartID+`"archcheck": {"architecture": "x86_64"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_arch=x86_64", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_arch=x86", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"archcheck": {"architecture": "x86_64"}}`, id))
	// OSCheck
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"oscheck": {"os": "Windows"}}`,
		firstPartID+`"oscheck": {"os": "Windows"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=macOS", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"", "", release1URL)
	// POST request Test
	form := url.Values{}
	form.Add("channel", "test1")
	form.Add("os", "Windows")
	AssertAPIRedirectOKWithHeaders(t, router, "POST", s("/api/v1/update/%s", "vlc"), map[string]string{"Content-Type": "application/x-www-form-urlencoded"}, form.Encode(), release2URL)
	form = url.Values{}
	form.Add("channel", "test1")
	form.Add("os", "macOS")
	AssertAPIRedirectOKWithHeaders(t, router, "POST", s("/api/v1/update/%s", "vlc"), map[string]string{"Content-Type": "application/x-www-form-urlencoded"}, form.Encode(), release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "", s(firstPartID+`"oscheck": {"os": "Windows"}}`, id))
	// OSVersionCheck
	// EQ
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.0.0", "operator": "EQ"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "EQ"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "EQ"}}`, id))
	// NEQ
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.0", "operator": "NEQ"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.0", "operator": "NEQ"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=12", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.0", "operator": "NEQ"}}`, id))
	// GT
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.0.0", "operator": "GT"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "GT"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=12.1", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.0.0", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=9.99.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "GT"}}`, id))
	// GTE
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.0.0", "operator": "GTE"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "GTE"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=12.1", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.0.0", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=9.99.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.0.0", "operator": "GTE"}}`, id))
	// LT
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.2.12", "operator": "LT"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.2.12", "operator": "LT"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=9.12.42-toto", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.2.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.3.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.2.12", "operator": "LT"}}`, id))
	// LTE
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"osversioncheck": {"os_version": "10.2.12", "operator": "LTE"}}`,
		firstPartID+`"osversioncheck": {"os_version": "10.2.12", "operator": "LTE"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=9.12.42-toto", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.2.12", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&os_version=10.3.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"osversioncheck": {"os_version": "10.2.12", "operator": "LTE"}}`, id))
	// VersionCheck
	// EQ
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "EQ"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "EQ"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.12", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.10", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "EQ"}}`, id))
	// NEQ
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "NEQ"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "NEQ"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.10", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "NEQ"}}`, id))
	// GT
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "GT"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "GT"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.14", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.2", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "GT"}}`, id))
	// GTE
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "GTE"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "GTE"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.14", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.12", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.0.2", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.0.12", "operator": "GTE"}}`, id))
	// LT
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.2.12", "operator": "LT"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.2.12", "operator": "LT"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.1.14", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.2.12", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=4.0.3", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.2.12", "operator": "LT"}}`, id))
	// LTE
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"versioncheck": {"productversion": "3.2.12", "operator": "LTE"}}`,
		firstPartID+`"versioncheck": {"productversion": "3.2.12", "operator": "LTE"}}`)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.1.14", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=3.2.12", "", release2URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows&product_version=4.0.3", "", release1URL)
	AssertAPIRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"versioncheck": {"productversion": "3.2.12", "operator": "LTE"}}`, id))
	// RollCheck
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"rollcheck": {"rollingpercentage": 50}}`,
		firstPartID+`"rollcheck": {"rollingpercentage": 50}}`)
	redirectURLs := map[string]bool{
		release1URL: false,
		release2URL: false,
	}
	for i := 0; i < 100; i++ {
		url := AssertAPIStatusRedirectOK(t, router, "GET", updateChannel1+"&os=Windows", "")
		fmt.Println("Found redirect URL: ", url)
		switch url {
		case release1URL:
			redirectURLs[release1URL] = true
		case release2URL:
			redirectURLs[release2URL] = true
		default:
			require.Fail(t, "This is an unpexpected redirect URL: "+url)
		}
	}
	if !redirectURLs[release1URL] || !redirectURLs[release2URL] {
		// the probability to not have both releases is 1 / 2^100 , which seems unlikely enough
		require.Fail(t, "Did not have both releases!")
	}
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"rollcheck": {"rollingpercentage": 50}}`, id))
	// NetworkCheck
	id = AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		firstPart+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`,
		firstPartID+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`)
	ipHeader := map[string]string{"X-Forwarded-For": "10.0.0.42"}
	AssertAPIRedirectOKWithHeaders(t, router, "GET", updateChannel1+"&os=Windows", ipHeader, "", release2URL)
	ipHeader = map[string]string{"X-Forwarded-For": "10.0.1.12"}
	AssertAPIRedirectOKWithHeaders(t, router, "GET", updateChannel1+"&os=Windows", ipHeader, "", release1URL)
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		s(firstPartID+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`, id))

}
