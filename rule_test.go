package main

import (
	"testing"
	"time"
)

func TestRule(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	// Create a release
	releaseID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", releasePrefix,
		s(`{"title": "VLC 3.0.42", "description": "", "url": "http://lolilol", "os": "Windows", "os_version": "10", "os_arch": "x86_64", "product": %d, "product_version": "3.0.42", "hashes": {"sha256": "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b", "sha512": "be688838ca8686e5c90689bf2ab585cef1137c999b48c70b92f67a5c34dc15697b5d11c982ed6d71be1e1e7f7b4e0733884aa97c3f7a339a8ed03577cf74be09"} }`, productID1)))
	// Create a signature
	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))

	product1ChannelPrefix := s("%s/%d/channel", productPrefix, productID1)
	// List Channels (empty)
	AssertAPIOK(t, router, "GET", product1ChannelPrefix, "", "[]")
	// Create Channel1
	channelID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))
	// Create Channel2
	channelID2 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test2", "product": %d}`, productID1)))
	test1RulePrefix := s("%s/%d/rule", channelPrefix, channelID1)
	test2RulePrefix := s("%s/%d/rule", channelPrefix, channelID2)

	// List (empty)
	AssertAPIOK(t, router, "GET", test1RulePrefix, "", "[]")

	// Real Create
	releaseChannelStr := s(`"release": %d, "channel": %d`, releaseID1, channelID1)
	id := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 10, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 10, `+releaseChannelStr+` }`)

	// Sign release
	releaseSignature1 := getKeyFromFile("testdata/release_signature_with_this_key_is_for_tests_only.asc")
	AssertAPIStatusOK(t, router, "POST", s("%s/%d/sign", releasePrefix, releaseID1),
		s(`{"fingerprint": "%s", "signature": "%s"}`, finger1, releaseSignature1))

	// Cannot remove a channel or a release with an associated rule
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%d", channelPrefix, channelID1), "")
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%d", releasePrefix, releaseID1), "")

	// Cannot create a rule with the same priority
	AssertAPIErrorUniqueConstraint(t, router, "POST", rulePrefix,
		s(`{"priority": 10, "release": %d, "channel": %d}`, releaseID1, channelID1))
	id2 := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 12, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 12, `+releaseChannelStr+` }`)
	// Get again
	AssertAPIOK(t, router, "GET", test1RulePrefix, "",
		s(`[{"id": %d, "priority": 10, %s}, {"id": %d, "priority": 12, %s}]`, id, releaseChannelStr, id2, releaseChannelStr))
	// List channel2 rules (empty)
	AssertAPIOK(t, router, "GET", test2RulePrefix, "", "[]")
	// Direct Get
	AssertAPIOK(t, router, "GET", s("%s/%d", rulePrefix, id), "",
		s(`{"id": %d, "priority": 10, %s}`, id, releaseChannelStr))
	// Modify rule priority
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		s(`{"id": %d, "priority": 42, %s}`, id, releaseChannelStr),
		s(`{"id": %d, "priority": 42, %s}`, id, releaseChannelStr))

	// Modify checks
	firstPart := s(`{"id": %d, "priority": 42, "release": %d, "channel": %d, `, id, releaseID1, channelID1)
	// Time Check
	// match
	now := time.Now().Format(time.RFC3339)
	nowPlus10Seconds := time.Now().Add(10 * time.Second).Format(time.RFC3339)
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"timecheck": {"starttime": "`+now+`", "endtime": "`+nowPlus10Seconds+`"}}`,
		firstPart+`"timecheck": {"starttime": "`+now+`", "endtime": "`+nowPlus10Seconds+`"}}`)
	// ArchCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"archcheck": {"architecture": "x86_64"}}`,
		firstPart+`"archcheck": {"architecture": "x86_64"}}`)
	// OSCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"oscheck": {"os": "Windows"}}`,
		firstPart+`"oscheck": {"os": "Windows"}}`)
	// OSVersionCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"osversioncheck": {"os_version": "10.0.0", "operator": "EQ"}}`,
		firstPart+`"osversioncheck": {"os_version": "10.0.0", "operator": "EQ"}}`)
	// OSVersionCheck errors
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%d", rulePrefix, id), firstPart+`"osversioncheck": {"os_version": "eee10", "operator": "EQ"}}`)
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%d", rulePrefix, id), firstPart+`"osversioncheck": {"os_version": "10", "operator": "lol"}}`)
	// VersionCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "EQ"}}`,
		firstPart+`"versioncheck": {"productversion": "3.0.12", "operator": "EQ"}}`)
	// VersionCheck errors
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%d", rulePrefix, id), firstPart+`"versioncheck": {"productversion": "eee10", "operator": "EQ"}}`)
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%d", rulePrefix, id), firstPart+`"versioncheck": {"productversion": "10", "operator": "lol"}}`)
	// RollCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"rollcheck": {"rollingpercentage": 50}}`,
		firstPart+`"rollcheck": {"rollingpercentage": 50}}`)
	// NetworkCheck
	AssertAPIOK(t, router, "PUT", s("%s/%d", rulePrefix, id),
		firstPart+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`,
		firstPart+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`)
	// Delete
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id), "",
		firstPart+`"networksourcecheck": {"network": "10.0.0.0", "netmask": 24}}`)
	AssertAPIErrorNotFound(t, router, "DELETE", s("%s/%d", rulePrefix, id), "")
	AssertAPIErrorNotFound(t, router, "GET", s("%s/%d", rulePrefix, id), "")
	AssertAPIOK(t, router, "DELETE", s("%s/%d", rulePrefix, id2), "",
		s(`{"id": %d, "priority": 12, %s}`, id2, releaseChannelStr))
	// Relist (empty)
	AssertAPIOK(t, router, "GET", test1RulePrefix, "", "[]")

	// Up/Down Checks
	uid1 := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 10, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 10, `+releaseChannelStr+` }`)
	uid2 := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 20, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 20, `+releaseChannelStr+` }`)
	uid3 := AssertAPIOKAndGetID(t, router, "POST", rulePrefix,
		s(`{"priority": 30, "release": %d, "channel": %d}`, releaseID1, channelID1),
		`{"id": %d, "priority": 30, `+releaseChannelStr+` }`)
	// Up (no change): uid1 (priority 10), uid2 (priority 20), uid3 (priority 30)
	AssertAPIOK(t, router, "PUT", s("%s/%d/up", rulePrefix, uid1), "",
		s(`{"id": %d, "priority": 10, %s }`, uid1, releaseChannelStr))
	// Down: uid2 (priority 10), uid1 (priority 20), uid3 (priority 30)
	AssertAPIOK(t, router, "PUT", s("%s/%d/down", rulePrefix, uid1), "",
		s(`{"id": %d, "priority": 20, %s }`, uid1, releaseChannelStr))
	// Down: uid2 (priority 10), uid3 (priority 20), uid1 (priority 30)
	AssertAPIOK(t, router, "PUT", s("%s/%d/down", rulePrefix, uid1), "",
		s(`{"id": %d, "priority": 30, %s }`, uid1, releaseChannelStr))
	// Down (no change): uid2 (priority 10), uid3 (priority 20), uid1 (priority 30)
	AssertAPIOK(t, router, "PUT", s("%s/%d/down", rulePrefix, uid1), "",
		s(`{"id": %d, "priority": 30, %s }`, uid1, releaseChannelStr))
	// List
	AssertAPIOK(t, router, "GET", test1RulePrefix, "",
		s(`[{"id": %d, "priority": 10, %s}, {"id": %d, "priority": 20, %s}, {"id": %d, "priority": 30, %s}]`, uid2, releaseChannelStr, uid3, releaseChannelStr, uid1, releaseChannelStr))
	// Up: uid2 (priority 10), uid1 (priority 20), uid3 (priority 30)
	AssertAPIOK(t, router, "PUT", s("%s/%d/up", rulePrefix, uid1), "",
		s(`{"id": %d, "priority": 20, %s }`, uid1, releaseChannelStr))
	// List
	AssertAPIOK(t, router, "GET", test1RulePrefix, "",
		s(`[{"id": %d, "priority": 10, %s}, {"id": %d, "priority": 20, %s}, {"id": %d, "priority": 30, %s}]`, uid2, releaseChannelStr, uid1, releaseChannelStr, uid3, releaseChannelStr))

	// Delete again
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, uid1), "")
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, uid2), "")
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", rulePrefix, uid3), "")
	// Can remove release and channel now
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", releasePrefix, releaseID1), "")
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", channelPrefix, channelID1), "")
}
