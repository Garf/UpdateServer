package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestChannel(t *testing.T) {
	router := initTest(t)

	// Create Product
	productID1 := getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", productPrefix,
		`{"name": "vlc", "description": "A cØØl multimedia player"}`))

	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"

	product1ChannelPrefix := s("%s/%d/channel", productPrefix, productID1)

	// List Channels (empty)
	AssertAPIOK(t, router, "GET", product1ChannelPrefix, "", "[]")
	// Get Channel that does not exists
	AssertAPIErrorNotFound(t, router, "GET", s("%s/%d", channelPrefix, 42), "")
	// Create
	body := AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1))
	channelID1 := getIDFromJSON(t, body)
	require.JSONEq(t, s(`{"id": %d, "name": "test1", "product": %d}`, channelID1, productID1), body, s("POST %s", channelPrefix))
	// Cannot remove a product with an associated channel
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%d", productPrefix, productID1), "")

	prefixChannelID1 := s("%s/%d", channelPrefix, channelID1)
	// Cannot create without a product
	AssertAPIErrorNotFound(t, router, "POST", channelPrefix, `{"name": "test12422"}`)
	// Cannot recreate the same channel
	AssertAPIErrorUniqueConstraint(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1))
	// Cannot create a channel with empty name
	AssertAPIErrorRequired(t, router, "POST", channelPrefix,
		s(`{"name": "", "product": %d}`, productID1))
	// Get Channel
	AssertAPIOK(t, router, "GET", prefixChannelID1, "",
		s(`{"id": %d, "name": "test1", "product": %d}`, channelID1, productID1))
	// Modify
	AssertAPIOK(t, router, "PUT", prefixChannelID1,
		s(`{"name": "test2", "product": %d}`, productID1),
		s(`{"id": %d, "name": "test2", "product": %d}`, channelID1, productID1))
	// Delete
	AssertAPIOK(t, router, "DELETE", prefixChannelID1, "",
		s(`{"id": %d, "name": "test2", "product": %d}`, channelID1, productID1))
	// Cannot Modify that does not exists
	AssertAPIErrorNotFound(t, router, "PUT", prefixChannelID1,
		s(`{"name": "test12422", "product": %d}`, productID1))
	// Cannot Delete again
	AssertAPIErrorNotFound(t, router, "DELETE", prefixChannelID1, "")
	// Create a channel with a key
	// First create a signature key
	AssertAPIStatusOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1))
	// Recreate a channel
	channelID1 = getIDFromJSON(t, AssertAPIStatusOK(t, router, "POST", channelPrefix,
		s(`{"name": "test1", "product": %d}`, productID1)))
	prefixChannelID1 = s("%s/%d", channelPrefix, channelID1)
	// Add key to channel
	AssertAPIOK(t, router, "PUT", prefixChannelID1,
		s(`{"name": "test1", "signature_key": "%s", "product": %d}`, finger1, productID1),
		s(`{"id": %d, "name": "test1", "signature_key": "%s", "product": %d}`, channelID1, finger1, productID1))
	// Cannot remove a signaturekey with an associated channel
	AssertAPIErrorForeignKeyConstraint(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "")
	// Cannot add a fake key
	AssertAPIErrorNotFound(t, router, "PUT", prefixChannelID1,
		s(`{"name": "test1", "signature_key": "lol", "product": %d}`, productID1))
	// get channel with key
	AssertAPIOK(t, router, "GET", prefixChannelID1, "",
		s(`{"id": %d, "name": "test1", "signature_key": "%s", "product": %d}`, channelID1, finger1, productID1))
	// List final channels
	AssertAPIOK(t, router, "GET", product1ChannelPrefix, "",
		s(`[{"id": %d, "name": "test1", "signature_key": "%s", "product": %d}]`, channelID1, finger1, productID1))
	// Remove key
	AssertAPIOK(t, router, "PUT", prefixChannelID1,
		s(`{"name": "test1", "product": %d}`, productID1),
		s(`{"id": %d, "name": "test1", "product": %d}`, channelID1, productID1))
	// Get final form of channel
	AssertAPIOK(t, router, "GET", prefixChannelID1, "", s(`{"id": %d, "name": "test1", "product": %d}`, channelID1, productID1))
	// delete channel
	AssertAPIOK(t, router, "DELETE", prefixChannelID1, "", s(`{"id": %d, "name": "test1", "product": %d}`, channelID1, productID1))
	// Now we can remove product
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%d", productPrefix, productID1), "")
	// Now we can remove the signaturekey
	AssertAPIStatusOK(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "")
}
