package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	nethttp "net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"

	"code.videolan.org/videolan/updateserver/cache"
	"code.videolan.org/videolan/updateserver/config"
	"code.videolan.org/videolan/updateserver/core"
	"code.videolan.org/videolan/updateserver/cron"
	"code.videolan.org/videolan/updateserver/http"
	"code.videolan.org/videolan/updateserver/model"
	"code.videolan.org/videolan/updateserver/utils"
)

const (
	testingConfigMaxRequests = 42
	productPrefix            = "/admin/api/v1/product"
	signaturePrefix          = "/admin/api/v1/signaturekey"
	channelPrefix            = "/admin/api/v1/channel"
	rulePrefix               = "/admin/api/v1/rule"
	releasePrefix            = "/admin/api/v1/release"
	requestPrefix            = "/admin/api/v1/request/paginated"
	autoreleasePrefix        = "/admin/api/v1/autorelease"
	loginPrefix              = "/login"
	logoutPrefix             = "/logout"
	cookiePrefix             = "UpdateServerSession="
)

var (
	username     = "admin"
	password     = "qwerty0"
	globalCookie = ""
	initOnce     sync.Once
	router       *gin.Engine
)

// shortcut for fmt.Sprintf
func s(format string, a ...interface{}) string {
	return fmt.Sprintf(format, a...)
}

func getStringValueFromJSONString(s string, field string) (string, error) {
	var arbitraryJSON map[string]interface{}
	json.Unmarshal([]byte(s), &arbitraryJSON)
	for key, value := range arbitraryJSON {
		if key == field {
			r, ok := value.(string)
			if !ok {
				return "", errors.New("Cannot convert field to string")
			}
			return r, nil
		}
	}
	return "", errors.New("Cannot find field")
}

// returns status code, body as string, and headers cookie
func request(router *gin.Engine, method string, url string, headersMap *map[string]string, body string) (int, string, nethttp.Header) {
	w := httptest.NewRecorder()
	req, _ := nethttp.NewRequest(method, url, strings.NewReader(body))
	if globalCookie != "" {
		req.Header.Set("Cookie", globalCookie)
	}
	if headersMap != nil {
		for key, value := range *headersMap {
			req.Header.Set(key, value)
		}
	}
	router.ServeHTTP(w, req)
	return w.Code, w.Body.String(), w.Header()
}

// same as request, but with some struct that can be serialized in JSON
// returns code, body as string, and headers cookie
func requestJSON(router *gin.Engine, method string, url string, headersMap *map[string]string, jsonObject interface{}) (int, string, nethttp.Header) {
	b, err := json.Marshal(jsonObject)
	if err != nil {
		panic(fmt.Sprint("Unable to marshal following object in JSON: ", jsonObject))
	}
	return request(router, method, url, headersMap, string(b))
}

// Assert status and body
func AssertRequest(t *testing.T, router *gin.Engine, method string, url string, body string, code int, data string) {
	realCode, realBody, _ := request(router, method, url, nil, body)
	assertText := s("%s %s", method, url)
	require.Equal(t, code, realCode, assertText)
	require.Equal(t, data, realBody, assertText)
}

// Assert status and return body will be OK
func AssertAPIOK(t *testing.T, router *gin.Engine, method string, url string, body string, data string) {
	realCode, realBody, _ := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody)
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusOK, realCode, assertText)
	require.JSONEq(t, data, realBody, assertText)
}

// Assert status and return body will be OK
// Return the cookie
func AssertAPIOKAndGetCookie(t *testing.T, router *gin.Engine, method string, url string, body string, data string) string {
	realCode, realBody, header := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody)
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusOK, realCode, assertText)
	require.JSONEq(t, data, realBody, assertText)
	return header.Get("Set-Cookie")
}

// Assert status only will be OK
// returns the body
func AssertAPIStatusOK(t *testing.T, router *gin.Engine, method string, url string, body string) string {
	realCode, realBody, _ := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody)
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusOK, realCode, assertText)
	return realBody
}

// Assert status will be OK
func AssertAPIStringOK(t *testing.T, router *gin.Engine, method string, url string, body string, data string) {
	realCode, realBody, _ := request(router, method, url, nil, body)
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusOK, realCode, assertText)
	// data string will probably have its carriage return characters converted to their \n representation, so convert it back
	require.Equal(t, strings.ReplaceAll(data, "\\n", "\n"), realBody, assertText)
}

// Assert status will be OK
func AssertAPIOKAndGetID(t *testing.T, router *gin.Engine, method string, url string, body string, data string) uint {
	realCode, realBody, _ := request(router, method, url, nil, body)
	assertText := s("%s %s %s", method, url, realBody)
	require.Equal(t, nethttp.StatusOK, realCode, assertText)
	id := getIDFromJSON(t, realBody)
	require.JSONEq(t, s(data, id), realBody, assertText)
	return id
}

// Assert redirection and check redirect url
func AssertAPIRedirectOK(t *testing.T, router *gin.Engine, method string, url string, body string, redirectURL string) {
	realCode, realBody, header := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody, ` | `, header.Get("Location"))
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusFound, realCode, assertText)
	require.Equal(t, header.Get("Location"), redirectURL, assertText)
}

// Assert redirection and check redirect url
func AssertAPIFoundOK(t *testing.T, router *gin.Engine, method string, url string, body string, redirectURL string) {
	realCode, realBody, header := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody, ` | `, header.Get("Location"))
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusFound, realCode, assertText)
	require.Equal(t, header.Get("Location"), redirectURL, assertText)
}

// Assert redirection and check redirect url, with HTTP headers
func AssertAPIRedirectOKWithHeaders(t *testing.T, router *gin.Engine, method string, url string, headersMap map[string]string, body string, redirectURL string) {
	realCode, realBody, header := request(router, method, url, &headersMap, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody, ` | `, header.Get("Location"))
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusFound, realCode, assertText)
	require.Equal(t, header.Get("Location"), redirectURL, assertText)
}

// Assert redirection and check redirect url
func AssertAPIStatusRedirectOK(t *testing.T, router *gin.Engine, method string, url string, body string) string {
	realCode, realBody, header := request(router, method, url, nil, body)
	fmt.Println(`Response: `, realCode, ` | `, realBody, ` | `, header.Get("Location"))
	assertText := s("%s %s", method, url)
	require.Equal(t, nethttp.StatusFound, realCode, assertText)
	return header.Get("Location")
}

func getIDFromJSON(t *testing.T, jsonString string) uint {
	var s struct {
		ID uint `json:"id"`
	}
	getStructFromJson(t, &s, jsonString)
	return s.ID
}

func getStructFromJson(t *testing.T, s interface{}, jsonString string) {
	require.NoError(t, json.Unmarshal([]byte(jsonString), &s), "Structure should match json")
}

// Assert status will not be OK
func assertAPIError(t *testing.T, router *gin.Engine, method string, url string, body string, returnCode int, errorType string) {
	realCode, realBody, _ := request(router, method, url, nil, body)
	assertText := s("%s %s", method, url)
	require.Equal(t, returnCode, realCode, assertText)
	realErrorType, err := getStringValueFromJSONString(realBody, "error")
	require.Nil(t, nil, err, assertText)
	require.Equal(t, errorType, realErrorType, assertText)
}
func AssertAPIErrorNotFound(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusNotFound, model.ErrorTypeNotFound)
}
func AssertAPIErrorUnauthorized(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusUnauthorized, "Authentication failed")
}
func AssertAPIErrorTooManyRequests(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusTooManyRequests, "Too many simultaneous requests")
}
func AssertAPIErrorInvalid(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusBadRequest, model.ErrorTypeInvalid)
}
func AssertAPIErrorUniqueConstraint(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusBadRequest, model.ErrorTypeUniqueConstraint)
}
func AssertAPIErrorForeignKeyConstraint(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusConflict, model.ErrorTypeForeignKeyConstraint)
}
func AssertAPIErrorRequired(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusBadRequest, model.ErrorTypeRequired)
}
func AssertAPIErrorGeneric(t *testing.T, router *gin.Engine, method string, url string, body string) {
	assertAPIError(t, router, method, url, body, nethttp.StatusInternalServerError, model.ErrorTypeGeneric)
}

func getKeyFromFile(filename string) string {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("Cannot read key")
	}
	return strings.ReplaceAll(string(bytes), "\n", "\\n")
}

func initTestOrBench(t *testing.T, configuration config.Configuration) *gin.Engine {
	initOnce.Do(func() {
		config.Set(&configuration)
		// Init Redis
		redis.Connect(&configuration)
		core.SetRedis(redis)

		// Init & Start Cron
		cron.Init()
		cron.Start()
		core.RegisterAllAutoRelease()

		// Configure cache
		cache.Config(&configuration)
		core.Config(&configuration)
		router = http.RouterInit(&configuration)
		cookie := AssertAPIOKAndGetCookie(t, router, "POST", loginPrefix, s(`{"username": "%s", "password": "%s"}`, username, password),
			`{"message":"Successfully authenticated user"}`)
		globalCookie = cookie
	})
	// Wipe DB
	redis.Flush()
	// Clear cache, except sessions
	cache.ClearRequests()
	cache.ClearPrivateKeys()

	return router

}

func initTest(t *testing.T) *gin.Engine {
	var configuration config.Configuration
	// hardcode database configuration to avoid wiping a production DB
	configuration.Redis.Host = "testredis"
	configuration.Redis.Port = "6379"
	configuration.Redis.Password = "test"
	configuration.Redis.DBIndex = 0
	configuration.HTTP.Verbose = true
	configuration.Cache.MaxRequests = testingConfigMaxRequests
	// Set admin credentials
	configuration.Admin.Username = username
	encodedPassword, err := utils.GenerateEncodedHashPassword(password)
	if err != nil {
		panic(err.Error())
	}
	configuration.Admin.PasswordHash = encodedPassword

	return initTestOrBench(t, configuration)
}

func initBench(t *testing.T) *gin.Engine {
	var configuration config.Configuration
	// hardcode database configuration to avoid wiping a production DB
	configuration.Redis.Host = "testredis"
	configuration.Redis.Port = "6379"
	configuration.Redis.Password = "test"
	configuration.Redis.DBIndex = 0
	configuration.HTTP.Verbose = false
	configuration.Cache.MaxRequests = testingConfigMaxRequests
	// Set admin credentials
	configuration.Admin.Username = username
	encodedPassword, err := utils.GenerateEncodedHashPassword(password)
	if err != nil {
		panic(err.Error())
	}
	configuration.Admin.PasswordHash = encodedPassword

	return initTestOrBench(t, configuration)
}
