package main

import (
	"testing"
)

func TestSignatureKey(t *testing.T) {
	router := initTest(t)

	pubkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.pub")
	privkey1 := getKeyFromFile("testdata/this_key_is_for_tests_only.priv")
	finger1 := "705815352252CBF0F880B219259A183BA7C31427"
	pubkey2 := getKeyFromFile("testdata/this_key_is_for_tests_only2.pub")
	finger2 := "1EBAA20875557370D4E845A8E47DA2BA7935DEAF"

	// List (empty)
	AssertAPIOK(t, router, "GET", signaturePrefix, "", "[]")
	// Get one that does not exists
	AssertAPIErrorNotFound(t, router, "GET", s("%s/%s", signaturePrefix, "noonehere"), "")
	// Cannot create with a fake pubkey
	AssertAPIErrorInvalid(t, router, "POST", signaturePrefix,
		`{"pubkey": "itsatrap", "description": ""}`)
	// Create
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// Cannot recreate the same one
	AssertAPIErrorUniqueConstraint(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1))
	// Get
	AssertAPIOK(t, router, "GET", s("%s/%s", signaturePrefix, finger1), "",
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey1, finger1))
	// Modify description
	AssertAPIOK(t, router, "PUT", s("%s/%s", signaturePrefix, finger1),
		s(`{"pubkey": "%s", "description": "a description2"}`, pubkey1),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description2"}`, pubkey1, finger1))
	// Cannot Modify public key
	AssertAPIErrorInvalid(t, router, "PUT", s("%s/%s", signaturePrefix, finger1),
		s(`{"pubkey": "%s", "description": "a description2"}`, pubkey2))
	// Add a real privatekey
	AssertAPIOK(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		s(`{"privatekey": "%s"}`, privkey1), "{}")
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%s", signaturePrefix, finger1), "",
		s(`{"pubkey": "%s", "has_privatekey": true, "fingerprint": "%s", "description": "a description2"}`, pubkey1, finger1))
	// Cannot set a fake privatekey
	AssertAPIErrorInvalid(t, router, "POST", s("%s/%s/privatekey", signaturePrefix, finger1),
		`{"privatekey": "itsatrap"}`)
	// Remove private key
	AssertAPIOK(t, router, "DELETE", s("%s/%s/privatekey", signaturePrefix, finger1), "", "{}")
	// Get again
	AssertAPIOK(t, router, "GET", s("%s/%s", signaturePrefix, finger1), "",
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description2"}`, pubkey1, finger1))
	// Cannot Modify that does not exists
	AssertAPIErrorNotFound(t, router, "PUT", s("%s/%s", signaturePrefix, "test42"),
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey1))
	// Delete
	AssertAPIOK(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "",
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description2"}`, pubkey1, finger1))
	// Cannot Delete again
	AssertAPIErrorNotFound(t, router, "DELETE", s("%s/%s", signaturePrefix, finger1), "")
	// Create another signaturekey
	AssertAPIOK(t, router, "POST", signaturePrefix,
		s(`{"pubkey": "%s", "description": "a description"}`, pubkey2),
		s(`{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}`, pubkey2, finger2))
	// List all signaturekeys
	AssertAPIOK(t, router, "GET", signaturePrefix, "",
		s(`[{"pubkey": "%s", "has_privatekey": false, "fingerprint": "%s", "description": "a description"}]`, pubkey2, finger2))
}
