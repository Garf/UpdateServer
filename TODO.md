
- test reconnection
- in product name change, warn admin the release manifests include old product name -> it should be changed
- make redis more production-ready (backups)
- add some multi-node architecture (one primary service, multiple secondaries)
- manage count history/release history -> do not delete any release that has been published/manifest downloaded ?
