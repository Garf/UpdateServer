package cron

import (
	"fmt"
	"sync"

	"code.videolan.org/videolan/updateserver/model"
	rcron "github.com/robfig/cron/v3"
	"golang.org/x/net/context"
)

var (
	cronMu          = &sync.Mutex{}
	autoReleaseCron = AutoReleaseCron{}
)

func Init() {
	cronMu.Lock()
	defer cronMu.Unlock()
	autoReleaseCron.Init()
}

func RegisterAutoRelease(autoRelease *model.AutoRelease, job func()) error {
	cronMu.Lock()
	defer cronMu.Unlock()
	return autoReleaseCron.RegisterAutoRelease(autoRelease, job)
}

func UnregisterAutoRelease(autoReleaseID uint) error {
	cronMu.Lock()
	defer cronMu.Unlock()
	return autoReleaseCron.UnregisterAutoRelease(autoReleaseID)
}

func Start() {
	cronMu.Lock()
	defer cronMu.Unlock()
	autoReleaseCron.Start()
}

func Stop() context.Context {
	cronMu.Lock()
	defer cronMu.Unlock()
	return autoReleaseCron.Stop()
}

// AutoReleaseCron includes a Cron and a map between AutoReleases and the Cron jobs
type AutoReleaseCron struct {
	InnerCron           *rcron.Cron
	AutoReleaseEntryMap map[uint]rcron.EntryID
}

func (ac *AutoReleaseCron) Init() {
	ac.InnerCron = rcron.New()
	ac.AutoReleaseEntryMap = map[uint]rcron.EntryID{}
}

// RegisterAutoRelease takes the autoRelease into account in the Cron.
// If the autoRelease has already been registered, it does nothing.
// If the autoRelease Schedule is empty, it removes it from the Cron (if necessary).
// If the autoRelease Schedule is not empty, it adds a corresponding job to the Cron.
func (ac *AutoReleaseCron) RegisterAutoRelease(autoRelease *model.AutoRelease, job func()) error {
	if autoRelease == nil {
		return fmt.Errorf("Cannot register a non existant AutoRelease to Cron")
	}
	if _, found := ac.AutoReleaseEntryMap[autoRelease.ID]; found {
		ac.UnregisterAutoRelease(autoRelease.ID)
	}
	if autoRelease.Schedule != "" {
		entry, err := ac.InnerCron.AddFunc(autoRelease.Schedule, job)
		if err != nil {
			return err
		}
		ac.AutoReleaseEntryMap[autoRelease.ID] = entry
	}
	return nil
}

// UnregisterAutoRelease removes any trace of the autoRelease from the Cron
func (ac *AutoReleaseCron) UnregisterAutoRelease(autoReleaseID uint) error {
	entry, found := ac.AutoReleaseEntryMap[autoReleaseID]
	if !found {
		return fmt.Errorf("Cannot find Autorelease %d in Cron", autoReleaseID)
	}
	ac.InnerCron.Remove(entry)
	delete(ac.AutoReleaseEntryMap, autoReleaseID)
	return nil
}

func (ac *AutoReleaseCron) Start() {
	ac.InnerCron.Start()
}

func (ac *AutoReleaseCron) Stop() context.Context {
	for autoReleaseID := range ac.AutoReleaseEntryMap {
		ac.UnregisterAutoRelease(autoReleaseID)
	}
	return ac.InnerCron.Stop()
}
